call `sp_add_product`(1,'Chocolate','Producto','2018-05-17','2018-07-20',30,200,'0 Azucar','',1,1,5);
call `sp_add_product`(2,'Bombones','OFERTA','2018-05-17','2018-07-20',80,40,'Relleno no incluido','',1,2,5);
call `sp_add_product`(3,'Empanadas','OFERTA','2018-05-17','2018-07-20',24,50,'Rellenas de una receta de Oxigeno con un poco de carne','',1,3,5);
call `sp_add_product`(4,'Refuerzos','OFERTA','2018-05-17','2018-07-20',15,100,'De Jamon con... Jamon','',1,4,5);

call `sp_add_product`(1,'Televisor','Producto','2018-05-17','2018-07-20',2500,10,'Blanco y Negro','',1,1,1);
call `sp_add_product`(2,'PC','OFERTA','2018-05-17','2018-07-20',5300,1,'Enciende','',1,2,1);
call `sp_add_product`(3,'Chromecast','OFERTA','2018-05-17','2018-07-20',1200,1,'Trae Nefli','',1,3,1);
call `sp_add_product`(4,'Smartphone','OFERTA','2018-05-17','2018-07-20',9000,1,'Se transforma en piedra dependiendo del comprador','',1,4,1);

call `sp_add_product`(1,'Dibujo','Producto','2018-05-17','2018-07-20',100,1,'Hecho con mucho amor por mi nene de 5','',1,1,9);
call `sp_add_product`(2,'Cuadro','Producto','2018-05-17','2018-07-20',1000000,1,'Descubierto en un viaje a Babilonia, justo pase por una feria y el tipo me dijo que lo pinto Moises antes de abrir el mar','',1,2,9);
call `sp_add_product`(3,'Escultura','Producto','2018-05-17','2018-07-20',2100,1,'Mi mama dice que esta buena','',1,3,9);
call `sp_add_product`(4,'Poster','Producto','2018-05-17','2018-07-20',50,4,'Es grande','',1,4,9);

call `sp_add_product`(1,'Shampo','Producto','2018-05-17','2018-07-20',94,40,'Shampo para monje Tibetano','',1,1,2);
call `sp_add_product`(2,'Jabon Aleman','Producto','2018-05-17','2018-07-20',10,100,'De Auschwitz a tu puerta','',1,2,2);
call `sp_add_product`(3,'Esmalte de uñas','Producto','2018-05-17','2018-07-20',38,32,'Es tan transparente que parece agua pero no lo es','',1,3,2);
call `sp_add_product`(4,'Fijador','Producto','2018-05-17','2018-07-20',115,10,'Efectos duran de 1 a 3 meses','',1,4,2);