<?php 
class graph{
  public function color_generator(){
    // crea colores random RGB
    $r=rand(0, 255);
    $g=rand(0, 255);
    $b=rand(0, 255);
    //Setea los colores del borde en 1 y el Fondo con transparencia 0.6      
    $border_color="'rgba(".$r.",".$g.",".$b.", 1)'";
    $background_color="'rgba(".$r.",".$g.",".$b.", 0.6)'";
    $color=array($border_color,$background_color);

    return $color;
  }
  public function dataset_construct($array){
    //Construye los dataset de chart.js a partir de los resultados de la consulta
    $dataset="[";
    $a="[";
    $b="[";
    $n=$array->columnCount();
    if ($n>1) {
      foreach ($array as $data) {
        $a.="'".$data[0]."',";
        $b.="".$data[1].",";
      }
      $a=substr($a,0,-1)."]";
      $b=substr($b,0,-1)."]";
      $dataset=array('label' => $a , 'data' => $b );
    }else{
      foreach ($array as $data) {
        $dataset.="'".$data[0]."',";
      }
      $dataset=substr($dataset,0,-1)."]";
    }
    
   
    return $dataset;
  }
  public function get_data($query){
    //ejecuta la query y la guarda en un array
    include("conexion.php");
    $rs=$con->query($query);
    return $rs;
  }
  public function graph_construct($arrayX,$arrayY,$temp,$canvas,$type,$n,$data,$title){
    //Construye los dataset dependiendo si es una consulta doble o
    //si llegan los resultados por separado
    if ($data != '-1') {
      $result=$this->dataset_construct($data);
      $label=$result['label'];
      $value=$result['data'];
      $size=$data->rowCount();
    }else{
      $label=$this->dataset_construct($arrayX);
      $value=$this->dataset_construct($arrayY);
      $size=$arrayX->rowCount();
    }
    //carga los colores
    $color=array();
    $border="";$background="";
    for ($i=0; $i < $size; $i++) { 
      $color=$this->color_generator();
      $border.=$color[0].",";
      $background.=$color[1].",";
    }
    $border="[".substr($border,0,-1)."]";
    $background="[".substr($background,0,-1)."]";
    //sustituye el chart por la informacion
    $a=array("{label}","{data}","{border}","{background}");
    $b=array("cantidad",$value,$border,$background);
    $temp=str_replace($a, $b, $temp);
    
   
    //crea la Vista y la retorna
    $a=array("{nombre}","{data}","{datos}","{type}","{n}","{title}");
    $b=array($label,$temp,$value,$type,$n,$title);
    $canvas=str_replace($a, $b, $canvas);
    return $canvas;
  }
  public function view_construct($query,$query2,$type,$n,$data,$title){
    //carga los templates
    $temp=file_get_contents("./View/templates/dataset.html");
    $canvas=file_get_contents("./View/templates/graph.html");
    $index=file_get_contents("./View/index.html");
    //obtiene el resultado de las consultas
    $rs=$this->get_data($query);
    $rsy=$this->get_data($query2);
    if ($data!="-1") {
      $data=$this->get_data($data);
    }

    $graph=$this->graph_construct($rs,$rsy,$temp,$canvas,$type,$n,$data,$title);
    $view=str_replace("{GRAFICA}", $graph, $index);
    return $view;
  }

}


  
  $v=new graph;
  //recive las query
  //$query="select name from product";
  //$query2="$_POST['sql2']";
  //$data=$_POST['data'];
  //construye los distintos tipos de grafica
  $query="select name from product";
  $query2="select price from product";
  $data="call sp_get_category_quantity";

  $type="pie";
  $view=$v->view_construct($query,$query2,$type,"0",$data,"Productos/Categoria");

  $query="select name from product";
  $query2="select price from product";
  $data="call sp_get_chart_by_id(2)";

  $type="bar";
  $view.=$v->view_construct($query,$query2,$type,"1",$data,"Ventas por producto");

  $query="select name from product";
  $query2="select price from product";
  $data="call sp_get_product_sells(2)";

  $type="line";
  $view.=$v->view_construct($query,$query2,$type,"2",$data,"Ventas por dia");
  $query="select name from product";
  $query2="select price from product";
  $data="call sp_get_category_quantity";

  $type="radar";
  
  $view.=$v->view_construct($query,$query2,$type,"3",$data,"Productos/Categoria");

  $query="select name from product";
  $query2="select price from product";
  $data="call sp_get_chart_by_id(2)";

  $type="polarArea";
  $view.=$v->view_construct($query,$query2,$type,"4",$data,"Ventas por producto");
  $query="select name from product";
  $query2="select price from product";
  $data="call sp_get_product_sells(2)";

  $type="doughnut";
  
  $view.=$v->view_construct($query,$query2,$type,"5",$data,"Ventas por dia");
  print $view;

?>