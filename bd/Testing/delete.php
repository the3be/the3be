<?php 
require('conexion.php');

$DELETE=array("truncate table user;","
truncate table user_states;","
truncate table product;","
truncate table product_updates;","
truncate table purchase;","
truncate table purchase_states;","
truncate table local;","
truncate table favorite;","
truncate table category;");

$REINICIAR=array("ALTER TABLE user AUTO_INCREMENT =1;",
"ALTER TABLE user_states AUTO_INCREMENT =1;",
"ALTER TABLE product AUTO_INCREMENT =1;",
"ALTER TABLE product_updates AUTO_INCREMENT =1;",
"ALTER TABLE purchase AUTO_INCREMENT =1;",
"ALTER TABLE purchase_states AUTO_INCREMENT =1;",
"ALTER TABLE local AUTO_INCREMENT =1;",
"ALTER TABLE favorite AUTO_INCREMENT =1;",
"ALTER TABLE category AUTO_INCREMENT =1;");
$inicio="SET foreign_key_checks = 0;";
$fin="SET foreign_key_checks = 1;";
try {
	for ($i=0; $i < 9; $i++) { 
		try {
			$sql=$inicio.$DELETE[$i].$fin;
			$con->query($sql);
			
			echo "Tabla ".$i." vaciada correctame <br>";
		} catch (PDOException $e) {
			echo "Error en tabla ".$i;
		}
	}
	for ($i=0; $i < 9; $i++) { 
		try {
			$con->query($REINICIAR[$i]);
			
			echo "Indice en tabla ".$i." reiniciado correctame <br>";
		} catch (PDOException $e) {
			echo "Error en tabla ".$i;
		}
	}
	

	echo "Base de Datos vaciada exitosamente <a href='index.php'>Volver</a>";
} catch (PDOException $e) {
	echo "error <a href='index.php'>Volver</a>".$e->getMessage();
}

 ?>
 