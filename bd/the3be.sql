-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 06-01-2019 a las 02:45:51
-- Versión del servidor: 10.1.32-MariaDB
-- Versión de PHP: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `the3be`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_favorite` (IN `user_id` INT, IN `product_id` INT, IN `var` INT)  BEGIN
SET @spy=(select user_id from favorite where favorite.user_id=user_id and 	favorite.product_id=product_id);
	IF(var=1) THEN
        IF (@spy is null) THEN
            insert into favorite (user_id,product_id) values 		(user_id,product_id);
        END IF;
  	ELSEIF(var=2) THEN
    delete from favorite where user_id=favorite.user_id and favorite.product_id=product_id;
    END IF;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_local` (IN `user_id` INT(8), IN `name` VARCHAR(255), IN `address` VARCHAR(255), IN `schedule` VARCHAR(23), IN `phone` VARCHAR(50))  BEGIN
  insert into local (user_id, name, address, schedule, phone) values(user_id,name,address,schedule,phone);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_product` (`user_id` INT(10), `name` VARCHAR(100), `type` VARCHAR(45), `date_ini` DATE, `date_end` DATE, `price` FLOAT(11), `stock` INT(11), `info` TEXT, `img` VARCHAR(50), `status` INT(11), `local_id` INT(10), `category` INT(10))  BEGIN
  insert into product (user_id,name,type,date_ini,date_end,price,stock,info,img,status,local_id,category_id)values(user_id,name,type,date_ini,date_end,price,stock,info,img,status,local_id,category);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_product_review` (IN `user_id` INT, IN `product_id` INT, IN `purchase_id` INT, IN `review` TEXT, IN `stars` INT)  BEGIN
	SET @date=CURDATE();
	insert into review (user_id,product_id,purchase_id,date,review,stars) values (user_id,product_id,purchase_id,@date,review,stars);
    update purchase set purchase.status=3 where purchase.id=purchase_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_purchase` (`user_id` INT(11), `product_id` INT(11), `coment` TEXT)  BEGIN
  insert into purchase (user_id,product_id,date,status,coment) values (user_id,product_id,CURDATE(),1,coment);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_add_user` (IN `name` VARCHAR(45), IN `email` VARCHAR(100), IN `pass` VARCHAR(50), IN `status` INT(11), IN `sub_state` INT(11))  BEGIN
	
	SET @mail=(select mail from user where mail=email);
    SET @state=(select user.status from user where mail=email);
	IF (@mail=email and @state = 0) then
		update user set name=name where mail=@mail;
		update user set pass=pass where mail=@mail;
		update user set status=status where mail=@mail;
	ELSE
			insert into user (name,mail,pass,status,sub_state) values(name,email,pass,status,sub_state);
	END IF; 
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_all_sales` (`idUser` INT)  BEGIN
	select purchase.id,product.name as product,date_end,price,img,client_info.name,client_info.mail,seller_info.name as seller,seller_info.mail as mail_seller,local.address,local.schedule
	from purchase
	inner join product on product.id=purchase.product_id
	inner join user as seller_info on seller_info.id=product.user_id
	inner join user as client_info on client_info.id=purchase.user_id
	inner join local on local.id=product.local_id
	where purchase.status=1 and purchase.user_id=idUser
	group by product.id ;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_all_sales_by_idUser` (IN `idUser` INT)  BEGIN
	select purchase.id,product.id as id_product,product.name as product,date_end,price,img,client_info.name,client_info.mail,seller_info.name as seller,seller_info.mail as mail_seller,local.address,local.schedule,local.phone
	from purchase
	inner join product on product.id=purchase.product_id
	inner join user as seller_info on seller_info.id=product.user_id
	inner join user as client_info on client_info.id=purchase.user_id
	inner join local on local.id=product.local_id
	where purchase.status=1 and purchase.user_id=idUser
	group by product.id ;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_best4_category` ()  BEGIN
	select distinct category_id,count(purchase.id) as cantidad, category.name
    from purchase,product,category
    where product.id=purchase.product_id and purchase.status=3 and product.status=1 and category_id=category.id
    group by category.name
    order by cantidad desc
    LIMIT 4;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_bestseller_product` ()  BEGIN
	select distinct product.id,product.name,category_id,product.user_id,category.name as category,type,info,price,img,product_id, count(product_id) as cantidad
from purchase,category,product,user
where product.id=product_id and product.user_id=user.id and user.status=1 and category.id=category_id and purchase.status=3 and product.status=1
group by product_id,product.id,category.id
order by cantidad desc limit 4;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_best_of_category` ()  BEGIN
	select product_id,name,max(cantidad) as quantity,user_id,category_id,category,date_end,type,info,price,img
	from sales
	group by category
	order by cantidad desc
  limit 6;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_best_of_week` ()  BEGIN
	select product.id,product.name,category_id,category.name as category,type,date,info,price,img,product_id, count(product_id) as cantidad
from purchase, product, category,user
where date >= DATE_SUB(CURDATE(), INTERVAL 1 WEEK) and product.user_id=user.id and user.status=1 and product_id=product.id and category_id=category.id
group by product_id 
order by cantidad desc limit 1;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_category` ()  BEGIN
  select id,name from category;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_category_quantity` ()  BEGIN
  select category.name as category,count(product.id) as cantidad
  from product,category
  where category.id=product.category_id
  group by category.name;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_chart_by_id` (`id` INT)  BEGIN
  select name,count(product_id) from product
  left join purchase on product.id=purchase.product_id
  where product.user_id=id and purchase.status=3
  group by name;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_earnings` ()  BEGIN
  select date,sum(price)*0.05 as Ganancia,date
  from purchase
  join product on purchase.product_id=product.id
  where purchase.status=3 and date > curdate()-14
  group by date;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_favorites` (IN `user_id` INT)  BEGIN
	select 

product.id,
product.name,
category_id,
category.name as category,
type,info,
price,img,
product.date_ini,
user_id as user_id


from favorite,product,category

    where favorite.product_id=product.id and category_id=category.id and favorite.user_id= user_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_filtered_product` (IN `nombre` VARCHAR(50), IN `price_min` FLOAT(10), IN `price_max` FLOAT(10), IN `category` INT(10), IN `type` VARCHAR(50))  BEGIN
  set @today=CURDATE();
  IF (nombre = -1 and category = -1 and price_min = -1 and price_max = -1 and type = -1) THEN
    select product.id,product.name,category_id,product.date_ini,product.user_id,category.name as category,product.type,info,price,img 
    from product,category,user
    where product.status = 1 and date_end >= @today and product.user_id=user.id and user.status=1 and category_id=category.id
    group by product.id order by product.id desc;
  ELSE 
  SET @consulta= "select product.id,product.name,product.user_id,category_id,product.date_ini,category.name as category,type,info,price,img \n    from product,category ,user\n    where";
  IF  (nombre <> -1) THEN
    SET @consulta=CONCAT(@consulta," (product.name like'%",nombre,"%' or  product.info like'%",nombre,"%')");
  END IF;
    
  IF  (category <> -1) THEN
    IF (nombre <> -1) THEN
      SET @consulta=CONCAT(@consulta," and");
    END IF;
  SET @consulta=CONCAT(@consulta," category_id = ",category,"");
  END IF;

  IF  (type <> -1) THEN
    IF (nombre <> -1 or category <> -1) THEN
      SET @consulta=CONCAT(@consulta," and");
    END IF;
    SET @consulta=CONCAT(@consulta," type = '",type,"'");
  END IF;
    
  IF  (price_min <> -1 and price_max <> -1) THEN
    IF (nombre <> -1 or category <> -1 or type <> -1) THEN
      SET @consulta=CONCAT(@consulta," and");
    END IF;
    
    SET @consulta=CONCAT(@consulta," price <= '",price_max,"' and price >= '",price_min,"'");
  END IF;
  

  
	SET @consulta=CONCAT(@consulta," and product.user_id=user.id and user.status=1 and product.status = 1 and date_end >= @today and category_id=category.id  order by product.id desc");
    PREPARE stmt FROM @consulta;
    EXECUTE stmt;
  
  
  
  END IF;

  

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_local` (`u_id` INT(8))  BEGIN
  select * from local where user_id=u_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_new_products` ()  BEGIN
  select product.id,product.name,product.user_id,category_id,date_ini,category.name as category,type,info,price,img,avg(stars) as stars
  from product
  left join review on product.id=review.product_id
  join category on category_id=category.id
  where status=1
  group by product.id
  order by date_ini desc
  limit 5;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_num_purchases` (IN `user_id` INT)  BEGIN
	SELECT count(product.id)
    FROM purchase
    join product on product_id=product.id
    join user on purchase.user_id=user_id
    WHERE product.user_id=user.id and purchase.status=2;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_num_sales` (IN `userId` INT)  BEGIN
	select count(purchase.id)
	from purchase
  join product on product_id=product.id
	where purchase.user_id and purchase.status=1 and product.user_id=userId;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_product_from_id` (IN `id` INT)  BEGIN
select product.id,product.name,product.user_id,category_id,category.name as category,type,info,price,img,avg(stars) as stars
from product
left join review on product.id=review.product_id
join category on category_id=category.id
where product.id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_product_review` (`id` INT)  BEGIN
  SELECT user.name,review
  from review,user
  where product_id=id
  and user.id = review.user_id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_product_sells` (`productid` INT, `userid` INT)  BEGIN

SET @product_id=productid;
SET @user_id=userid;

if (@product_id='-1') then
  SET @product_id=(select product_id from purchase
  join product on product_id=product.id
  where product.user_id=@user_id
  group by product_id order by count(product_id) desc limit 1);
end if;


select date,count(product_id) as Cantidad,name from product
  left join purchase on product.id=purchase.product_id
  where product.id=@product_id and purchase.status=3
  group by date;



END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_prox_to_end` ()  BEGIN
SET @today = CURDATE();
	select product.id,product.name,category_id,product.date_end,category.name as category,type,info,price,img
	from product,category ,user
    where product.status <> -1 and date_end >= @today and product.user_id=user.id and user.status=1 and category_id=category.id order by date_end   LIMIT 1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_subscriber` ()  BEGIN
  select mail from user where sub_state=1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_user` (IN `email` VARCHAR(100), IN `password` VARCHAR(50))  BEGIN
  SET @status=(select status from user where mail=email and pass=password);
  if (@status=2) then
	update user set status = 1 where mail=email;
  end if;
  select id,name,mail,pass,status from user where mail=email and pass=password and status=1;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_user_data` (`id` INT)  BEGIN


	select id,name,mail,status,sub_state from user where user.id=id;


END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_user_products` (`user_id` INT)  BEGIN
	select product.id,product.name,category_id,date_end,category.name as category,type,info,price,img 
	from product,category
    where user_id=user_id and category_id=category.id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_user_purchases` (IN `user_id` INT)  BEGIN
	SELECT product.id,purchase.id as id_purchase,product.name AS product,user.name AS seller,date_end,purchase.status,img,price
    FROM purchase
    join product on product_id=product.id
    join user on purchase.user_id=user_id
    WHERE product.user_id=user.id and (purchase.status=1 or purchase.status=2);
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_get_user_sales` (IN `user_id` INT)  BEGIN	
	select purchase.id,product.name as product,date_end,user.name as client,purchase.status,price,img
	from purchase,product,user
	where product.user_id=user_id and purchase.status=1 and product.id=purchase.product_id and user.id=purchase.user_id
	group by product.id ;

END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_purchase_state` (IN `id` INT, IN `status` INT, IN `coment` TEXT)  BEGIN
	update purchase set purchase.status=status where purchase.id=id;
    update purchase set purchase.coment=coment where purchase.id=id;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_user_info` (IN `user_id` INT, IN `name` VARCHAR(100), IN `pass` VARCHAR(100), IN `mail` VARCHAR(100), IN `status` INT, IN `sub_state` INT)  BEGIN
	SET @user_id=user_id;
    SET @name=name;
    SET @pass=pass;
    SET @mail=mail;
    SET @status=status;
    SET @sub_state=sub_state;
    SET @consulta="";
	if(@name <> '-1') then
        SET @consulta=CONCAT(@consulta,'update user set name=@name where id=@user_id;');
    end if;
    if(@pass <> '-1') then
        SET @consulta=CONCAT(@consulta,'update user set pass=@pass where id=@user_id;');
    end if;
    if(@mail <> '-1') then
        SET @consulta=CONCAT(@consulta,'update user set mail=@mail where id=@user_id;');
    end if;
    if(@status ='-1' or @status ='1' or @status ='2') then
        SET @consulta=CONCAT(@consulta,'update user set status=@status where id=@user_id;');
    end if;
    if(@sub_state ='-1' or @sub_state ='1' or @sub_state ='2') then
        SET @consulta=CONCAT(@consulta,'update user set sub_state=@sub_state where id=@user_id;');
    end if;
	PREPARE stmt FROM @consulta;
    EXECUTE stmt;
    
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_update_user_state_mail` (`mail` VARCHAR(100))  BEGIN
	update user set status=1 where user.mail=mail;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Tecnologia'),
(2, 'Salud y Belleza'),
(3, 'Entretenimiento'),
(4, 'Hogar'),
(5, 'Alimentos y Bebidas'),
(6, 'Inmuebles'),
(7, 'Vehiculos'),
(8, 'Animales y Mascotas'),
(9, 'Arte y Antiguedades'),
(10, 'Adultos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favorite`
--

CREATE TABLE `favorite` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local`
--

CREATE TABLE `local` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `schedule` varchar(23) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `local`
--

INSERT INTO `local` (`id`, `user_id`, `name`, `address`, `schedule`, `phone`) VALUES
(1, 1, 'Avanza', 'Avenida 18 de Julio 1386, Montevideo, Uruguay', 'De 09AM a 05PM', '098183725'),
(2, 2, 'Pato Cueva', 'Lima 1747, Montevideo, Uruguay', 'De 05PM a 09PM', '099474348'),
(3, 3, 'La cueva del hermitaÃ±o', 'Turquia 4020, Montevideo, Uruguay', 'Todo el dia', '093971404'),
(4, 4, 'Jorge Town', 'Avenida 18 de Julio 1487, Montevideo, Uruguay', 'De 12PM a 05PM', '091428396');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `type` varchar(45) DEFAULT NULL COMMENT 'Oferta/Producto',
  `date_ini` date DEFAULT NULL,
  `date_end` date DEFAULT NULL,
  `price` float DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `info` text,
  `img` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `local_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `user_id`, `name`, `type`, `date_ini`, `date_end`, `price`, `stock`, `info`, `img`, `status`, `local_id`, `category_id`) VALUES
(1, 1, 'Ropa interior comestible', 'DESCUENTO', '2018-12-02', '2019-01-23', 3197, 46, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/1.jpg', 1, 1, 10),
(2, 2, 'Preservativos saborizado', 'DESCUENTO', '2018-12-02', '2019-01-13', 261, 44, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/2.jpg', 1, 2, 10),
(3, 3, 'Jueguetes', 'DESCUENTO', '2018-12-02', '2019-01-09', 495, -3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/3.jpg', -1, 3, 10),
(4, 4, 'Peliculas Clasificadas por encargo', 'OFERTA', '2018-12-02', '2019-01-13', 2883, 75, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/4.jpg', 1, 4, 10),
(5, 1, 'Fort T restaurado', 'VENTA', '2018-12-02', '2019-01-31', 1500, 68, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/5.jpg', 1, 1, 9),
(6, 2, 'Jarron de la dinastÃ­a ming', 'DESCUENTO', '2018-12-02', '2019-01-24', 322, -10, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/6.jpg', -1, 2, 9),
(7, 3, 'Cuadro Babilonico', 'OFERTA', '2018-12-02', '2019-01-29', 2136, 16, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/7.jpg', 1, 3, 9),
(8, 4, 'tenedores de alpaca', 'DESCUENTO', '2018-12-02', '2019-01-08', 3084, 69, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/8.jpg', 1, 4, 9),
(9, 1, 'Huesitos', 'DESCUENTO', '2018-12-02', '2019-01-12', 3072, 89, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/9.jpg', 1, 1, 8),
(10, 2, 'Accesorios', 'DESCUENTO', '2018-12-02', '2019-01-15', 1687, 83, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/10.jpg', 1, 2, 8),
(11, 3, 'Buzos de perro', 'OFERTA', '2018-12-02', '2019-01-14', 1687, 50, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/11.jpg', 1, 3, 8),
(12, 4, 'Montas', 'VENTA', '2018-12-02', '2019-01-08', 740, -2, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/12.jpg', -1, 4, 8),
(13, 1, 'Bora', 'OFERTA', '2018-12-02', '2019-01-19', 1608, 75, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/13.jpg', 1, 1, 7),
(14, 2, 'Vespa', 'VENTA', '2018-12-02', '2019-01-09', 2608, 32, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/14.jpg', 1, 2, 7),
(15, 3, 'Harley Davison', 'OFERTA', '2018-12-02', '2019-01-21', 2877, 49, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/15.jpg', 1, 3, 7),
(16, 4, 'Cubiertas de motos', 'VENTA', '2018-12-02', '2019-01-03', 2645, 51, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/16.jpg', 1, 4, 7),
(17, 1, 'Alquiler de apartamento en el centro', 'OFERTA', '2018-12-02', '2019-01-20', 2162, 41, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/17.jpg', 1, 1, 6),
(18, 2, 'Terreno baldio en casavalle ', 'VENTA', '2018-12-02', '2019-01-31', 3555, 92, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/18.jpg', 1, 2, 6),
(19, 3, 'Ropero', 'DESCUENTO', '2018-12-02', '2019-01-10', 3724, 55, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/19.jpg', 1, 3, 6),
(20, 4, 'Penhouse in Borro town', 'OFERTA', '2018-12-02', '2019-01-13', 767, 21, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/20.jpg', 1, 4, 6),
(21, 1, 'Alfajores de maicena', 'DESCUENTO', '2018-12-02', '2019-01-16', 3167, 37, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/21.jpg', 1, 1, 5),
(22, 2, 'Agua mineral levemente gasificada', 'VENTA', '2018-12-02', '2019-01-27', 3896, 86, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/22.jpg', 1, 2, 5),
(23, 3, 'Garrapinada', 'DESCUENTO', '2018-12-02', '2019-01-18', 1531, -3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/23.jpg', -1, 3, 5),
(24, 4, 'Cheesecake', 'OFERTA', '2018-12-02', '2019-01-02', 3115, 5, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/24.jpg', 1, 4, 5),
(25, 1, 'Alfombras Welcome', 'VENTA', '2018-12-02', '2019-01-01', 375, 54, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/25.jpg', 1, 1, 4),
(26, 2, 'Jabon liquido', 'OFERTA', '2018-12-02', '2019-01-23', 1245, 80, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/26.jpg', 1, 2, 4),
(27, 3, 'Manteles Bordados', 'OFERTA', '2018-12-02', '2019-01-31', 1787, 22, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/27.jpg', 1, 3, 4),
(28, 4, 'Lamparas de sal del desierto del Himalaya', 'DESCUENTO', '2018-12-02', '2019-01-18', 3168, 20, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/28.jpg', 1, 4, 4),
(29, 1, 'Juegos de PS2', 'VENTA', '2018-12-02', '2019-01-30', 2629, 14, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/29.jpg', 1, 1, 3),
(30, 2, 'Juegos de mesa', 'OFERTA', '2018-12-02', '2019-01-06', 397, 26, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/30.jpg', 1, 2, 3),
(31, 3, 'Guitarra', 'VENTA', '2018-12-02', '2019-01-18', 1176, 25, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/31.jpg', 1, 3, 3),
(32, 4, 'Consolas de juegos retro', 'DESCUENTO', '2018-12-02', '2019-01-29', 1645, 80, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/32.jpg', 1, 4, 3),
(33, 1, 'Cera de Pelo', 'VENTA', '2018-12-02', '2019-01-31', 2442, 46, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/33.jpg', 1, 1, 2),
(34, 2, 'Espuma de Afeitar', 'VENTA', '2018-12-02', '2019-01-01', 3940, -22, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/34.jpg', -1, 2, 2),
(35, 3, 'Labial', 'DESCUENTO', '2018-12-02', '2019-01-13', 970, 37, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/35.jpg', 1, 3, 2),
(36, 4, 'Planchita para el pelo', 'OFERTA', '2018-12-02', '2019-01-23', 1232, 75, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/36.jpg', 1, 4, 2),
(37, 1, 'Mouse Inalambrico Logitec', 'VENTA', '2018-12-02', '2019-01-15', 2275, 58, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/37.jpg', 1, 1, 1),
(38, 2, 'Auriculares', 'DESCUENTO', '2018-12-02', '2019-01-07', 3409, 10, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/38.jpg', 1, 2, 1),
(39, 3, 'Monitor 20', 'DESCUENTO', '2018-12-02', '2019-01-25', 2913, 71, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/39.jpg', 1, 3, 1),
(40, 4, 'Gadget', 'OFERTA', '2018-12-02', '2019-01-25', 3155, 47, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'product/40.jpg', 1, 4, 1);

--
-- Disparadores `product`
--
DELIMITER $$
CREATE TRIGGER `product_AFTER_INSERT` AFTER INSERT ON `product` FOR EACH ROW BEGIN
  insert into product_updates (product_id,date_ini,date_end,hour_ini,hour_end,state,price) values (NEW.id,now(),null,time(now()),null,NEW.status,NEW.price);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `product_BEFORE_UPDATE` BEFORE UPDATE ON `product` FOR EACH ROW BEGIN
  IF (NEW.stock='0' ) THEN
    SET NEW.status = '-1';
    END IF;
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `product_after_update` AFTER UPDATE ON `product` FOR EACH ROW BEGIN
set @today= CURDATE();
  
  IF (new.status <> old.status or new.price <> old.price) THEN BEGIN
        update product_updates set date_end=now() where product_id=new.id and date_end is null;
    update product_updates set hour_end=time(now()) where product_id=new.id and hour_end is null;
    insert into product_updates (product_id,date_ini,date_end,hour_ini,hour_end,state,price) VALUES (new.id,now(),null,time(now()),null,new.status,new.price);
    END; END IF;
  
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product_updates`
--

CREATE TABLE `product_updates` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date_ini` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `hour_ini` varchar(8) NOT NULL,
  `hour_end` varchar(8) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `price` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `product_updates`
--

INSERT INTO `product_updates` (`id`, `product_id`, `date_ini`, `date_end`, `hour_ini`, `hour_end`, `state`, `price`) VALUES
(1, 1, '2018-12-02', NULL, '12:26:49', NULL, 1, 3197),
(2, 2, '2018-12-02', NULL, '12:26:49', NULL, 1, 261),
(3, 3, '2018-12-02', '2018-12-02', '12:26:49', '12:27:16', 1, 495),
(4, 4, '2018-12-02', NULL, '12:26:49', NULL, 1, 2883),
(5, 5, '2018-12-02', NULL, '12:26:49', NULL, 1, 1500),
(6, 6, '2018-12-02', '2018-12-02', '12:26:49', '12:26:58', 1, 322),
(7, 7, '2018-12-02', NULL, '12:26:49', NULL, 1, 2136),
(8, 8, '2018-12-02', NULL, '12:26:49', NULL, 1, 3084),
(9, 9, '2018-12-02', NULL, '12:26:50', NULL, 1, 3072),
(10, 10, '2018-12-02', NULL, '12:26:50', NULL, 1, 1687),
(11, 11, '2018-12-02', NULL, '12:26:50', NULL, 1, 1687),
(12, 12, '2018-12-02', '2018-12-02', '12:26:50', '12:27:19', 1, 740),
(13, 13, '2018-12-02', NULL, '12:26:50', NULL, 1, 1608),
(14, 14, '2018-12-02', NULL, '12:26:50', NULL, 1, 2608),
(15, 15, '2018-12-02', NULL, '12:26:50', NULL, 1, 2877),
(16, 16, '2018-12-02', NULL, '12:26:50', NULL, 1, 2645),
(17, 17, '2018-12-02', NULL, '12:26:50', NULL, 1, 2162),
(18, 18, '2018-12-02', NULL, '12:26:50', NULL, 1, 3555),
(19, 19, '2018-12-02', NULL, '12:26:50', NULL, 1, 3724),
(20, 20, '2018-12-02', NULL, '12:26:50', NULL, 1, 767),
(21, 21, '2018-12-02', NULL, '12:26:50', NULL, 1, 3167),
(22, 22, '2018-12-02', NULL, '12:26:50', NULL, 1, 3896),
(23, 23, '2018-12-02', '2018-12-02', '12:26:50', '12:27:10', 1, 1531),
(24, 24, '2018-12-02', NULL, '12:26:50', NULL, 1, 3115),
(25, 25, '2018-12-02', NULL, '12:26:50', NULL, 1, 375),
(26, 26, '2018-12-02', NULL, '12:26:50', NULL, 1, 1245),
(27, 27, '2018-12-02', NULL, '12:26:50', NULL, 1, 1787),
(28, 28, '2018-12-02', NULL, '12:26:50', NULL, 1, 3168),
(29, 29, '2018-12-02', NULL, '12:26:50', NULL, 1, 2629),
(30, 30, '2018-12-02', NULL, '12:26:50', NULL, 1, 397),
(31, 31, '2018-12-02', NULL, '12:26:50', NULL, 1, 1176),
(32, 32, '2018-12-02', NULL, '12:26:50', NULL, 1, 1645),
(33, 33, '2018-12-02', NULL, '12:26:50', NULL, 1, 2442),
(34, 34, '2018-12-02', '2018-12-02', '12:26:50', '12:26:55', 1, 3940),
(35, 35, '2018-12-02', NULL, '12:26:50', NULL, 1, 970),
(36, 36, '2018-12-02', NULL, '12:26:50', NULL, 1, 1232),
(37, 37, '2018-12-02', NULL, '12:26:50', NULL, 1, 2275),
(38, 38, '2018-12-02', NULL, '12:26:50', NULL, 1, 3409),
(39, 39, '2018-12-02', NULL, '12:26:50', NULL, 1, 2913),
(40, 40, '2018-12-02', NULL, '12:26:50', NULL, 1, 3155),
(41, 34, '2018-12-02', NULL, '12:26:55', NULL, -1, 3940),
(42, 6, '2018-12-02', NULL, '12:26:58', NULL, -1, 322),
(43, 23, '2018-12-02', NULL, '12:27:10', NULL, -1, 1531),
(44, 3, '2018-12-02', NULL, '12:27:16', NULL, -1, 495),
(45, 12, '2018-12-02', NULL, '12:27:19', NULL, -1, 740);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `purchase`
--

CREATE TABLE `purchase` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `date` date DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `coment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `purchase`
--

INSERT INTO `purchase` (`id`, `user_id`, `product_id`, `date`, `status`, `coment`) VALUES
(1, 2, 1, '2018-12-02', 3, 'comentario'),
(2, 2, 3, '2018-07-03', 3, 'comentario'),
(3, 3, 2, '2018-05-19', 3, 'comentario'),
(4, 2, 33, '2018-07-07', 3, 'comentario'),
(5, 4, 31, '2018-07-13', 3, 'comentario'),
(6, 2, 7, '2018-12-02', 3, 'comentario'),
(7, 2, 31, '2018-12-02', 3, 'comentario'),
(8, 3, 29, '2018-12-02', 3, 'comentario'),
(9, 3, 34, '2018-07-01', 3, 'comentario'),
(10, 4, 4, '2018-05-31', 3, 'comentario'),
(11, 3, 12, '2018-05-19', 3, 'comentario'),
(12, 2, 40, '2018-05-28', 3, 'comentario'),
(13, 3, 31, '2018-05-23', 3, 'comentario'),
(14, 3, 8, '2018-06-07', 3, 'comentario'),
(15, 4, 12, '2018-06-24', 3, 'comentario'),
(16, 2, 13, '2018-05-27', 3, 'comentario'),
(17, 3, 33, '2018-06-23', 3, 'comentario'),
(18, 1, 38, '2018-07-11', 3, 'comentario'),
(19, 3, 34, '2018-07-08', 3, 'comentario'),
(20, 1, 18, '2018-06-13', 3, 'comentario'),
(21, 1, 26, '2018-06-08', 3, 'comentario'),
(22, 1, 27, '2018-05-22', 3, 'comentario'),
(23, 2, 25, '2018-05-30', 3, 'comentario'),
(24, 2, 15, '2018-07-01', 3, 'comentario'),
(25, 1, 34, '2018-07-05', 3, 'comentario'),
(26, 1, 37, '2018-06-01', 3, 'comentario'),
(27, 3, 24, '2018-06-06', 3, 'comentario'),
(28, 4, 12, '2018-06-26', 3, 'comentario'),
(29, 4, 34, '2018-06-21', 3, 'comentario'),
(30, 2, 21, '2018-12-02', 3, 'comentario'),
(31, 2, 21, '2018-07-01', 3, 'comentario'),
(32, 3, 4, '2018-06-22', 3, 'comentario'),
(33, 4, 9, '2018-12-02', 3, 'comentario'),
(34, 1, 26, '2018-07-06', 3, 'comentario'),
(35, 1, 2, '2018-06-03', 3, 'comentario'),
(36, 4, 7, '2018-07-10', 3, 'comentario'),
(37, 4, 13, '2018-05-18', 3, 'comentario'),
(38, 1, 15, '2018-07-05', 3, 'comentario'),
(39, 3, 7, '2018-05-28', 3, 'comentario'),
(40, 3, 11, '2018-06-11', 3, 'comentario'),
(41, 2, 1, '2018-12-02', 3, 'comentario'),
(42, 2, 1, '2018-05-28', 3, 'comentario'),
(43, 1, 23, '2018-06-19', 3, 'comentario'),
(44, 3, 34, '2018-07-15', 3, 'comentario'),
(45, 4, 6, '2018-06-05', 3, 'comentario'),
(46, 3, 36, '2018-12-02', 3, 'comentario'),
(47, 4, 19, '2018-05-30', 3, 'comentario'),
(48, 1, 27, '2018-12-02', 3, 'comentario'),
(49, 4, 35, '2018-06-07', 3, 'comentario'),
(50, 4, 6, '2018-06-21', 3, 'comentario'),
(51, 2, 6, '2018-07-14', 3, 'comentario'),
(52, 2, 38, '2018-06-18', 3, 'comentario'),
(53, 2, 9, '2018-05-19', 3, 'comentario'),
(54, 3, 14, '2018-12-02', 3, 'comentario'),
(55, 4, 39, '2018-07-03', 3, 'comentario'),
(56, 3, 3, '2018-06-13', 3, 'comentario'),
(57, 1, 32, '2018-07-13', 3, 'comentario'),
(58, 3, 5, '2018-07-09', 3, 'comentario'),
(59, 3, 28, '2018-05-26', 3, 'comentario'),
(60, 1, 40, '2018-07-03', 3, 'comentario'),
(61, 2, 36, '2018-06-29', 3, 'comentario'),
(62, 2, 34, '2018-06-12', 3, 'comentario'),
(63, 1, 1, '2018-12-02', 3, 'comentario'),
(64, 1, 37, '2018-06-13', 3, 'comentario'),
(65, 3, 3, '2018-07-11', 3, 'comentario'),
(66, 1, 7, '2018-05-19', 3, 'comentario'),
(67, 3, 28, '2018-06-11', 3, 'comentario'),
(68, 4, 20, '2018-05-22', 3, 'comentario'),
(69, 1, 24, '2018-06-30', 3, 'comentario'),
(70, 1, 36, '2018-06-29', 3, 'comentario'),
(71, 2, 21, '2018-06-06', 3, 'comentario'),
(72, 3, 24, '2018-12-02', 3, 'comentario'),
(73, 3, 38, '2018-05-25', 3, 'comentario'),
(74, 4, 10, '2018-06-15', 3, 'comentario'),
(75, 1, 5, '2018-07-13', 3, 'comentario'),
(76, 2, 19, '2018-06-06', 3, 'comentario'),
(77, 1, 8, '2018-06-22', 3, 'comentario'),
(78, 1, 21, '2018-06-26', 3, 'comentario'),
(79, 2, 19, '2018-06-11', 3, 'comentario'),
(80, 2, 24, '2018-07-05', 3, 'comentario'),
(81, 2, 31, '2018-06-17', 3, 'comentario'),
(82, 4, 21, '2018-06-17', 3, 'comentario'),
(83, 1, 2, '2018-05-21', 3, 'comentario'),
(84, 3, 32, '2018-12-02', 3, 'comentario'),
(85, 2, 25, '2018-06-30', 3, 'comentario'),
(86, 1, 36, '2018-06-10', 3, 'comentario'),
(87, 4, 4, '2018-12-02', 3, 'comentario'),
(88, 2, 13, '2018-05-21', 3, 'comentario'),
(89, 3, 12, '2018-07-10', 3, 'comentario'),
(90, 4, 36, '2018-06-27', 3, 'comentario'),
(91, 3, 13, '2018-05-22', 3, 'comentario'),
(92, 4, 34, '2018-06-19', 3, 'comentario'),
(93, 2, 4, '2018-06-22', 3, 'comentario'),
(94, 3, 9, '2018-12-02', 3, 'comentario'),
(95, 1, 29, '2018-07-01', 3, 'comentario'),
(96, 4, 18, '2018-06-14', 3, 'comentario'),
(97, 4, 22, '2018-05-21', 3, 'comentario'),
(98, 4, 31, '2018-06-21', 3, 'comentario'),
(99, 3, 16, '2018-06-19', 3, 'comentario'),
(100, 2, 19, '2018-06-12', 3, 'comentario'),
(101, 4, 17, '2018-12-02', 3, 'comentario'),
(102, 4, 22, '2018-12-02', 3, 'comentario'),
(103, 2, 12, '2018-12-02', 3, 'comentario'),
(104, 2, 18, '2018-12-02', 3, 'comentario'),
(105, 3, 31, '2018-12-02', 3, 'comentario'),
(106, 1, 6, '2018-12-02', 3, 'comentario'),
(107, 3, 23, '2018-12-02', 3, 'comentario'),
(108, 1, 5, '2018-12-02', 3, 'comentario'),
(109, 3, 21, '2018-12-02', 3, 'comentario'),
(110, 2, 14, '2018-12-02', 3, 'comentario'),
(111, 1, 24, '2018-12-02', 3, 'comentario'),
(112, 2, 18, '2018-12-02', 3, 'comentario'),
(113, 2, 20, '2018-12-02', 3, 'comentario'),
(114, 2, 25, '2018-12-02', 3, 'comentario'),
(115, 2, 40, '2018-12-02', 3, 'comentario'),
(116, 3, 12, '2018-12-02', 3, 'comentario'),
(117, 1, 4, '2018-12-02', 3, 'comentario'),
(118, 1, 29, '2018-12-02', 3, 'comentario'),
(119, 1, 32, '2018-12-02', 3, 'comentario'),
(120, 4, 9, '2018-12-02', 3, 'comentario'),
(121, 1, 23, '2018-12-02', 3, 'comentario'),
(122, 4, 4, '2018-12-02', 3, 'comentario'),
(123, 1, 17, '2018-12-02', 3, 'comentario'),
(124, 4, 22, '2018-12-02', 3, 'comentario'),
(125, 1, 19, '2018-12-02', 3, 'comentario'),
(126, 4, 36, '2018-12-02', 3, 'comentario'),
(127, 3, 21, '2018-12-02', 3, 'comentario'),
(128, 3, 28, '2018-12-02', 3, 'comentario'),
(129, 1, 12, '2018-12-02', 3, 'comentario'),
(130, 3, 26, '2018-12-02', 3, 'comentario'),
(131, 1, 26, '2018-12-02', 3, 'comentario'),
(132, 4, 21, '2018-12-02', 3, 'comentario'),
(133, 1, 39, '2018-12-02', 3, 'comentario'),
(134, 3, 19, '2018-12-02', 3, 'comentario'),
(135, 1, 30, '2018-12-02', 3, 'comentario'),
(136, 3, 11, '2018-12-02', 3, 'comentario'),
(137, 3, 16, '2018-12-02', 3, 'comentario'),
(138, 1, 7, '2018-12-02', 3, 'comentario'),
(139, 2, 21, '2018-12-02', 3, 'comentario'),
(140, 3, 39, '2018-12-02', 3, 'comentario'),
(141, 1, 22, '2018-12-02', 3, 'comentario'),
(142, 2, 8, '2018-12-02', 3, 'comentario'),
(143, 4, 29, '2018-12-02', 3, 'comentario'),
(144, 1, 21, '2018-12-02', 3, 'comentario'),
(145, 3, 26, '2018-12-02', 3, 'comentario'),
(146, 4, 22, '2018-12-02', 3, 'comentario'),
(147, 2, 3, '2018-12-02', 3, 'comentario'),
(148, 3, 33, '2018-12-02', 3, 'comentario'),
(149, 2, 28, '2018-12-02', 3, 'comentario'),
(150, 4, 11, '2018-12-02', 3, 'comentario'),
(151, 1, 5, '2018-12-02', 3, 'comentario'),
(152, 1, 40, '2018-12-02', 3, 'comentario'),
(153, 1, 11, '2018-12-02', 3, 'comentario'),
(154, 4, 5, '2018-12-02', 3, 'comentario'),
(155, 1, 9, '2018-12-02', 3, 'comentario'),
(156, 3, 27, '2018-12-02', 3, 'comentario'),
(157, 3, 38, '2018-12-02', 3, 'comentario'),
(158, 3, 27, '2018-12-02', 3, 'comentario'),
(159, 1, 29, '2018-12-02', 3, 'comentario'),
(160, 3, 12, '2018-12-02', 3, 'comentario'),
(161, 3, 30, '2018-12-02', 3, 'comentario'),
(162, 1, 31, '2018-12-02', 3, 'comentario'),
(163, 1, 21, '2018-12-02', 3, 'comentario'),
(164, 2, 10, '2018-12-02', 3, 'comentario'),
(165, 1, 36, '2018-12-02', 3, 'comentario'),
(166, 4, 16, '2018-12-02', 3, 'comentario'),
(167, 4, 33, '2018-12-02', 3, 'comentario'),
(168, 4, 39, '2018-12-02', 3, 'comentario'),
(169, 2, 32, '2018-12-02', 3, 'comentario'),
(170, 2, 10, '2018-12-02', 3, 'comentario'),
(171, 1, 31, '2018-12-02', 3, 'comentario'),
(172, 1, 10, '2018-12-02', 3, 'comentario'),
(173, 4, 34, '2018-12-02', 3, 'comentario'),
(174, 3, 37, '2018-12-02', 3, 'comentario'),
(175, 4, 13, '2018-12-02', 3, 'comentario'),
(176, 4, 3, '2018-12-02', 3, 'comentario'),
(177, 4, 35, '2018-12-02', 3, 'comentario'),
(178, 4, 20, '2018-12-02', 3, 'comentario'),
(179, 2, 3, '2018-12-02', 3, 'comentario'),
(180, 2, 10, '2018-12-02', 3, 'comentario'),
(181, 2, 12, '2018-12-02', 3, 'comentario'),
(182, 1, 40, '2018-12-02', 3, 'comentario'),
(183, 2, 4, '2018-12-02', 3, 'comentario'),
(184, 1, 28, '2018-12-02', 3, 'comentario'),
(185, 2, 34, '2018-12-02', 3, 'comentario'),
(186, 4, 20, '2018-12-02', 3, 'comentario'),
(187, 2, 32, '2018-12-02', 3, 'comentario'),
(188, 2, 13, '2018-12-02', 3, 'comentario'),
(189, 2, 28, '2018-12-02', 3, 'comentario'),
(190, 4, 9, '2018-12-02', 3, 'comentario'),
(191, 1, 26, '2018-12-02', 3, 'comentario'),
(192, 2, 23, '2018-12-02', 3, 'comentario'),
(193, 2, 32, '2018-12-02', 3, 'comentario'),
(194, 4, 26, '2018-12-02', 3, 'comentario'),
(195, 3, 22, '2018-12-02', 3, 'comentario'),
(196, 1, 7, '2018-12-02', 3, 'comentario'),
(197, 4, 37, '2018-12-02', 3, 'comentario'),
(198, 2, 11, '2018-12-02', 3, 'comentario'),
(199, 2, 16, '2018-12-02', 3, 'comentario'),
(200, 2, 39, '2018-12-02', 3, 'comentario'),
(201, 3, 40, '2019-01-03', -1, ''),
(202, 3, 40, '2019-01-03', -1, ''),
(203, 3, 36, '2019-01-03', -1, ''),
(204, 3, 32, '2019-01-03', -1, ''),
(205, 2, 39, '2019-01-05', -1, ''),
(206, 2, 39, '2019-01-05', -1, ''),
(207, 2, 39, '2019-01-05', -1, ''),
(208, 2, 39, '2019-01-05', -1, ''),
(209, 2, 39, '2019-01-05', -1, ''),
(210, 2, 39, '2019-01-05', -1, ''),
(211, 2, 39, '2019-01-05', -1, ''),
(212, 2, 35, '2019-01-05', -1, ''),
(213, 2, 31, '2019-01-05', -1, ''),
(214, 2, 39, '2019-01-05', 3, 'es putazo'),
(215, 3, 38, '2019-01-05', -1, ''),
(216, 3, 38, '2019-01-05', 3, ''),
(217, 2, 39, '2019-01-05', 3, ''),
(218, 2, 39, '2019-01-05', 3, 'asdad'),
(219, 2, 39, '2019-01-05', 3, '');

--
-- Disparadores `purchase`
--
DELIMITER $$
CREATE TRIGGER `purchase_after_insert` AFTER INSERT ON `purchase` FOR EACH ROW BEGIN
  insert into purchase_states (purchase_id,date_ini,date_end,hour_ini,hour_end,state) values (NEW.id,now(),null,time(now()),null,NEW.status);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `purchase_after_update` AFTER UPDATE ON `purchase` FOR EACH ROW BEGIN
  IF (new.status = 3) THEN
    SET @id=(select product_id from purchase where id=new.id);
    update product set stock = stock-1 where id=@id;
  END IF;
  IF (new.status <> old.status) THEN
  BEGIN
    update purchase_states set date_end=now() where purchase_id=new.id and date_end is null;
    update purchase_states set hour_end=time(now()) where purchase_id=new.id and hour_end is null;
    insert into purchase_states (purchase_id,date_ini,date_end,hour_ini,hour_end,state) VALUES (new.id,now(),null,time(now()),null,new.status);
  END;
  END IF;
    
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `purchase_states`
--

CREATE TABLE `purchase_states` (
  `id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `date_ini` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `hour_ini` varchar(8) NOT NULL,
  `hour_end` varchar(8) DEFAULT NULL,
  `state` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `purchase_states`
--

INSERT INTO `purchase_states` (`id`, `purchase_id`, `date_ini`, `date_end`, `hour_ini`, `hour_end`, `state`) VALUES
(1, 1, '2018-12-02', '2018-12-02', '12:26:54', '12:26:54', 1),
(2, 1, '2018-12-02', NULL, '12:26:54', NULL, 3),
(3, 2, '2018-12-02', '2018-12-02', '12:26:54', '12:26:54', 1),
(4, 2, '2018-12-02', NULL, '12:26:54', NULL, 3),
(5, 3, '2018-12-02', '2018-12-02', '12:26:54', '12:26:54', 1),
(6, 3, '2018-12-02', NULL, '12:26:54', NULL, 3),
(7, 4, '2018-12-02', '2018-12-02', '12:26:54', '12:26:54', 1),
(8, 4, '2018-12-02', NULL, '12:26:54', NULL, 3),
(9, 5, '2018-12-02', '2018-12-02', '12:26:54', '12:26:54', 1),
(10, 5, '2018-12-02', NULL, '12:26:54', NULL, 3),
(11, 6, '2018-12-02', '2018-12-02', '12:26:54', '12:26:54', 1),
(12, 6, '2018-12-02', NULL, '12:26:54', NULL, 3),
(13, 7, '2018-12-02', '2018-12-02', '12:26:54', '12:26:55', 1),
(14, 7, '2018-12-02', NULL, '12:26:55', NULL, 3),
(15, 8, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(16, 8, '2018-12-02', NULL, '12:26:55', NULL, 3),
(17, 9, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(18, 9, '2018-12-02', NULL, '12:26:55', NULL, 3),
(19, 10, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(20, 10, '2018-12-02', NULL, '12:26:55', NULL, 3),
(21, 11, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(22, 11, '2018-12-02', NULL, '12:26:55', NULL, 3),
(23, 12, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(24, 12, '2018-12-02', NULL, '12:26:55', NULL, 3),
(25, 13, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(26, 13, '2018-12-02', NULL, '12:26:55', NULL, 3),
(27, 14, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(28, 14, '2018-12-02', NULL, '12:26:55', NULL, 3),
(29, 15, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(30, 15, '2018-12-02', NULL, '12:26:55', NULL, 3),
(31, 16, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(32, 16, '2018-12-02', NULL, '12:26:55', NULL, 3),
(33, 17, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(34, 17, '2018-12-02', NULL, '12:26:55', NULL, 3),
(35, 18, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(36, 18, '2018-12-02', NULL, '12:26:55', NULL, 3),
(37, 19, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(38, 19, '2018-12-02', NULL, '12:26:55', NULL, 3),
(39, 20, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(40, 20, '2018-12-02', NULL, '12:26:55', NULL, 3),
(41, 21, '2018-12-02', '2018-12-02', '12:26:55', '12:26:55', 1),
(42, 21, '2018-12-02', NULL, '12:26:55', NULL, 3),
(43, 22, '2018-12-02', '2018-12-02', '12:26:56', '12:26:56', 1),
(44, 22, '2018-12-02', NULL, '12:26:56', NULL, 3),
(45, 23, '2018-12-02', '2018-12-02', '12:26:56', '12:26:56', 1),
(46, 23, '2018-12-02', NULL, '12:26:56', NULL, 3),
(47, 24, '2018-12-02', '2018-12-02', '12:26:56', '12:26:56', 1),
(48, 24, '2018-12-02', NULL, '12:26:56', NULL, 3),
(49, 25, '2018-12-02', '2018-12-02', '12:26:56', '12:26:56', 1),
(50, 25, '2018-12-02', NULL, '12:26:56', NULL, 3),
(51, 26, '2018-12-02', '2018-12-02', '12:26:56', '12:26:56', 1),
(52, 26, '2018-12-02', NULL, '12:26:56', NULL, 3),
(53, 27, '2018-12-02', '2018-12-02', '12:26:56', '12:26:56', 1),
(54, 27, '2018-12-02', NULL, '12:26:56', NULL, 3),
(55, 28, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(56, 28, '2018-12-02', NULL, '12:26:57', NULL, 3),
(57, 29, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(58, 29, '2018-12-02', NULL, '12:26:57', NULL, 3),
(59, 30, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(60, 30, '2018-12-02', NULL, '12:26:57', NULL, 3),
(61, 31, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(62, 31, '2018-12-02', NULL, '12:26:57', NULL, 3),
(63, 32, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(64, 32, '2018-12-02', NULL, '12:26:57', NULL, 3),
(65, 33, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(66, 33, '2018-12-02', NULL, '12:26:57', NULL, 3),
(67, 34, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(68, 34, '2018-12-02', NULL, '12:26:57', NULL, 3),
(69, 35, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(70, 35, '2018-12-02', NULL, '12:26:57', NULL, 3),
(71, 36, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(72, 36, '2018-12-02', NULL, '12:26:57', NULL, 3),
(73, 37, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(74, 37, '2018-12-02', NULL, '12:26:57', NULL, 3),
(75, 38, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(76, 38, '2018-12-02', NULL, '12:26:57', NULL, 3),
(77, 39, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(78, 39, '2018-12-02', NULL, '12:26:57', NULL, 3),
(79, 40, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(80, 40, '2018-12-02', NULL, '12:26:57', NULL, 3),
(81, 41, '2018-12-02', '2018-12-02', '12:26:57', '12:26:57', 1),
(82, 41, '2018-12-02', NULL, '12:26:57', NULL, 3),
(83, 42, '2018-12-02', '2018-12-02', '12:26:57', '12:26:58', 1),
(84, 42, '2018-12-02', NULL, '12:26:58', NULL, 3),
(85, 43, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(86, 43, '2018-12-02', NULL, '12:26:58', NULL, 3),
(87, 44, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(88, 44, '2018-12-02', NULL, '12:26:58', NULL, 3),
(89, 45, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(90, 45, '2018-12-02', NULL, '12:26:58', NULL, 3),
(91, 46, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(92, 46, '2018-12-02', NULL, '12:26:58', NULL, 3),
(93, 47, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(94, 47, '2018-12-02', NULL, '12:26:58', NULL, 3),
(95, 48, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(96, 48, '2018-12-02', NULL, '12:26:58', NULL, 3),
(97, 49, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(98, 49, '2018-12-02', NULL, '12:26:58', NULL, 3),
(99, 50, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(100, 50, '2018-12-02', NULL, '12:26:58', NULL, 3),
(101, 51, '2018-12-02', '2018-12-02', '12:26:58', '12:26:58', 1),
(102, 51, '2018-12-02', NULL, '12:26:58', NULL, 3),
(103, 52, '2018-12-02', '2018-12-02', '12:26:58', '12:26:59', 1),
(104, 52, '2018-12-02', NULL, '12:26:59', NULL, 3),
(105, 53, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(106, 53, '2018-12-02', NULL, '12:26:59', NULL, 3),
(107, 54, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(108, 54, '2018-12-02', NULL, '12:26:59', NULL, 3),
(109, 55, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(110, 55, '2018-12-02', NULL, '12:26:59', NULL, 3),
(111, 56, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(112, 56, '2018-12-02', NULL, '12:26:59', NULL, 3),
(113, 57, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(114, 57, '2018-12-02', NULL, '12:26:59', NULL, 3),
(115, 58, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(116, 58, '2018-12-02', NULL, '12:26:59', NULL, 3),
(117, 59, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(118, 59, '2018-12-02', NULL, '12:26:59', NULL, 3),
(119, 60, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(120, 60, '2018-12-02', NULL, '12:26:59', NULL, 3),
(121, 61, '2018-12-02', '2018-12-02', '12:26:59', '12:26:59', 1),
(122, 61, '2018-12-02', NULL, '12:26:59', NULL, 3),
(123, 62, '2018-12-02', '2018-12-02', '12:26:59', '12:27:00', 1),
(124, 62, '2018-12-02', NULL, '12:27:00', NULL, 3),
(125, 63, '2018-12-02', '2018-12-02', '12:27:00', '12:27:00', 1),
(126, 63, '2018-12-02', NULL, '12:27:00', NULL, 3),
(127, 64, '2018-12-02', '2018-12-02', '12:27:00', '12:27:00', 1),
(128, 64, '2018-12-02', NULL, '12:27:00', NULL, 3),
(129, 65, '2018-12-02', '2018-12-02', '12:27:00', '12:27:00', 1),
(130, 65, '2018-12-02', NULL, '12:27:00', NULL, 3),
(131, 66, '2018-12-02', '2018-12-02', '12:27:00', '12:27:00', 1),
(132, 66, '2018-12-02', NULL, '12:27:00', NULL, 3),
(133, 67, '2018-12-02', '2018-12-02', '12:27:00', '12:27:00', 1),
(134, 67, '2018-12-02', NULL, '12:27:00', NULL, 3),
(135, 68, '2018-12-02', '2018-12-02', '12:27:00', '12:27:00', 1),
(136, 68, '2018-12-02', NULL, '12:27:00', NULL, 3),
(137, 69, '2018-12-02', '2018-12-02', '12:27:00', '12:27:00', 1),
(138, 69, '2018-12-02', NULL, '12:27:00', NULL, 3),
(139, 70, '2018-12-02', '2018-12-02', '12:27:00', '12:27:00', 1),
(140, 70, '2018-12-02', NULL, '12:27:00', NULL, 3),
(141, 71, '2018-12-02', '2018-12-02', '12:27:00', '12:27:01', 1),
(142, 71, '2018-12-02', NULL, '12:27:01', NULL, 3),
(143, 72, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(144, 72, '2018-12-02', NULL, '12:27:01', NULL, 3),
(145, 73, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(146, 73, '2018-12-02', NULL, '12:27:01', NULL, 3),
(147, 74, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(148, 74, '2018-12-02', NULL, '12:27:01', NULL, 3),
(149, 75, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(150, 75, '2018-12-02', NULL, '12:27:01', NULL, 3),
(151, 76, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(152, 76, '2018-12-02', NULL, '12:27:01', NULL, 3),
(153, 77, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(154, 77, '2018-12-02', NULL, '12:27:01', NULL, 3),
(155, 78, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(156, 78, '2018-12-02', NULL, '12:27:01', NULL, 3),
(157, 79, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(158, 79, '2018-12-02', NULL, '12:27:01', NULL, 3),
(159, 80, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(160, 80, '2018-12-02', NULL, '12:27:01', NULL, 3),
(161, 81, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(162, 81, '2018-12-02', NULL, '12:27:01', NULL, 3),
(163, 82, '2018-12-02', '2018-12-02', '12:27:01', '12:27:01', 1),
(164, 82, '2018-12-02', NULL, '12:27:01', NULL, 3),
(165, 83, '2018-12-02', '2018-12-02', '12:27:01', '12:27:02', 1),
(166, 83, '2018-12-02', NULL, '12:27:02', NULL, 3),
(167, 84, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(168, 84, '2018-12-02', NULL, '12:27:02', NULL, 3),
(169, 85, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(170, 85, '2018-12-02', NULL, '12:27:02', NULL, 3),
(171, 86, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(172, 86, '2018-12-02', NULL, '12:27:02', NULL, 3),
(173, 87, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(174, 87, '2018-12-02', NULL, '12:27:02', NULL, 3),
(175, 88, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(176, 88, '2018-12-02', NULL, '12:27:02', NULL, 3),
(177, 89, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(178, 89, '2018-12-02', NULL, '12:27:02', NULL, 3),
(179, 90, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(180, 90, '2018-12-02', NULL, '12:27:02', NULL, 3),
(181, 91, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(182, 91, '2018-12-02', NULL, '12:27:02', NULL, 3),
(183, 92, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(184, 92, '2018-12-02', NULL, '12:27:02', NULL, 3),
(185, 93, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(186, 93, '2018-12-02', NULL, '12:27:02', NULL, 3),
(187, 94, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(188, 94, '2018-12-02', NULL, '12:27:02', NULL, 3),
(189, 95, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(190, 95, '2018-12-02', NULL, '12:27:02', NULL, 3),
(191, 96, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(192, 96, '2018-12-02', NULL, '12:27:02', NULL, 3),
(193, 97, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(194, 97, '2018-12-02', NULL, '12:27:02', NULL, 3),
(195, 98, '2018-12-02', '2018-12-02', '12:27:02', '12:27:02', 1),
(196, 98, '2018-12-02', NULL, '12:27:02', NULL, 3),
(197, 99, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(198, 99, '2018-12-02', NULL, '12:27:03', NULL, 3),
(199, 100, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(200, 100, '2018-12-02', NULL, '12:27:03', NULL, 3),
(201, 101, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(202, 101, '2018-12-02', NULL, '12:27:03', NULL, 3),
(203, 102, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(204, 102, '2018-12-02', NULL, '12:27:03', NULL, 3),
(205, 103, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(206, 103, '2018-12-02', NULL, '12:27:03', NULL, 3),
(207, 104, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(208, 104, '2018-12-02', NULL, '12:27:03', NULL, 3),
(209, 105, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(210, 105, '2018-12-02', NULL, '12:27:03', NULL, 3),
(211, 106, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(212, 106, '2018-12-02', NULL, '12:27:03', NULL, 3),
(213, 107, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(214, 107, '2018-12-02', NULL, '12:27:03', NULL, 3),
(215, 108, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(216, 108, '2018-12-02', NULL, '12:27:03', NULL, 3),
(217, 109, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(218, 109, '2018-12-02', NULL, '12:27:03', NULL, 3),
(219, 110, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(220, 110, '2018-12-02', NULL, '12:27:03', NULL, 3),
(221, 111, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(222, 111, '2018-12-02', NULL, '12:27:03', NULL, 3),
(223, 112, '2018-12-02', '2018-12-02', '12:27:03', '12:27:03', 1),
(224, 112, '2018-12-02', NULL, '12:27:03', NULL, 3),
(225, 113, '2018-12-02', '2018-12-02', '12:27:03', '12:27:04', 1),
(226, 113, '2018-12-02', NULL, '12:27:04', NULL, 3),
(227, 114, '2018-12-02', '2018-12-02', '12:27:04', '12:27:04', 1),
(228, 114, '2018-12-02', NULL, '12:27:04', NULL, 3),
(229, 115, '2018-12-02', '2018-12-02', '12:27:04', '12:27:04', 1),
(230, 115, '2018-12-02', NULL, '12:27:04', NULL, 3),
(231, 116, '2018-12-02', '2018-12-02', '12:27:04', '12:27:04', 1),
(232, 116, '2018-12-02', NULL, '12:27:04', NULL, 3),
(233, 117, '2018-12-02', '2018-12-02', '12:27:04', '12:27:04', 1),
(234, 117, '2018-12-02', NULL, '12:27:04', NULL, 3),
(235, 118, '2018-12-02', '2018-12-02', '12:27:04', '12:27:04', 1),
(236, 118, '2018-12-02', NULL, '12:27:04', NULL, 3),
(237, 119, '2018-12-02', '2018-12-02', '12:27:04', '12:27:04', 1),
(238, 119, '2018-12-02', NULL, '12:27:04', NULL, 3),
(239, 120, '2018-12-02', '2018-12-02', '12:27:04', '12:27:04', 1),
(240, 120, '2018-12-02', NULL, '12:27:04', NULL, 3),
(241, 121, '2018-12-02', '2018-12-02', '12:27:04', '12:27:04', 1),
(242, 121, '2018-12-02', NULL, '12:27:04', NULL, 3),
(243, 122, '2018-12-02', '2018-12-02', '12:27:04', '12:27:05', 1),
(244, 122, '2018-12-02', NULL, '12:27:05', NULL, 3),
(245, 123, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(246, 123, '2018-12-02', NULL, '12:27:05', NULL, 3),
(247, 124, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(248, 124, '2018-12-02', NULL, '12:27:05', NULL, 3),
(249, 125, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(250, 125, '2018-12-02', NULL, '12:27:05', NULL, 3),
(251, 126, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(252, 126, '2018-12-02', NULL, '12:27:05', NULL, 3),
(253, 127, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(254, 127, '2018-12-02', NULL, '12:27:05', NULL, 3),
(255, 128, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(256, 128, '2018-12-02', NULL, '12:27:05', NULL, 3),
(257, 129, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(258, 129, '2018-12-02', NULL, '12:27:05', NULL, 3),
(259, 130, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(260, 130, '2018-12-02', NULL, '12:27:05', NULL, 3),
(261, 131, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(262, 131, '2018-12-02', NULL, '12:27:05', NULL, 3),
(263, 132, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(264, 132, '2018-12-02', NULL, '12:27:05', NULL, 3),
(265, 133, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(266, 133, '2018-12-02', NULL, '12:27:05', NULL, 3),
(267, 134, '2018-12-02', '2018-12-02', '12:27:05', '12:27:05', 1),
(268, 134, '2018-12-02', NULL, '12:27:05', NULL, 3),
(269, 135, '2018-12-02', '2018-12-02', '12:27:05', '12:27:06', 1),
(270, 135, '2018-12-02', NULL, '12:27:06', NULL, 3),
(271, 136, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(272, 136, '2018-12-02', NULL, '12:27:06', NULL, 3),
(273, 137, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(274, 137, '2018-12-02', NULL, '12:27:06', NULL, 3),
(275, 138, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(276, 138, '2018-12-02', NULL, '12:27:06', NULL, 3),
(277, 139, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(278, 139, '2018-12-02', NULL, '12:27:06', NULL, 3),
(279, 140, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(280, 140, '2018-12-02', NULL, '12:27:06', NULL, 3),
(281, 141, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(282, 141, '2018-12-02', NULL, '12:27:06', NULL, 3),
(283, 142, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(284, 142, '2018-12-02', NULL, '12:27:06', NULL, 3),
(285, 143, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(286, 143, '2018-12-02', NULL, '12:27:06', NULL, 3),
(287, 144, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(288, 144, '2018-12-02', NULL, '12:27:06', NULL, 3),
(289, 145, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(290, 145, '2018-12-02', NULL, '12:27:06', NULL, 3),
(291, 146, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(292, 146, '2018-12-02', NULL, '12:27:06', NULL, 3),
(293, 147, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(294, 147, '2018-12-02', NULL, '12:27:06', NULL, 3),
(295, 148, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(296, 148, '2018-12-02', NULL, '12:27:06', NULL, 3),
(297, 149, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(298, 149, '2018-12-02', NULL, '12:27:06', NULL, 3),
(299, 150, '2018-12-02', '2018-12-02', '12:27:06', '12:27:06', 1),
(300, 150, '2018-12-02', NULL, '12:27:06', NULL, 3),
(301, 151, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(302, 151, '2018-12-02', NULL, '12:27:07', NULL, 3),
(303, 152, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(304, 152, '2018-12-02', NULL, '12:27:07', NULL, 3),
(305, 153, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(306, 153, '2018-12-02', NULL, '12:27:07', NULL, 3),
(307, 154, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(308, 154, '2018-12-02', NULL, '12:27:07', NULL, 3),
(309, 155, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(310, 155, '2018-12-02', NULL, '12:27:07', NULL, 3),
(311, 156, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(312, 156, '2018-12-02', NULL, '12:27:07', NULL, 3),
(313, 157, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(314, 157, '2018-12-02', NULL, '12:27:07', NULL, 3),
(315, 158, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(316, 158, '2018-12-02', NULL, '12:27:07', NULL, 3),
(317, 159, '2018-12-02', '2018-12-02', '12:27:07', '12:27:07', 1),
(318, 159, '2018-12-02', NULL, '12:27:07', NULL, 3),
(319, 160, '2018-12-02', '2018-12-02', '12:27:08', '12:27:08', 1),
(320, 160, '2018-12-02', NULL, '12:27:08', NULL, 3),
(321, 161, '2018-12-02', '2018-12-02', '12:27:08', '12:27:08', 1),
(322, 161, '2018-12-02', NULL, '12:27:08', NULL, 3),
(323, 162, '2018-12-02', '2018-12-02', '12:27:08', '12:27:08', 1),
(324, 162, '2018-12-02', NULL, '12:27:08', NULL, 3),
(325, 163, '2018-12-02', '2018-12-02', '12:27:08', '12:27:08', 1),
(326, 163, '2018-12-02', NULL, '12:27:08', NULL, 3),
(327, 164, '2018-12-02', '2018-12-02', '12:27:08', '12:27:08', 1),
(328, 164, '2018-12-02', NULL, '12:27:08', NULL, 3),
(329, 165, '2018-12-02', '2018-12-02', '12:27:08', '12:27:08', 1),
(330, 165, '2018-12-02', NULL, '12:27:08', NULL, 3),
(331, 166, '2018-12-02', '2018-12-02', '12:27:08', '12:27:08', 1),
(332, 166, '2018-12-02', NULL, '12:27:08', NULL, 3),
(333, 167, '2018-12-02', '2018-12-02', '12:27:08', '12:27:08', 1),
(334, 167, '2018-12-02', NULL, '12:27:08', NULL, 3),
(335, 168, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(336, 168, '2018-12-02', NULL, '12:27:09', NULL, 3),
(337, 169, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(338, 169, '2018-12-02', NULL, '12:27:09', NULL, 3),
(339, 170, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(340, 170, '2018-12-02', NULL, '12:27:09', NULL, 3),
(341, 171, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(342, 171, '2018-12-02', NULL, '12:27:09', NULL, 3),
(343, 172, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(344, 172, '2018-12-02', NULL, '12:27:09', NULL, 3),
(345, 173, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(346, 173, '2018-12-02', NULL, '12:27:09', NULL, 3),
(347, 174, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(348, 174, '2018-12-02', NULL, '12:27:09', NULL, 3),
(349, 175, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(350, 175, '2018-12-02', NULL, '12:27:09', NULL, 3),
(351, 176, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(352, 176, '2018-12-02', NULL, '12:27:09', NULL, 3),
(353, 177, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(354, 177, '2018-12-02', NULL, '12:27:09', NULL, 3),
(355, 178, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(356, 178, '2018-12-02', NULL, '12:27:09', NULL, 3),
(357, 179, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(358, 179, '2018-12-02', NULL, '12:27:09', NULL, 3),
(359, 180, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(360, 180, '2018-12-02', NULL, '12:27:09', NULL, 3),
(361, 181, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(362, 181, '2018-12-02', NULL, '12:27:09', NULL, 3),
(363, 182, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(364, 182, '2018-12-02', NULL, '12:27:09', NULL, 3),
(365, 183, '2018-12-02', '2018-12-02', '12:27:09', '12:27:09', 1),
(366, 183, '2018-12-02', NULL, '12:27:09', NULL, 3),
(367, 184, '2018-12-02', '2018-12-02', '12:27:09', '12:27:10', 1),
(368, 184, '2018-12-02', NULL, '12:27:10', NULL, 3),
(369, 185, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(370, 185, '2018-12-02', NULL, '12:27:10', NULL, 3),
(371, 186, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(372, 186, '2018-12-02', NULL, '12:27:10', NULL, 3),
(373, 187, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(374, 187, '2018-12-02', NULL, '12:27:10', NULL, 3),
(375, 188, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(376, 188, '2018-12-02', NULL, '12:27:10', NULL, 3),
(377, 189, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(378, 189, '2018-12-02', NULL, '12:27:10', NULL, 3),
(379, 190, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(380, 190, '2018-12-02', NULL, '12:27:10', NULL, 3),
(381, 191, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(382, 191, '2018-12-02', NULL, '12:27:10', NULL, 3),
(383, 192, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(384, 192, '2018-12-02', NULL, '12:27:10', NULL, 3),
(385, 193, '2018-12-02', '2018-12-02', '12:27:10', '12:27:10', 1),
(386, 193, '2018-12-02', NULL, '12:27:10', NULL, 3),
(387, 194, '2018-12-02', '2018-12-02', '12:27:11', '12:27:11', 1),
(388, 194, '2018-12-02', NULL, '12:27:11', NULL, 3),
(389, 195, '2018-12-02', '2018-12-02', '12:27:11', '12:27:11', 1),
(390, 195, '2018-12-02', NULL, '12:27:11', NULL, 3),
(391, 196, '2018-12-02', '2018-12-02', '12:27:11', '12:27:11', 1),
(392, 196, '2018-12-02', NULL, '12:27:11', NULL, 3),
(393, 197, '2018-12-02', '2018-12-02', '12:27:11', '12:27:11', 1),
(394, 197, '2018-12-02', NULL, '12:27:11', NULL, 3),
(395, 198, '2018-12-02', '2018-12-02', '12:27:11', '12:27:11', 1),
(396, 198, '2018-12-02', NULL, '12:27:11', NULL, 3),
(397, 199, '2018-12-02', '2018-12-02', '12:27:11', '12:27:11', 1),
(398, 199, '2018-12-02', NULL, '12:27:11', NULL, 3),
(399, 200, '2018-12-02', '2018-12-02', '12:27:11', '12:27:11', 1),
(400, 200, '2018-12-02', NULL, '12:27:11', NULL, 3),
(401, 201, '2019-01-03', '2019-01-03', '05:14:23', '05:39:18', 1),
(402, 201, '2019-01-03', NULL, '05:39:18', NULL, -1),
(403, 202, '2019-01-03', '2019-01-03', '14:54:57', '16:06:02', 1),
(404, 203, '2019-01-03', '2019-01-03', '15:42:48', '16:06:00', 1),
(405, 204, '2019-01-03', '2019-01-03', '15:42:48', '16:05:59', 1),
(406, 204, '2019-01-03', NULL, '16:05:59', NULL, -1),
(407, 203, '2019-01-03', NULL, '16:06:00', NULL, -1),
(408, 202, '2019-01-03', NULL, '16:06:02', NULL, -1),
(409, 205, '2019-01-05', '2019-01-05', '03:22:47', '03:25:44', 1),
(410, 205, '2019-01-05', NULL, '03:25:44', NULL, -1),
(411, 206, '2019-01-05', '2019-01-05', '03:28:27', '03:29:51', 1),
(412, 206, '2019-01-05', NULL, '03:29:51', NULL, -1),
(413, 207, '2019-01-05', '2019-01-05', '03:41:03', '03:42:56', 1),
(414, 207, '2019-01-05', NULL, '03:42:56', NULL, -1),
(415, 208, '2019-01-05', '2019-01-05', '03:43:48', '03:44:01', 1),
(416, 208, '2019-01-05', NULL, '03:44:01', NULL, -1),
(417, 209, '2019-01-05', '2019-01-05', '03:46:28', '03:46:36', 1),
(418, 209, '2019-01-05', NULL, '03:46:36', NULL, -1),
(419, 210, '2019-01-05', '2019-01-05', '03:47:24', '03:47:41', 1),
(420, 210, '2019-01-05', NULL, '03:47:41', NULL, -1),
(421, 211, '2019-01-05', '2019-01-05', '03:48:20', '03:49:16', 1),
(422, 212, '2019-01-05', '2019-01-05', '03:48:20', '03:49:07', 1),
(423, 213, '2019-01-05', '2019-01-05', '03:48:39', '03:49:04', 1),
(424, 213, '2019-01-05', NULL, '03:49:04', NULL, -1),
(425, 212, '2019-01-05', NULL, '03:49:07', NULL, -1),
(426, 211, '2019-01-05', NULL, '03:49:16', NULL, -1),
(427, 214, '2019-01-05', '2019-01-05', '03:52:21', '03:52:59', 1),
(428, 214, '2019-01-05', '2019-01-05', '03:52:59', '04:21:22', 2),
(429, 215, '2019-01-05', '2019-01-05', '04:19:21', '04:19:52', 1),
(430, 215, '2019-01-05', NULL, '04:19:52', NULL, -1),
(431, 216, '2019-01-05', '2019-01-05', '04:20:44', '04:50:43', 1),
(432, 214, '2019-01-05', NULL, '04:21:22', NULL, 3),
(433, 217, '2019-01-05', '2019-01-05', '04:25:39', '04:47:08', 1),
(434, 217, '2019-01-05', '2019-01-05', '04:47:08', '04:47:39', 2),
(435, 217, '2019-01-05', NULL, '04:47:39', NULL, 3),
(436, 216, '2019-01-05', '2019-01-05', '04:50:43', '05:10:36', 2),
(437, 216, '2019-01-05', NULL, '05:10:36', NULL, 3),
(438, 218, '2019-01-05', '2019-01-05', '05:14:34', '05:14:53', 1),
(439, 218, '2019-01-05', '2019-01-05', '05:14:53', '05:16:21', 2),
(440, 218, '2019-01-05', NULL, '05:16:21', NULL, 3),
(441, 219, '2019-01-05', '2019-01-05', '15:29:54', '15:30:25', 1),
(442, 219, '2019-01-05', '2019-01-05', '15:30:25', '15:30:48', 2),
(443, 219, '2019-01-05', NULL, '15:30:48', NULL, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `purchase_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `review` text NOT NULL,
  `stars` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `review`
--

INSERT INTO `review` (`id`, `product_id`, `user_id`, `purchase_id`, `date`, `review`, `stars`) VALUES
(1, 1, 2, 1, '2018-12-02', 'Excelente', 5),
(2, 3, 2, 2, '2018-12-02', 'bueno', 2),
(3, 2, 3, 3, '2018-12-02', 'Excelente', 5),
(4, 33, 2, 4, '2018-12-02', 'muy bueno', 3),
(5, 31, 4, 5, '2018-12-02', 'muy bueno', 3),
(6, 7, 2, 6, '2018-12-02', 'malo', 1),
(7, 31, 2, 7, '2018-12-02', 'malo', 1),
(8, 29, 3, 8, '2018-12-02', 'malo', 1),
(9, 34, 3, 9, '2018-12-02', 'Excelente', 5),
(10, 4, 4, 10, '2018-12-02', 'muy bueno', 3),
(11, 12, 3, 11, '2018-12-02', 'recomendable', 4),
(12, 40, 2, 12, '2018-12-02', 'bueno', 2),
(13, 31, 3, 13, '2018-12-02', 'bueno', 2),
(14, 8, 3, 14, '2018-12-02', 'Excelente', 5),
(15, 12, 4, 15, '2018-12-02', 'muy bueno', 3),
(16, 13, 2, 16, '2018-12-02', 'malo', 1),
(17, 33, 3, 17, '2018-12-02', 'malo', 1),
(18, 38, 1, 18, '2018-12-02', 'recomendable', 4),
(19, 34, 3, 19, '2018-12-02', 'malo', 1),
(20, 18, 1, 20, '2018-12-02', 'muy bueno', 3),
(21, 26, 1, 21, '2018-12-02', 'malo', 1),
(22, 27, 1, 22, '2018-12-02', 'Excelente', 5),
(23, 25, 2, 23, '2018-12-02', 'Excelente', 5),
(24, 15, 2, 24, '2018-12-02', 'malo', 1),
(25, 34, 1, 25, '2018-12-02', 'Excelente', 5),
(26, 37, 1, 26, '2018-12-02', 'recomendable', 4),
(27, 24, 3, 27, '2018-12-02', 'muy bueno', 3),
(28, 12, 4, 28, '2018-12-02', 'recomendable', 4),
(29, 34, 4, 29, '2018-12-02', 'bueno', 2),
(30, 21, 2, 30, '2018-12-02', 'recomendable', 4),
(31, 21, 2, 31, '2018-12-02', 'Excelente', 5),
(32, 4, 3, 32, '2018-12-02', 'muy bueno', 3),
(33, 9, 4, 33, '2018-12-02', 'recomendable', 4),
(34, 26, 1, 34, '2018-12-02', 'Excelente', 5),
(35, 2, 1, 35, '2018-12-02', 'bueno', 2),
(36, 7, 4, 36, '2018-12-02', 'recomendable', 4),
(37, 13, 4, 37, '2018-12-02', 'muy bueno', 3),
(38, 15, 1, 38, '2018-12-02', 'malo', 1),
(39, 7, 3, 39, '2018-12-02', 'Excelente', 5),
(40, 11, 3, 40, '2018-12-02', 'bueno', 2),
(41, 1, 2, 41, '2018-12-02', 'muy bueno', 3),
(42, 1, 2, 42, '2018-12-02', 'Excelente', 5),
(43, 23, 1, 43, '2018-12-02', 'muy bueno', 3),
(44, 34, 3, 44, '2018-12-02', 'malo', 1),
(45, 6, 4, 45, '2018-12-02', 'malo', 1),
(46, 36, 3, 46, '2018-12-02', 'muy bueno', 3),
(47, 19, 4, 47, '2018-12-02', 'bueno', 2),
(48, 27, 1, 48, '2018-12-02', 'muy bueno', 3),
(49, 35, 4, 49, '2018-12-02', 'malo', 1),
(50, 6, 4, 50, '2018-12-02', 'bueno', 2),
(51, 6, 2, 51, '2018-12-02', 'malo', 1),
(52, 38, 2, 52, '2018-12-02', 'Excelente', 5),
(53, 9, 2, 53, '2018-12-02', 'bueno', 2),
(54, 14, 3, 54, '2018-12-02', 'muy bueno', 3),
(55, 39, 4, 55, '2018-12-02', 'muy bueno', 3),
(56, 3, 3, 56, '2018-12-02', 'muy bueno', 3),
(57, 32, 1, 57, '2018-12-02', 'recomendable', 4),
(58, 5, 3, 58, '2018-12-02', 'Excelente', 5),
(59, 28, 3, 59, '2018-12-02', 'recomendable', 4),
(60, 40, 1, 60, '2018-12-02', 'Excelente', 5),
(61, 36, 2, 61, '2018-12-02', 'Excelente', 5),
(62, 34, 2, 62, '2018-12-02', 'Excelente', 5),
(63, 1, 1, 63, '2018-12-02', 'malo', 1),
(64, 37, 1, 64, '2018-12-02', 'muy bueno', 3),
(65, 3, 3, 65, '2018-12-02', 'Excelente', 5),
(66, 7, 1, 66, '2018-12-02', 'muy bueno', 3),
(67, 28, 3, 67, '2018-12-02', 'malo', 1),
(68, 20, 4, 68, '2018-12-02', 'recomendable', 4),
(69, 24, 1, 69, '2018-12-02', 'recomendable', 4),
(70, 36, 1, 70, '2018-12-02', 'Excelente', 5),
(71, 21, 2, 71, '2018-12-02', 'bueno', 2),
(72, 24, 3, 72, '2018-12-02', 'recomendable', 4),
(73, 38, 3, 73, '2018-12-02', 'muy bueno', 3),
(74, 10, 4, 74, '2018-12-02', 'bueno', 2),
(75, 5, 1, 75, '2018-12-02', 'Excelente', 5),
(76, 19, 2, 76, '2018-12-02', 'malo', 1),
(77, 8, 1, 77, '2018-12-02', 'malo', 1),
(78, 21, 1, 78, '2018-12-02', 'Excelente', 5),
(79, 19, 2, 79, '2018-12-02', 'recomendable', 4),
(80, 24, 2, 80, '2018-12-02', 'Excelente', 5),
(81, 31, 2, 81, '2018-12-02', 'recomendable', 4),
(82, 21, 4, 82, '2018-12-02', 'malo', 1),
(83, 2, 1, 83, '2018-12-02', 'recomendable', 4),
(84, 32, 3, 84, '2018-12-02', 'muy bueno', 3),
(85, 25, 2, 85, '2018-12-02', 'recomendable', 4),
(86, 36, 1, 86, '2018-12-02', 'recomendable', 4),
(87, 4, 4, 87, '2018-12-02', 'Excelente', 5),
(88, 13, 2, 88, '2018-12-02', 'recomendable', 4),
(89, 12, 3, 89, '2018-12-02', 'recomendable', 4),
(90, 36, 4, 90, '2018-12-02', 'malo', 1),
(91, 13, 3, 91, '2018-12-02', 'recomendable', 4),
(92, 34, 4, 92, '2018-12-02', 'malo', 1),
(93, 4, 2, 93, '2018-12-02', 'recomendable', 4),
(94, 9, 3, 94, '2018-12-02', 'bueno', 2),
(95, 29, 1, 95, '2018-12-02', 'muy bueno', 3),
(96, 18, 4, 96, '2018-12-02', 'bueno', 2),
(97, 22, 4, 97, '2018-12-02', 'muy bueno', 3),
(98, 31, 4, 98, '2018-12-02', 'malo', 1),
(99, 16, 3, 99, '2018-12-02', 'recomendable', 4),
(100, 19, 2, 100, '2018-12-02', 'muy bueno', 3),
(101, 17, 4, 101, '2018-12-02', 'Excelente', 5),
(102, 22, 4, 102, '2018-12-02', 'muy bueno', 3),
(103, 12, 2, 103, '2018-12-02', 'bueno', 2),
(104, 18, 2, 104, '2018-12-02', 'malo', 1),
(105, 31, 3, 105, '2018-12-02', 'recomendable', 4),
(106, 6, 1, 106, '2018-12-02', 'bueno', 2),
(107, 23, 3, 107, '2018-12-02', 'Excelente', 5),
(108, 5, 1, 108, '2018-12-02', 'bueno', 2),
(109, 21, 3, 109, '2018-12-02', 'malo', 1),
(110, 14, 2, 110, '2018-12-02', 'recomendable', 4),
(111, 24, 1, 111, '2018-12-02', 'recomendable', 4),
(112, 18, 2, 112, '2018-12-02', 'bueno', 2),
(113, 20, 2, 113, '2018-12-02', 'muy bueno', 3),
(114, 25, 2, 114, '2018-12-02', 'muy bueno', 3),
(115, 40, 2, 115, '2018-12-02', 'recomendable', 4),
(116, 12, 3, 116, '2018-12-02', 'Excelente', 5),
(117, 4, 1, 117, '2018-12-02', 'muy bueno', 3),
(118, 29, 1, 118, '2018-12-02', 'muy bueno', 3),
(119, 32, 1, 119, '2018-12-02', 'recomendable', 4),
(120, 9, 4, 120, '2018-12-02', 'recomendable', 4),
(121, 23, 1, 121, '2018-12-02', 'Excelente', 5),
(122, 4, 4, 122, '2018-12-02', 'bueno', 2),
(123, 17, 1, 123, '2018-12-02', 'Excelente', 5),
(124, 22, 4, 124, '2018-12-02', 'recomendable', 4),
(125, 19, 1, 125, '2018-12-02', 'muy bueno', 3),
(126, 36, 4, 126, '2018-12-02', 'malo', 1),
(127, 21, 3, 127, '2018-12-02', 'recomendable', 4),
(128, 28, 3, 128, '2018-12-02', 'muy bueno', 3),
(129, 12, 1, 129, '2018-12-02', 'recomendable', 4),
(130, 26, 3, 130, '2018-12-02', 'muy bueno', 3),
(131, 26, 1, 131, '2018-12-02', 'recomendable', 4),
(132, 21, 4, 132, '2018-12-02', 'recomendable', 4),
(133, 39, 1, 133, '2018-12-02', 'Excelente', 5),
(134, 19, 3, 134, '2018-12-02', 'malo', 1),
(135, 30, 1, 135, '2018-12-02', 'Excelente', 5),
(136, 11, 3, 136, '2018-12-02', 'muy bueno', 3),
(137, 16, 3, 137, '2018-12-02', 'bueno', 2),
(138, 7, 1, 138, '2018-12-02', 'recomendable', 4),
(139, 21, 2, 139, '2018-12-02', 'bueno', 2),
(140, 39, 3, 140, '2018-12-02', 'recomendable', 4),
(141, 22, 1, 141, '2018-12-02', 'malo', 1),
(142, 8, 2, 142, '2018-12-02', 'bueno', 2),
(143, 29, 4, 143, '2018-12-02', 'recomendable', 4),
(144, 21, 1, 144, '2018-12-02', 'Excelente', 5),
(145, 26, 3, 145, '2018-12-02', 'bueno', 2),
(146, 22, 4, 146, '2018-12-02', 'bueno', 2),
(147, 3, 2, 147, '2018-12-02', 'muy bueno', 3),
(148, 33, 3, 148, '2018-12-02', 'recomendable', 4),
(149, 28, 2, 149, '2018-12-02', 'recomendable', 4),
(150, 11, 4, 150, '2018-12-02', 'malo', 1),
(151, 5, 1, 151, '2018-12-02', 'malo', 1),
(152, 40, 1, 152, '2018-12-02', 'malo', 1),
(153, 11, 1, 153, '2018-12-02', 'muy bueno', 3),
(154, 5, 4, 154, '2018-12-02', 'bueno', 2),
(155, 9, 1, 155, '2018-12-02', 'recomendable', 4),
(156, 27, 3, 156, '2018-12-02', 'bueno', 2),
(157, 38, 3, 157, '2018-12-02', 'muy bueno', 3),
(158, 27, 3, 158, '2018-12-02', 'Excelente', 5),
(159, 29, 1, 159, '2018-12-02', 'bueno', 2),
(160, 12, 3, 160, '2018-12-02', 'recomendable', 4),
(161, 30, 3, 161, '2018-12-02', 'bueno', 2),
(162, 31, 1, 162, '2018-12-02', 'muy bueno', 3),
(163, 21, 1, 163, '2018-12-02', 'malo', 1),
(164, 10, 2, 164, '2018-12-02', 'malo', 1),
(165, 36, 1, 165, '2018-12-02', 'malo', 1),
(166, 16, 4, 166, '2018-12-02', 'bueno', 2),
(167, 33, 4, 167, '2018-12-02', 'malo', 1),
(168, 39, 4, 168, '2018-12-02', 'malo', 1),
(169, 32, 2, 169, '2018-12-02', 'bueno', 2),
(170, 10, 2, 170, '2018-12-02', 'malo', 1),
(171, 31, 1, 171, '2018-12-02', 'muy bueno', 3),
(172, 10, 1, 172, '2018-12-02', 'bueno', 2),
(173, 34, 4, 173, '2018-12-02', 'malo', 1),
(174, 37, 3, 174, '2018-12-02', 'Excelente', 5),
(175, 13, 4, 175, '2018-12-02', 'bueno', 2),
(176, 3, 4, 176, '2018-12-02', 'Excelente', 5),
(177, 35, 4, 177, '2018-12-02', 'Excelente', 5),
(178, 20, 4, 178, '2018-12-02', 'Excelente', 5),
(179, 3, 2, 179, '2018-12-02', 'malo', 1),
(180, 10, 2, 180, '2018-12-02', 'muy bueno', 3),
(181, 12, 2, 181, '2018-12-02', 'Excelente', 5),
(182, 40, 1, 182, '2018-12-02', 'malo', 1),
(183, 4, 2, 183, '2018-12-02', 'recomendable', 4),
(184, 28, 1, 184, '2018-12-02', 'malo', 1),
(185, 34, 2, 185, '2018-12-02', 'Excelente', 5),
(186, 20, 4, 186, '2018-12-02', 'muy bueno', 3),
(187, 32, 2, 187, '2018-12-02', 'bueno', 2),
(188, 13, 2, 188, '2018-12-02', 'muy bueno', 3),
(189, 28, 2, 189, '2018-12-02', 'recomendable', 4),
(190, 9, 4, 190, '2018-12-02', 'malo', 1),
(191, 26, 1, 191, '2018-12-02', 'Excelente', 5),
(192, 23, 2, 192, '2018-12-02', 'Excelente', 5),
(193, 32, 2, 193, '2018-12-02', 'Excelente', 5),
(194, 26, 4, 194, '2018-12-02', 'Excelente', 5),
(195, 22, 3, 195, '2018-12-02', 'muy bueno', 3),
(196, 7, 1, 196, '2018-12-02', 'recomendable', 4),
(197, 37, 4, 197, '2018-12-02', 'bueno', 2),
(198, 11, 2, 198, '2018-12-02', 'Excelente', 5),
(199, 16, 2, 199, '2018-12-02', 'bueno', 2),
(200, 39, 2, 200, '2018-12-02', 'malo', 1),
(201, 39, 2, 214, '2019-01-05', 'caca', 2),
(202, 39, 2, 217, '2019-01-05', '223', 2),
(203, 38, 3, 216, '2019-01-05', '', 2),
(204, 39, 2, 218, '2019-01-05', 'asdc', 1),
(205, 39, 2, 219, '2019-01-05', '', 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `sales`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `sales` (
`product_id` int(11)
,`cantidad` bigint(21)
,`name` varchar(100)
,`category_id` int(11)
,`user_id` int(11)
,`category` varchar(100)
,`date_end` date
,`type` varchar(45)
,`info` text
,`price` float
,`img` varchar(50)
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL COMMENT 'nombre',
  `mail` varchar(100) NOT NULL COMMENT 'email',
  `pass` varchar(50) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `sub_state` int(11) DEFAULT NULL COMMENT 'estado de la suscripcion'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `name`, `mail`, `pass`, `status`, `sub_state`) VALUES
(1, 'John Gallo', 'jhon.gallo1994@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1),
(2, 'German Perez', 'gnpjoestar@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1),
(3, 'Matias Gazzani', 'matiasgazzani@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1),
(4, 'Jorge Gonzalez', 'jorgegonzalez.csi@gmail.com', '81dc9bdb52d04dc20036dbd8313ed055', 1, 1);

--
-- Disparadores `user`
--
DELIMITER $$
CREATE TRIGGER `state_change` AFTER INSERT ON `user` FOR EACH ROW BEGIN
insert into user_states (user_id,date_ini,date_end,hour_ini,hour_end,state) values(NEW.id,now(),null,time(now()),null,NEW.status);
END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `state_change2` AFTER UPDATE ON `user` FOR EACH ROW BEGIN
    IF new.status <> old.status THEN BEGIN
        update user_states set date_end=now() where user_id=NEW.id and date_end is null;
		update user_states set hour_end=time(now()) where user_id=id and hour_end is null;
		insert into user_states (user_id,date_ini,date_end,hour_ini,hour_end,state) VALUES (NEW.id,now(),null,time(now()),null,NEW.status);
    END; END IF;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_states`
--

CREATE TABLE `user_states` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_ini` date NOT NULL,
  `date_end` date DEFAULT NULL,
  `hour_ini` varchar(8) NOT NULL,
  `hour_end` varchar(8) DEFAULT NULL,
  `state` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `user_states`
--

INSERT INTO `user_states` (`id`, `user_id`, `date_ini`, `date_end`, `hour_ini`, `hour_end`, `state`) VALUES
(1, 1, '2018-12-02', NULL, '12:26:48', NULL, 1),
(2, 2, '2018-12-02', NULL, '12:26:48', NULL, 1),
(3, 3, '2018-12-02', NULL, '12:26:48', NULL, 1),
(4, 4, '2018-12-02', NULL, '12:26:48', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura para la vista `sales`
--
DROP TABLE IF EXISTS `sales`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `sales`  AS  select `purchase`.`product_id` AS `product_id`,count(`purchase`.`product_id`) AS `cantidad`,`product`.`name` AS `name`,`product`.`category_id` AS `category_id`,`product`.`user_id` AS `user_id`,`category`.`name` AS `category`,`product`.`date_end` AS `date_end`,`product`.`type` AS `type`,`product`.`info` AS `info`,`product`.`price` AS `price`,`product`.`img` AS `img` from (((`product` join `purchase`) join `category`) join `user`) where ((`purchase`.`product_id` = `product`.`id`) and (`product`.`category_id` = `category`.`id`) and (`product`.`user_id` = `user`.`id`) and (`user`.`status` = 1) and (`purchase`.`status` = 3) and (`product`.`status` = 1)) group by `purchase`.`product_id` order by count(`purchase`.`product_id`) desc ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `favorite`
--
ALTER TABLE `favorite`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `UNIQUE` (`id`,`user_id`),
  ADD KEY `fk_favorite_product1_idx` (`product_id`),
  ADD KEY `fk_favorite_user1_idx` (`user_id`);

--
-- Indices de la tabla `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_local_user1_idx` (`user_id`);

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`,`user_id`,`local_id`,`category_id`),
  ADD KEY `fk_product_user1_idx` (`user_id`),
  ADD KEY `fk_product_category1_idx` (`category_id`),
  ADD KEY `fk_product_local1_idx` (`local_id`);

--
-- Indices de la tabla `product_updates`
--
ALTER TABLE `product_updates`
  ADD PRIMARY KEY (`id`,`product_id`),
  ADD KEY `fk_product_updates_product1_idx` (`product_id`);

--
-- Indices de la tabla `purchase`
--
ALTER TABLE `purchase`
  ADD PRIMARY KEY (`id`,`user_id`,`product_id`),
  ADD KEY `fk_purchase_user1_idx` (`user_id`),
  ADD KEY `fk_purchase_product1_idx` (`product_id`);

--
-- Indices de la tabla `purchase_states`
--
ALTER TABLE `purchase_states`
  ADD PRIMARY KEY (`id`,`purchase_id`),
  ADD KEY `fk_purchase_states_purchase1_idx` (`purchase_id`);

--
-- Indices de la tabla `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`,`product_id`,`user_id`,`purchase_id`),
  ADD KEY `fk1_idx` (`product_id`),
  ADD KEY `fk2_idx` (`user_id`),
  ADD KEY `fk3_idx` (`purchase_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mail` (`mail`);

--
-- Indices de la tabla `user_states`
--
ALTER TABLE `user_states`
  ADD PRIMARY KEY (`id`,`user_id`),
  ADD KEY `fk_states_user_idx` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `favorite`
--
ALTER TABLE `favorite`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `local`
--
ALTER TABLE `local`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT de la tabla `product_updates`
--
ALTER TABLE `product_updates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT de la tabla `purchase`
--
ALTER TABLE `purchase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=220;

--
-- AUTO_INCREMENT de la tabla `purchase_states`
--
ALTER TABLE `purchase_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=444;

--
-- AUTO_INCREMENT de la tabla `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=206;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `user_states`
--
ALTER TABLE `user_states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `favorite`
--
ALTER TABLE `favorite`
  ADD CONSTRAINT `fk_favorite_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_favorite_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `local`
--
ALTER TABLE `local`
  ADD CONSTRAINT `fk_local_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `fk_product_category1` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_local1` FOREIGN KEY (`local_id`) REFERENCES `local` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_product_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `product_updates`
--
ALTER TABLE `product_updates`
  ADD CONSTRAINT `fk_product_updates_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `purchase`
--
ALTER TABLE `purchase`
  ADD CONSTRAINT `fk_purchase_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_purchase_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `purchase_states`
--
ALTER TABLE `purchase_states`
  ADD CONSTRAINT `fk_purchase_states_purchase1` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `review`
--
ALTER TABLE `review`
  ADD CONSTRAINT `fk_review_product1` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_review_use1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fkfk_review_purchase1` FOREIGN KEY (`purchase_id`) REFERENCES `purchase` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user_states`
--
ALTER TABLE `user_states`
  ADD CONSTRAINT `fk_states_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

DELIMITER $$
--
-- Eventos
--
CREATE DEFINER=`root`@`localhost` EVENT `event_available_control` ON SCHEDULE EVERY 1 SECOND STARTS '2018-06-07 11:51:57' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'Control de Disponibilidad' DO update product set product.status = '-1' where (product.date_end < curdate() or product.stock='0')$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
