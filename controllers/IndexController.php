<?php
class IndexController
    {
        private $config;
        private $view;

        function __construct()
        {
            //Creamos una instancia de nuestro mini motor de plantillas
            $this->view = new View();
            $this->config = Config_::singleton();                
        }

        private function userLogued()
        {
            session_start();
            if (!isset($_SESSION['USER']))
            {
                header("Location:index.php?MSG=userisnotlogued");
            }
        }
        
        public function viewIndex() 
        {            
            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController();

            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();

            require $this->config->get('modelsFolder').'/ProductModel.php'; 
            $product = new ProductModel();

            require $this->config->get('modelsFolder').'/FavoriteModel.php'; 
            $allFavorite = new FavoriteModel();

            @session_start();
            if (isset($_SESSION['USER']))
            {
                $idUser = $_SESSION['USER']['IDUSER'];
            }
            else
            {
                $idUser = 999;   
            }

            $data = array("category"=>$category->allCategory(),
                          "besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allProduct(),
                          "productToEnd"=>$product->getProductProxToEnd(),
                          "bestProductToWeek"=>$product->getBestProductToWeek(),
                          "bestSeller"=>$product->getProductBestSeller(),
                          "bestOfCategory"=>$product->getProductBestOfCategory(),
                          "allFavorite"=>$allFavorite->getFavoriteFromId($idUser)
                        );
            //traemos el contenido y lo mostramos
            $this->view->show("index", $data);
        }//fin index
    }
?>