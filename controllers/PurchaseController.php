<?php
class PurchaseController
    {
        private $config;
        private $view;

        function __construct()
        {
            //Creamos una instancia de nuestro mini motor de plantillas
            $this->view = new View();
            $this->config = Config_::singleton();                
        }

        private function userLogued()
        {
            session_start();
            if (!isset($_SESSION['USER']))
            {
                header("Location:index.php?MSG=userisnotlogued");
            }
        }

        public function endPurchase() 
        {               
          //Incluye el modelo que corresponde
          require $this->config->get('modelsFolder').'/PurchaseModel.php'; 
          $purchase = new PurchaseModel();

          require $this->config->get('controllersFolder').'/CartController.php';
          $cart = new CartController();  

          $productCart = $cart->allProduct();

          foreach ($productCart as $key => $value) {              
              if ($key != 'articulos_total' && $key != 'totalPrice')
              {
                  $purchase->setPurchase($productCart[$key]['idUser'],$productCart[$key]['id']);
              }
          }
          $cart->destroy();
        } 

        public function viewMyPurchase()
        {
            $this->userLogued();

            $idUser = $_SESSION['USER']['IDUSER'];

            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController(); 

            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();

            require $this->config->get('modelsFolder').'/PurchaseModel.php'; 
            $purchase = new PurchaseModel();

            $data = array("besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allProduct(),
                          "myPurchaseProduct"=>$purchase->getMyPurchase($idUser)
                      );

            //traemos el contenido y lo mostramos
            $this->view->show("mypurchase", $data);
        }//fin agregar

        public function setAddComent()
        {
            require $this->config->get('modelsFolder').'/PurchaseModel.php'; 
            $purchase = new PurchaseModel();
            $purchase->setAddComent($_GET['idUser'],$_GET['idProduct'],$_GET['idPurchase'],$_GET['coment'],$_GET['stars']);
        }

        public function setClosePurchase()
        {
            require $this->config->get('modelsFolder').'/PurchaseModel.php'; 
            $purchase = new PurchaseModel();
            $purchase->setClosePurchase($_GET['idPurchase']);
        }

    }//fin add Purchase  
?>