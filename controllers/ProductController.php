<?php
class ProductController
    {
        private $config;
        private $view;

        function __construct()
        {
            //Creamos una instancia de nuestro mini motor de plantillas
            $this->view = new View();
            $this->config = Config_::singleton();                
        }

        private function userLogued()
        {
            session_start();
            if (!isset($_SESSION['USER']))
            {
                header("Location:index.php?MSG=userisnotlogued");
            }
        }

        public function viewProduct() 
        {   
            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();   

            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController(); 
            
            require $this->config->get('modelsFolder').'/ProductModel.php'; 
            $allproduct = new ProductModel();

            require $this->config->get('modelsFolder').'/FavoriteModel.php'; 
            $allFavorite = new FavoriteModel();

            @session_start();
            if (isset($_SESSION['USER']))
            {
                $idUser = $_SESSION['USER']['IDUSER'];
            }
            else
            {
                $idUser = 999;   
            }

            $dataFilter['filterName'] = (isset($_GET['name'])) ? $_GET['name'] : '-1';
            $dataFilter['filterpriceMin'] = (isset($_GET['price_min'])) ? $_GET['price_min'] : '-1';
            $dataFilter['filterpriceMax'] = (isset($_GET['price_max'])) ? $_GET['price_max'] : '-1';
            $dataFilter['filterCategory'] = (isset($_GET['ProductByCategory'])) ? $_GET['ProductByCategory'] : '-1';
            $dataFilter['filterType'] 	  = (isset($_GET['ProductByType'])) ? $_GET['ProductByType'] : '-1';

            $data = array("category"=>$category->allCategory(),
                          "besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allProduct(),
                          "allProduct"=>$allproduct->getProductFilter($dataFilter),
                          "allFavorite"=>$allFavorite->getFavoriteFromId($idUser)
                      );
             //traemos el contenido y lo mostramos
            $this->view->show("product", $data);

        }//fin index

        public function getProductByCategory() 
        {   
            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();   

            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController(); 
            
            require $this->config->get('modelsFolder').'/ProductModel.php'; 
            $allproduct = new ProductModel();

            require $this->config->get('modelsFolder').'/FavoriteModel.php'; 
            $allFavorite = new FavoriteModel();

            @session_start();
            if (isset($_SESSION['USER']))
            {
                $idUser = $_SESSION['USER']['IDUSER'];
            }
            else
            {
                $idUser = 999;   
            }

            $dataFilter['filterName'] 	  = (isset($_GET['name'])) ? $_GET['name'] : '-1';
            $dataFilter['filterpriceMin'] = (isset($_GET['price_min'])) ? $_GET['price_min'] : '-1';
            $dataFilter['filterpriceMax'] = (isset($_GET['price_max'])) ? $_GET['price_max'] : '-1';
            $dataFilter['filterCategory'] = (isset($_GET['ProductByCategory'])) ? $_GET['ProductByCategory'] : '-1';
            $dataFilter['filterType'] 	  = (isset($_GET['ProductByType'])) ? $_GET['ProductByType'] : '-1';

            $data = array("category"=>$category->allCategory(),
                          "besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allProduct(),
                          "allProduct"=>$allproduct->getProductFilter($dataFilter),
                          "allFavorite"=>$allFavorite->getFavoriteFromId($idUser)
                      );
             //traemos el contenido y lo mostramos
            $this->view->show("product", $data);

        }//fin getProductByCategory

        public function addProduct()
        {
            $this->userLogued();

            //Incluye el modelo que corresponde
            require $this->config->get('modelsFolder').'/ProductModel.php'; 
            $items = new ProductModel();

            $target_dir = $this->config->get('imgProductFolder').'/';
            $img_name = basename($_FILES["picture"]["name"]);
            $target_file = $target_dir . $img_name;
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

            // Check file size
            if ($_FILES["picture"]["size"] > 500000) {
                header('Location:index.php?MSG=IMGSize');
            }
            // Allow certain file formats
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif" ) {
                header('Location:index.php?MSG=IMGFormat');
            }

            if (!move_uploaded_file($_FILES["picture"]["tmp_name"], $target_file)) {
                header('Location:index.php?MSG=IMGError');
            }

            $data = array('idUser'=>$_POST['idUser'],
                        'name'=>$_POST['name'],
                        'type'=>$_POST['type'],
                        'date_ini'=>$_POST['start'],
                        'date_end'=>$_POST['end'],
                        'price'=>$_POST['price'],
                        'quantity'=>$_POST['quantity'],
                        'info'=>$_POST['description'],
                        'img'=>'product/'.$img_name,
                        'status'=>1,
                        'local'=>$_POST['local'],
                        'category_id'=>$_POST['category']);
            //Le insertamos un nuevo
            $items->addProduct($data);

            header('Location:index.php?MSG=NewProductOK');
        }//fin agregar

        public function viewAddProduct()
        {
            $this->userLogued();

            $idUser = $_SESSION['USER']['IDUSER'];
            //Incluye el modelo que corresponde
            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController(); 

            require $this->config->get('controllersFolder').'/LocalController.php'; 
            $local = new LocalController();

            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();   

            $data = array("category"=>$category->allCategory(),
                          "besCategory"=>$category->bestCategory(),
                          "local"=>$local->allLocal($idUser),
                          "cartProduct"=>$cartProduct->allProduct()
                      );

            //traemos el contenido y lo mostramos
            $this->view->show("addproduct", $data);
        }//fin agregar

        public function getProductById()
        {
            //Incluye el modelo que corresponde
            require $this->config->get('modelsFolder').'/ProductModel.php'; 
            $product = new ProductModel();

            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();   

            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController();

            require $this->config->get('modelsFolder').'/FavoriteModel.php'; 
            $allFavorite = new FavoriteModel();

            @session_start();
            if (isset($_SESSION['USER']))
            {
                $idUser = $_SESSION['USER']['IDUSER'];
            }
            else
            {
                $idUser = 999;   
            } 
            
            $data = array("category"=>$category->allCategory(),
                          "besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allProduct(),
                          "productDetail"=>$product->getProductById($_GET['ProductById']),
                          "reviewProduct"=>$product->getReviewProductById($_GET['ProductById']),
                          "allSimilarProduct"=>$product->getSimilarProductByIdProduct($_GET['ProductById']),
                          "allFavorite"=>$allFavorite->getFavoriteFromId($idUser)
                      );
             //traemos el contenido y lo mostramos
            $this->view->show("productdetail", $data);
        }
        
        public function getProductProxToEnd()
        {
             //Incluye el modelo que corresponde
            require $this->config->get('modelsFolder').'/ProductModel.php'; 
            $product = new ProductModel();
            return $product->getProductProxToEnd();
        }

        public function getProductBestSalle()
        {
             //Incluye el modelo que corresponde
            require $this->config->get('modelsFolder').'/ProductModel.php'; 
            $product = new ProductModel();
            return $product->getProductBestSalle();
        }
    }
?>