<?php
class SaleController
    {
        private $config;
        private $view;

        function __construct()
        {
            //Creamos una instancia de nuestro mini motor de plantillas
            $this->view = new View();
            $this->config = Config_::singleton();                
        }

        private function userLogued()
        {
            session_start();
            if (!isset($_SESSION['USER']))
            {
                header("Location:index.php?MSG=userisnotlogued");
            }
        }
        
        public function viewMySale()
        {
            $this->userLogued();

            $idUser = $_SESSION['USER']['IDUSER'];

            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController(); 

            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();

            require $this->config->get('modelsFolder').'/SaleModel.php'; 
            $mysale = new SaleModel();

            $data = array("besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allProduct(),
                          "mySaleProduct"=>$mysale->getMySale($idUser)
                      );

            //traemos el contenido y lo mostramos
            $this->view->show("mysale", $data);
        }//fin agregar

        public function setCloseSale()
        {
            require $this->config->get('modelsFolder').'/SaleModel.php'; 
            $SaleModel = new SaleModel();
            $SaleModel->setCloseSales($_GET['idSale'],$_GET['coment']);
        }
    }
?>