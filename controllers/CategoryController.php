<?php
class CategoryController
{
    private $config;
    private $view;

    function __construct()
    {
        //Creamos una instancia de nuestro mini motor de plantillas
        $this->view = new View();
        $this->config = Config_::singleton();

        require $this->config->get('modelsFolder').'/CategoryModel.php'; 
        
        $this->category = new CategoryModel();               
    }

    private function userLogued()
    {
        session_start();
        if (!isset($_SESSION['USER']))
        {
            header("Location:index.php?MSG=userisnotlogued");
        }
    }

	public function allCategory()
	{
		
        return $this->category->allCategory();
	}

	public function bestCategory()
	{
	    return $this->category->bestCategory();
	}
}
?>