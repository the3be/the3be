<?php 
class LocalController
{
	private $config;
	private $view;

	function __construct()
	{
		//Creamos una instancia de nuestro mini motor de plantillas
		$this->view = new View();
		$this->config = Config_::singleton();                
	}

	private function userLogued()
	{
		session_start();
		if (!isset($_SESSION['USER']))
		{
		    header("Location:index.php?MSG=userisnotlogued");
		}
	}
	public function allLocal($idUser)
	{
		//Incluye el modelo que corresponde
		require $this->config->get('modelsFolder').'/LocalModel.php'; 
		//Creamos una instancia de nuestro "modelo"
		$items = new LocalModel();
		//Le insertamos un nuevo
        return $items->allLocal($idUser);
	}

	public function setLocal($data)
	{
		//Incluye el modelo que corresponde
		require $this->config->get('modelsFolder').'/LocalModel.php'; 
		//Creamos una instancia de nuestro "modelo"
		$items = new LocalModel();
		//Le insertamos un nuevo
        return $items->setLocal($data);
	}
}
?>