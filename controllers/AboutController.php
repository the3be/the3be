<?php
class AboutController
    {
        function __construct()
        {
            //Creamos una instancia de nuestro mini motor de plantillas
            $this->view = new View();                  
        }
     
        public function viewAbout() 
        {               
            require 'controllers/CategoryController.php'; 
            $category = new CategoryController();

            require 'controllers/CartController.php'; 
            $cartProduct = new CartController();

            $data = array("category"=>$category->allCategory(),
                          "besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allproduct()
                        );
            //traemos el contenido y lo mostramos
            $this->view->show("about", $data);
        }//fin about
    }
?>