<?php
class ContactController
    {
        private $config;
        private $view;

        function __construct()
        {
            //Creamos una instancia de nuestro mini motor de plantillas
            $this->view = new View();
            $this->config = Config_::singleton();                
        }

        private function userLogued()
        {
            session_start();
            if (!isset($_SESSION['USER']))
            {
                header("Location:index.php?MSG=userisnotlogued");
            }
        }
        
        public function viewContact() 
        {       
            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController();

            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();

            $data = array("category"=>$category->allCategory(),
                          "besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allProduct()
                        );
            //traemos el contenido y lo mostramos
            $this->view->show("contact", $data);
        }//fin contact
    }
?>