<?php 
class FavoriteController
{
    private $config;
    private $view;

    function __construct()
    {
        //Creamos una instancia de nuestro mini motor de plantillas
        $this->view = new View();
        $this->config = Config_::singleton();                
    }

    private function userLogued()
    {
        session_start();
        if (!isset($_SESSION['USER']))
        {
            header("Location:index.php?MSG=userisnotlogued");
        }
    }
	public function setFavorite($newFavorite)
	{
		//Incluye el modelo que corresponde
		require $this->config->get('modelsFolder').'/FavoriteModel.php'; 
		//Creamos una instancia de nuestro "modelo"
		$items = new FavoriteModel();
		//Le insertamos un nuevo
        return $items->setFavorite($newFavorite);
	}
    public function getMyFavorite()
    {

        $this->userLogued();

        require $this->config->get('controllersFolder').'/CartController.php'; 
        $cartProduct = new CartController();   

        require $this->config->get('controllersFolder').'/CategoryController.php'; 
        $category = new CategoryController(); 

        require $this->config->get('modelsFolder').'/FavoriteModel.php'; 
        $allFavorite = new FavoriteModel();

        @session_start();
        if (isset($_SESSION['USER']))
        {
            $idUser = $_SESSION['USER']['IDUSER'];
        }

        $data = array("category"=>$category->allCategory(),
                      "besCategory"=>$category->bestCategory(),
                      "cartProduct"=>$cartProduct->allProduct(),
                      "allFavorite"=>$allFavorite->getFavoriteFromId($idUser),
                      "myFavorite"=>$allFavorite->getMyFavorite($idUser)
                  );
         //traemos el contenido y lo mostramos
        $this->view->show("myfavorite", $data);
    }
}
?>