<?php
@session_start();
class CartController
{
	//aquí guardamos el contenido del cart
	private $cart = array();
 
	//seteamos el cart exista o no exista en el constructor
	public function __construct()
	{
		if(!isset($_SESSION["CART"]))
		{
			$_SESSION["CART"] = null;
			$this->cart["totalPrice"] = 0;
			$this->cart["articulos_total"] = 0;
		}else
		{
			$this->cart = $_SESSION["CART"];			
		}

		//Creamos una instancia de nuestro mini motor de plantillas
		$this->view = new View();   
	}
    private function userLogued()
    {
        @session_start();
        if (!isset($_SESSION['USER']))
        {
            header("Location:index.php?MSG=userisnotlogued");
        }
    }
	
	public function viewCart() 
    {   
    	$this->userLogued();

        require 'controllers/CategoryController.php'; 
        $category = new CategoryController();

        require 'controllers/ProductController.php'; 
        $product = new ProductController();

        $data = array("category"=>$category->allCategory(),
                      "besCategory"=>$category->bestCategory(),
                      "cartProduct"=>$this->allProduct(),
                      "productToEnd"=>$product->getProductProxToEnd()
                    );
        //traemos el contenido y lo mostramos
        $this->view->show("cart", $data);
	}//fin index

	//añadimos un product al cart
	public function addProduct($articulo = array())
	{
		$this->userLogued();

		//debemos crear un identificador único para cada product
		$unique_id = md5($articulo["id"]);

		//creamos la id única para el product
		$articulo["unique_id"] = $unique_id;

		//si no está vacío el cart lo recorremos 
		if(!empty($this->cart))
		{
			 foreach ($this->cart as $row) 
			 {
				 //comprobamos si este product ya estaba en el 
				 //cart para actualizar el product o insertar
				 //un nuevo product	
				 if($row["unique_id"] === $unique_id)
				 {
				 //si ya estaba alertamos de que solo puede comprar un cupon a la ves
				 	echo "error_409";			 	
				 }
			 }
		}
		//evitamos que nos pongan números negativos y que sólo sean números y precio		
		$articulo["precio"] = trim(preg_replace('/([^0-9\.])/i', '', $articulo["precio"]));
		//añadimos un elemento total al array cart para 
		//saber el precio total de la suma de este artículo
		//primero debemos eliminar el product si es que estaba en el cart
		$this->unsetProduct($unique_id);
		///ahora añadimos el product al cart
		$_SESSION["CART"][$unique_id] = $articulo;
		//actualizamos el cart
		$this->updateCart();
		//actualizamos el precio total y el número de artículos del cart
		//una vez hemos añadido el product
		$this->updatePrice(); 
 	} 
	//método que actualiza el precio total
	//de products total del cart
	private function updatePrice()
	{
		//seteamos las variables precio y artículos a 0
		$precio = 0;
		$articulos = 0;		 
		//recorrecmos el contenido del cart para actualizar
		//el precio total y el número de artículos
		foreach ($this->cart as $row) 
		{
			@$precio += $row['precio'];

			if (@count($row['precio'])>0) {
				$articulos += @count($row['precio']);
			}

		}
		//asignamos a articulos_total el número de artículos actual
		//y al precio el precio actual
		$_SESSION["CART"]["articulos_total"] = $articulos;
		$_SESSION["CART"]["totalPrice"] = $precio;		 
		//refrescamos él contenido del cart para que quedé actualizado
		$this->updateCart();
	}	 
	//método que retorna el precio total del cart
	public function totalPrice()
	{
		$this->userLogued();
		//si no está definido el elemento totalPrice o no existe el cart
		//el precio total será 0
		if(!isset($this->cart["totalPrice"]) || $this->cart === null)
		{
			return 0;
		}
		//en otro caso devolvemos el precio total del cart
		return $this->cart["totalPrice"] ? $this->cart["totalPrice"] : 0;
	}
	//este método retorna el contenido del cart
	public function allProduct()
	{
		$this->updatePrice();
		//asignamos el cart a una variable
		$cart = $this->cart;
		//debemos eliminar del cart el número de artículos
		//y el precio total para poder mostrar bien los artículos
		//ya que estos datos los devuelven los métodos 
		//totalPrice		 
		//unset($cart["totalPrice"]);
		return $cart == null ? null : $cart;
	}		 
	//método que llamamos al insertar un nuevo product al 
	//cart para eliminarlo si existia, así podemos insertarlo
	//de nuevo pero actualizado
	private function unsetProduct($unique_id)
	{
		unset($_SESSION["CART"][$unique_id]);
	}
	//para eliminar un product debemos pasar la clave única
	//que contiene cada uno de ellos
	public function removeProduct($unique_id)
	{
		$this->userLogued();
		//en otro caso, eliminamos el product, actualizamos el cart y 
		//el precio totale del cart
		$idProduct = md5($unique_id["idProduct"]);
		unset($_SESSION["CART"][$idProduct]);
		$this->updateCart();
		$this->updatePrice();
		return true;
	}	 
	//eliminamos el contenido del cart por completo
	public function destroy()
	{
		unset($_SESSION["CART"]);
		$this->cart = null;
		return true;
	}	 
	//actualizamos el contenido del cart
	public function updateCart()
	{
		self::__construct();
	}
}//fin clase cart