<?php
class EmailController
    {
    private $config;
    private $view;

    function __construct()
    {
        //Creamos una instancia de nuestro mini motor de plantillas
        //$this->view = new View();
        $this->config = Config_::singleton();

    }

    public function SendSales($idUser)
    {
        require $this->config->get('modelsFolder').'/EmailModel.php'; 
        $EmailModel = new EmailModel();        

        require $this->config->get('modelsFolder').'/SaleModel.php'; 
        $SaleModel = new SaleModel();

        $sales = $SaleModel->getAllSales($idUser);

        $this->bodySale = $this->config->get('htmlFolder').'/mail_purchase_element.html';
        //cargamos el html del template
        $this->templatebodySale = file_get_contents($this->bodySale);

        $this->bodySaleGeneral = $this->config->get('htmlFolder').'/mail_purchase.html';
        //cargamos el html del template
        $this->templatebodySaleGeneral = file_get_contents($this->bodySaleGeneral);

        $setTemplateSale = '';

        $localIMG = array(
                        array(
                            'address'=>$this->config->get('htmlFolder').'/images/heading-pages-01.jpg',
                            'cid'=>'heading-pages-01',
                            'name'=>'heading-pages-01.jpg'
                            ),
                        array(
                            'address'=>$this->config->get('htmlFolder').'/images/icons/logo.png',
                            'cid'=>'logo',
                            'name'=>'logo.png'
                        )
                    );

        foreach ($sales as $key => $detailSale) {  

                $img    = substr($detailSale['img'],8,-4);
                $imgExt = substr($detailSale['img'],8);
                $imgs   =  array(
                                'address'=>$this->config->get('imgProductFolder').'/'.$imgExt,
                                'cid'=>$img,
                                'name'=>$imgExt
                            );

                array_push($localIMG,$imgs);

                $setTemplateSale.= $this->templatebodySale;
                $setTemplateSale = $this->setFields('{IMG_PRODUCTO}',$img , $setTemplateSale);
                $setTemplateSale = $this->setFields('{NOMBRE_PRODUCTO}', $detailSale['product'],  $setTemplateSale);
                $setTemplateSale = $this->setFields('{NOMBRE_VENDEDOR}', $detailSale['seller'],  $setTemplateSale);
                $setTemplateSale = $this->setFields('{EMAIL_VENDEDOR}', $detailSale['mail_seller'],  $setTemplateSale);
                $setTemplateSale = $this->setFields('{DIRECCION_VENDEDOR}', $detailSale['address'],  $setTemplateSale);
                $setTemplateSale = $this->setFields('{HORARIO_VENDEDOR}', $detailSale['schedule'],  $setTemplateSale);
                $setTemplateSale = $this->setFields('{TELEFONO_VENDEDOR}', $detailSale['phone'],  $setTemplateSale);
                $setTemplateSale = $this->setFields('{ID_PRODUCTO}', $detailSale['id_product'],  $setTemplateSale);

                $this->mailClient = array($detailSale['mail']);
        }        

        $EmailModel->SubjectMail        = 'Te felicitamos por tu compra!!';
        $EmailModel->BodyMail           = $this->setFields('{VENTAS}', $setTemplateSale,  $this->templatebodySaleGeneral);
        $EmailModel->Destination        = $this->mailClient;
        $EmailModel->AddEmbeddedImage   = $localIMG;    

        var_dump($EmailModel->send());
        die();     
    }

    public function SendMailList()
    {
        require $this->config->get('modelsFolder').'/EmailModel.php'; 
        $EmailModel = new EmailModel();        

        require $this->config->get('modelsFolder').'/UserModel.php'; 
        $UserModel = new UserModel();
        $usersMailList = $UserModel->getAllMailList();

        require $this->config->get('modelsFolder').'/ProductModel.php'; 
        $ProductModel = new ProductModel();
        $productMailList = $ProductModel->getNewProducts();
        

        $this->bodyMailList = $this->config->get('htmlFolder').'/mail_list_element.html';
        //cargamos el html del template
        $this->templatebodyMailList = file_get_contents($this->bodyMailList);

        $this->bodyMailListGeneral = $this->config->get('htmlFolder').'/mail_list.html';
        //cargamos el html del template
        $this->templatebodyMailListGeneral = file_get_contents($this->bodyMailListGeneral);

        $setTemplateMailList = '';

        $localIMG = array(
                        array(
                            'address'=>$this->config->get('htmlFolder').'/images/mail_lister.jpg',
                            'cid'=>'mail_lister',
                            'name'=>'mail_lister.jpg'
                            ),
                        array(
                            'address'=>$this->config->get('htmlFolder').'/images/icons/logo.png',
                            'cid'=>'logo',
                            'name'=>'logo.png'
                        )
                    );

        foreach ($productMailList as $key => $detailMailList) {  

                $img    = substr($detailMailList['img'],8,-4);
                $imgExt = substr($detailMailList['img'],8);
                $imgs   =  array(
                                'address'=>$this->config->get('imgProductFolder').'/'.$imgExt,
                                'cid'=>$img,
                                'name'=>$imgExt
                            );

                array_push($localIMG,$imgs);

                $setTemplateMailList.= $this->templatebodyMailList;
                $setTemplateMailList = $this->setFields('{IMG_PRODUCTO}',$img , $setTemplateMailList);
                $setTemplateMailList = $this->setFields('{NOMBRE_PRODUCTO}', $detailMailList['name'],$setTemplateMailList);
                $setTemplateMailList = $this->setFields('{ID_PRODUCTO}', $detailMailList['id'],$setTemplateMailList);

                
        }        

        $EmailModel->SubjectMail        = 'Nuevos Productos para esta semana!!';
        $EmailModel->BodyMail           = $this->setFields('{PRODUCTOS_NUEVOS}', $setTemplateMailList,  $this->templatebodyMailListGeneral);
        $EmailModel->AddEmbeddedImage   = $localIMG;
        $EmailModel->Destination        = array("threebeses@gmail.com");
        $EmailModel->emailCopy          = $usersMailList;    

        $EmailModel->send();     
    }


    public function SendRegister($mailUser)
    {
        require $this->config->get('modelsFolder').'/EmailModel.php'; 
        $EmailModel = new EmailModel();        

        $this->bodyMailRegister = $this->config->get('htmlFolder').'/mail_register_user.html';
        //cargamos el html del template
        $this->templatebodyMailRegister = file_get_contents($this->bodyMailRegister);

        $setTemplateSale = '';

        $localIMG = array(
                        array(
                            'address'=>$this->config->get('htmlFolder').'/images/heading-pages-05_.jpg',
                            'cid'=>'heading-pages-05',
                            'name'=>'heading-pages-05.jpg'
                            ),
                        array(
                            'address'=>$this->config->get('htmlFolder').'/images/icons/logo.png',
                            'cid'=>'logo',
                            'name'=>'logo.png'
                        )
                    );

        $EmailModel->SubjectMail        = 'Bienvenido a The3Be!!!';
        $EmailModel->BodyMail           = $this->setFields('{MAIL_USUARIO}',$mailUser , $this->templatebodyMailRegister);
        $EmailModel->Destination        = array($mailUser);
        $EmailModel->AddEmbeddedImage   = $localIMG;    

        $EmailModel->send();     
    }

    public function SendMailContact($data)
    {
        require $this->config->get('modelsFolder').'/EmailModel.php'; 
        $EmailModel = new EmailModel();        

        $EmailModel->SubjectMail        = 'Solicitud de contacto';
        $EmailModel->BodyMail           = 'Comentario de ' . $data['name'] .' <br><br>';
        $EmailModel->BodyMail           .= $data['comment'] . '<br><br> ';
        $EmailModel->BodyMail           .= $data['phone'] . '<br><br>';
        $EmailModel->BodyMail           .= $data['email'];
        $EmailModel->Destination        = array('threebeses@gmail.com');
        $EmailModel->AddEmbeddedImage   = array();

        $EmailModel->send();     
    }


    private function setFields($field, $value, $template)
    { 
        return str_replace($field, $value, $template);
    }//fin setFields
}//end class

 ?>