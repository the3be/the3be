<?php
class UsersController
    {
        private $config;
        private $view;

        function __construct()
        {
            //Creamos una instancia de nuestro mini motor de plantillas
            $this->view = new View();
            $this->config = Config_::singleton();                
        }

        private function userLogued()
        {
            session_start();
            if (!isset($_SESSION['USER']))
            {
                header("Location:index.php?MSG=userisnotlogued");
            }
        }

        public function loginUser() 
        {               
            //Incluye el modelo que corresponde
           require $this->config->get('modelsFolder').'/UserModel.php'; 
           //Creamos una instancia de nuestro "modelo"
           $items = new UserModel();

           $data = array('mail'=>$_POST['maill'],'pass'=>$_POST['passl']);
           //enviamos los parametros
           $loginUser = $items->loginUser($data);
           if (count($loginUser)!=0)
           {
            @session_start();
            $_SESSION['USER']['IDUSER']     = $loginUser[0]['id'];
            $_SESSION['USER']['NAMEUSER']   = $loginUser[0]['name'];
            $_SESSION['USER']['MAILUSER']   = $loginUser[0]['mail'];
            header('Location: index.php');
           }
           else
           {
            @session_start();
            @session_destroy();
            header('Location: index.php?MSG=UserOrPassERROR');
           }          
        }//fin ver usuario
     
        public function addUser()
        {
            //Incluye el modelo que corresponde
           require $this->config->get('modelsFolder').'/UserModel.php'; 
           //Creamos una instancia de nuestro "modelo"
           $items = new UserModel(); 
           //Le insertamos un nuevo
           $data = array('name'=>$_POST['name'],'mail'=>$_POST['mail'],'pass'=>$_POST['pass']);

           $items->setUser($data);
        }//fin agregar

        public function setUserMailLister()
        {
            //Incluye el modelo que corresponde
           require $this->config->get('modelsFolder').'/UserModel.php'; 
           //Creamos una instancia de nuestro "modelo"
           $items = new UserModel(); 
           //Le insertamos un nuevo
           $data = array('mail'=>$_GET['email']);

           $items->setUserMailLister($data);

           header('Location: index.php?MSG=addListOK');
        }//fin agregar

        public function allowUser($mailUser)
        {
            //Incluye el modelo que corresponde
           require $this->config->get('modelsFolder').'/UserModel.php'; 
           //Creamos una instancia de nuestro "modelo"
           $items = new UserModel();
           $items->allowUser($mailUser);

        }//fin agregar

        public function updateUser()
        {
          $this->userLogued();

          $idUser = $_SESSION['USER']['IDUSER'];

          //Incluye el modelo que corresponde
          require $this->config->get('modelsFolder').'/UserModel.php'; 
          //Creamos una instancia de nuestro "modelo"
          $items = new UserModel(); 
          //Le insertamos un nuevo
          $data = array('idUser'=>$idUser,'name'=>$_GET['name'],'pass'=>$_GET['pass'],'state'=>$_GET['state']);

          $items->UpdateUser($data);

          if ($_GET['state']==2){
            @session_start();
            @session_destroy();
          } 
        }//fin agregar

        public function myProfile()
        {
            $this->userLogued();

            $idUser = $_SESSION['USER']['IDUSER'];

            require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController();

            require $this->config->get('controllersFolder').'/CartController.php'; 
            $cartProduct = new CartController();

            require $this->config->get('controllersFolder').'/ProductController.php'; 
            $product = new ProductController();

            require $this->config->get('modelsFolder').'/UserModel.php'; 
            $user = new UserModel();

            $data = array("category"=>$category->allCategory(),
                          "besCategory"=>$category->bestCategory(),
                          "cartProduct"=>$cartProduct->allProduct(),
                          "myProfile"=>$user->getUserData($idUser)
                        );
            //traemos el contenido y lo mostramos
            $this->view->show("myprofile", $data);
        }
    }
?>