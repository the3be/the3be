<?php 
class GraficController
{
	private $config;
	private $view;

	function __construct()
	{
		//Creamos una instancia de nuestro mini motor de plantillas
		$this->view = new View();
		$this->config = Config_::singleton();

	}

	public function color_generator(){
		// crea colores random RGB
		$r=rand(0, 255);
		$g=rand(0, 255);
		$b=rand(0, 255);
		//Setea los colores del borde en 1 y el Fondo con transparencia 0.6      
		$border_color="'rgba(".$r.",".$g.",".$b.", 1)'";
		$background_color="'rgba(".$r.",".$g.",".$b.", 0.6)'";
		$color=array($border_color,$background_color);

		return $color;
	}
	public function dataset_construct($array)
	{
		//Construye los dataset de chart.js a partir de los resultados de la consulta
		$dataset="[";
		$a="[";
		$b="[";

		foreach ($array as $data)
		{
		$a.="'".$data[0]."',";
		$b.="".$data[1].",";
		}

		$a=substr($a,0,-1)."]";
		$b=substr($b,0,-1)."]";
		$dataset=array('label' => $a , 'data' => $b );		
		return $dataset;
	}//fin 

	public function graph_construct($type,$n,$title,$data)
	{
		//Construye los dataset dependiendo si es una consulta doble o
		//si llegan los resultados por separado

		//Armamos la ruta a la plantilla
		$this->pathdataset = $this->config->get('htmlFolder').'/dataset.html';
		//cargamos el html del template
		$this->templatedataset = file_get_contents($this->pathdataset);

		$this->pathgraph = $this->config->get('htmlFolder').'/graph.html';
		//cargamos el html del template
		$this->templategraph = file_get_contents($this->pathgraph); 

		$result=$this->dataset_construct($data);
		$label=$result['label'];
		$value=$result['data'];
		$size=count($data);

		//carga los colores
		$color=array();
		$border="";$background="";

		for ($i=0; $i < $size; $i++)
		{ 
			$color=$this->color_generator();
			$border.=$color[0].",";
			$background.=$color[1].",";
		}

		$border="[".substr($border,0,-1)."]";
		$background="[".substr($background,0,-1)."]";
		
		if ($type=="line")
		{
			$border="";$background="";
			$color=$this->color_generator();
			$border.=$color[0].",";
			$background.=$color[1].",";
			$border="(".substr($border,0,-1).")";
			$background="(".substr($background,0,-1).")";
		}

		//sustituye el chart por la informacion
		$a=array("{label}","{data}","{border}","{background}");
		$b=array("cantidad",$value,$border,$background);
		$this->templatedataset=str_replace($a, $b, $this->templatedataset);

		//crea la Vista y la retorna
		$a=array("{nombre}","{data}","{datos}","{type}","{n}","{title}");
		$b=array($label,$this->templatedataset,$value,$type,$n,$title);
		$this->templategraph=str_replace($a, $b, $this->templategraph);

		return $this->templategraph;
		}

		public function viewAdministration()
		{
			@session_start();
			if (isset($_SESSION['USER']))
			{
				$idUser = $_SESSION['USER']['IDUSER'];
			}

			require $this->config->get('modelsFolder').'/GraficModel.php'; 
			$GraficModel = new GraficModel();

			require $this->config->get('controllersFolder').'/CartController.php'; 
			require $this->config->get('controllersFolder').'/CategoryController.php'; 
            $category = new CategoryController();
		    $cartProduct 	= new CartController();
		    $a 				= $GraficModel->getNumSales($idUser);
		    $b 				= $GraficModel->getNumPurchase($idUser);
			$data 			= array("cartProduct"=>$cartProduct->allProduct(),
																			"num_Sales"=>$a[0],
																			"num_Purchase"=>$b[0],
																			"besCategory"=>$category->bestCategory(),
																			"SalesTable"=>$GraficModel->getSalesTable($idUser),
																			"grafic2"=>$this->graph_construct('pie','0','Cantidad por Categoria',$GraficModel->getQuantityByCategorySale()),
																			"grafic1"=>$this->graph_construct('bar','1','Ventas por Producto',$GraficModel->getSaleByProduct($idUser)),
																			"grafic3"=>$this->graph_construct('line','2','Ganancias en los ultimos 15 dias',$GraficModel->getEarnings($idUser)),
																			"grafic4"=>$this->graph_construct('line','3','Historico del mas vendido',$GraficModel->getProductSells("-1",$idUser))// de forma temporal para que quede a vista la idea
						 );
			 //traemos el contenido y lo mostramos
			$this->view->show("administration", $data);
		}// fin  viewAdministration

		public function getNumPurchase($idUser)
		{
			require $this->config->get('modelsFolder').'/GraficModel.php'; 
			$graficModel = new GraficModel();
			return $graficModel->getNumPurchase($idUser);
		}// fin  getNumPurchase

		public function getNumSales($idUser)
		{
			require $this->config->get('modelsFolder').'/GraficModel.php'; 
			$graficModel = new GraficModel();
			return $graficModel->getNumSales($idUser);
		}// fin  getNumSales
}


 ?>