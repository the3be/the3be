<?php 
class myFavorite
{
    function __construct($datos)
    {
        $this->datos = $datos;
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config_::singleton(); 
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/index.html';
        //cargamos el html del template
        $this->index = file_get_contents($this->path);
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/my-favorite.html';
        //cargamos el html del template
        $this->template = file_get_contents($this->path);
        $this->template = $this->setFields('{BODY}',$this->template,$this->index);
        $this->userMenu = $config->get('htmlFolder').'/user-menu.html';
        //cargamos el html del template
        $this->templateUserMenu = file_get_contents($this->userMenu);

        $this->cartDetail = $config->get('htmlFolder').'/cart-detail.html';
        //cargamos el html del template
        $this->templatecartDetail = file_get_contents($this->cartDetail);

        $this->cartElement = $config->get('htmlFolder').'/cart-elemnt.html';
        //cargamos el html del template
        $this->templateCartElement = file_get_contents($this->cartElement);

        $this->cartElementMobile = $config->get('htmlFolder').'/cart-elemnt-mobile.html';
        //cargamos el html del template
        $this->templateCartElementMobile = file_get_contents($this->cartElementMobile);

        $this->allFavorite = $config->get('htmlFolder').'/product-element.html';
        //cargamos el html del template
        $this->templateallFavorite = file_get_contents($this->allFavorite);

        $this->useMenuNotLogued = $config->get('htmlFolder').'/user-menu-not-logued.html';
        //cargamos el html del template
        $this->templateUserMenuNotLogued = file_get_contents($this->useMenuNotLogued);

        $this->categoryFooter = $config->get('htmlFolder').'/category-footer.html';
        //cargamos el html del template
        $this->templateCategoryFooter = file_get_contents($this->categoryFooter);

        $this->loginRegister = $config->get('htmlFolder').'/login-register.html';
        //cargamos el html del template
        $this->templateloginRegister = file_get_contents($this->loginRegister);

        $this->administrationOption = $config->get('htmlFolder').'/administration-option.html';
        //cargamos el html del template
        $this->templateAdministrationOption = file_get_contents($this->administrationOption);

        $this->administrationOptionMobile = $config->get('htmlFolder').'/administration-option-mobile.html';
        //cargamos el html del template
        $this->templateAdministrationOptionMobile = file_get_contents($this->administrationOptionMobile);
    }
    
    public function processView(){
       @session_start();
       if (isset($_SESSION['USER']))
        {   
            $this->template = $this->setFields('{BUSCAR}', isset($_GET['name']) ? "value='".$_GET['name']."'": "placeholder='Samsung S8'", $this->template);

            $this->template = $this->setFields('{USER}', $this->templateUserMenu, $this->template);

            $this->template = $this->setFields('{MENU_MOBILE}', $this->templateCartElementMobile, $this->template);

            $this->template = $this->setFields('{USER_MOBILE}', $this->templateUserMenu, $this->template);
            
            $this->template = $this->setFields('{USUARIO_LOGADO}', $_SESSION['USER']['NAMEUSER'], $this->template);          

            $this->template = $this->setFields('{MEJORES_CATEGORIAS_DISPONILBES}', $this->setCategoryFooter(), $this->template);    

            $this->template = $this->setFields('{CARRITO_DE_COMPRAS}', $this->templatecartDetail, $this->template);

            $this->template = $this->setFields('{CANTIDAD_EN_CARITO}', $this->datos['cartProduct']['articulos_total'], $this->template);
            $this->template = $this->setFields('{CANTIDAD_EN_CARITO_MOBILE}', $this->datos['cartProduct']['articulos_total'], $this->template);

            $this->template = $this->setFields('{TOTAL_CARRITO}',$this->datos['cartProduct']['totalPrice'], $this->template);

            $this->template = $this->setFields('{TOTAL_CARRITO_MOBILE}',$this->datos['cartProduct']['totalPrice'], $this->template);
            $this->template = $this->setFields('{CARRITO}', $this->setCartDetail(), $this->template);

            $this->template = $this->setFields('{CARRITO_MOBILE}', $this->setCartDetail(), $this->template);            

            $this->template = $this->setFields('{TODOS_LOS_PRODUCTOS}', $this->setallFavorite(), $this->template);

            $this->template = $this->setSelect('{CATEGORIAS}', $this->datos['category'], $this->template);

            $this->template = $this->setFields('{IDUSER}', $_SESSION['USER']['IDUSER'], $this->template); //ULTIMO

            $this->template = $this->setFields('{LOGIN_REGISTRO}','', $this->template);
        
            $this->template = $this->setFields('{OPCION_USUARIO}',$this->templateAdministrationOption,$this->template);

            $this->template = $this->setFields('{OPCION_USUARIO_MOBILE}',$this->templateAdministrationOptionMobile,$this->template);
            
        }      
        echo $this->template;
    }

    private function setCartDetail()
    {   
        $allProductCart = '';

        foreach ($this->datos['cartProduct'] as $key => $CART) 
        {   if (@count($CART)==6)
            {
                $allProductCart .= $this->templateCartElement;
                $allProductCart = $this->setFields('{IMG}', $CART['img'], $allProductCart);
                $allProductCart = $this->setFields('{ID}', $CART['id'],  $allProductCart);
                $allProductCart = $this->setFields('{NOMBRE}', $CART['nombre'],  $allProductCart);
                $allProductCart = $this->setFields('{PRECIO}', $CART['precio'],  $allProductCart);
            }
        }
        return $allProductCart;
    }

    private function setallFavorite()
    {   
        $allFavorite = '';
        foreach ($this->datos['myFavorite'] as $key => $products) 
        {       $allFavorite .= $this->templateallFavorite;
                $allFavorite = $this->setFields('{IMG_PRODUCTO}', $products['img'], $allFavorite);                
                $allFavorite = $this->setFields('{NOMBRE_PRODUCTO}', $products['name'],  $allFavorite);
                $allFavorite = $this->setFields('{PRECIO_PRODUCTO}', $products['price'],  $allFavorite);
                $allFavorite = $this->setFields('{FECHA}', $products['date_ini'],  $allFavorite);
                $class="block2-labelnew";
                $date=$products['date_ini'];
                $today=date("Y-m-d");
                $nxtWeek = strtotime($today);
                $nxtWeek = strtotime("+7 day", $nxtWeek);
                $nxtWeek =date('Y-m-d', $nxtWeek);
                if ($date < $nxtWeek & $date >= $today) {
                   $allFavorite = $this->setFields('{CLASE}',$class,$allFavorite);
                }else{
                    $allFavorite = $this->setFields('{CLASE}',"",$allFavorite);
                }
                
                if (in_array($products['id'], $this->datos['allFavorite']))
                {
                    $allFavorite = $this->setFields('{ESTILO_FAVORITO}', 'fas fa-heart fa-2x',  $allFavorite);
                    $allFavorite = $this->setFields('{AGREGAR_SACAR_FAVORITO}', "dellToFavorite('{ID}')",  $allFavorite);
                    $allFavorite = $this->setFields('{TEXTO_BOTON_FAVORITO}', "Eliminar de mis Favoritos",  $allFavorite);
                    
                }
                else
                {
                    $allFavorite = $this->setFields('{ESTILO_FAVORITO}', 'far fa-heart fa-2x',  $allFavorite);                    
                    $allFavorite = $this->setFields('{AGREGAR_SACAR_FAVORITO}', "addToFavorite('{ID}')",  $allFavorite);
                    $allFavorite = $this->setFields('{TEXTO_BOTON_FAVORITO}', "Agregar a mis Favoritos",  $allFavorite);
                }

                $id_loged       = isset($_SESSION['USER']['IDUSER'])?$_SESSION['USER']['IDUSER']:0;
          
                $allFavorite    = $this->setFields('{MOSTRAR_BOTON}', '',  $allFavorite);

                $allFavorite    = $this->setFields('{ID}', $products['id'],  $allFavorite);
        }
        return $allFavorite;
    }

    private function setSelect($field, $value, $template)
    {
        $select = '';
            foreach ($value as $key => $value3) {
                    $select .=  "<option value='".$value3['id']."'>".$value3['name']."</option>";
            }                                              
        $template = str_replace($field, $select, $template);
        return $template;
    }//fin select

    private function setTable($field, $value, $template)
    { 
        $table = '';
        foreach ($value as $key2 => $value2)
        {             
            foreach ($value2 as $key => $value3) {
                $table .=  "<tr>";
                    $table .=  "<td>".$value3['id']."</td>";
                    $table .=  "<td>".$value3['nicname']."</td>";
                    $table .=  "<td>".$value3['user']."</td>";
                    $table .=  "<td>".$value3['status']."</td>";
                $table .=  "</tr>";
            }                                              
        }       
        $template = str_replace($field, $table, $template);  
        return $template;
    }//fin table
 
    private function setCategoryFooter()
    {   
        $categoryFooter = '';

        foreach ($this->datos['besCategory'] as $key => $category) 
        {       $categoryFooter .= $this->templateCategoryFooter;
                $categoryFooter = $this->setFields('{ID_CATEGORIA}', $category['category_id'], $categoryFooter);
                $categoryFooter = $this->setFields('{NOMBRE_CATEGORIA}', $category['name'],  $categoryFooter);
        }
        return $categoryFooter;
    }

    private function setFields($field, $value, $template)
    { 
        return str_replace($field, $value, $template);
    }//fin setFields
}//fin class vista listarusuarios
 ?>