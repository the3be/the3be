<?php 
class MyPurchase
{
    function __construct($datos)
    {
        $this->datos = $datos;
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config_::singleton(); 
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/index.html';
        //cargamos el html del template
        $this->index = file_get_contents($this->path);
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/my-purchase.html';
        //cargamos el html del template
        $this->template = file_get_contents($this->path);
        $this->template = $this->setFields('{BODY}',$this->template,$this->index);
        $this->userMenu = $config->get('htmlFolder').'/user-menu.html';
        //cargamos el html del template
        $this->templateUserMenu = file_get_contents($this->userMenu);

        $this->cartDetail = $config->get('htmlFolder').'/cart-detail.html';
        //cargamos el html del template
        $this->templatecartDetail = file_get_contents($this->cartDetail);

        $this->elemntCart = $config->get('htmlFolder').'/cart-elemnt.html';
        //cargamos el html del template
        $this->templateCartElement = file_get_contents($this->elemntCart);

        $this->cartElementMobile = $config->get('htmlFolder').'/cart-elemnt-mobile.html';
        //cargamos el html del template
        $this->templateCartElementMobile = file_get_contents($this->cartElementMobile);

        $this->categoryFooter = $config->get('htmlFolder').'/category-footer.html';
        //cargamos el html del template
        $this->templateCategoryFooter = file_get_contents($this->categoryFooter);

        $this->productMySale = $config->get('htmlFolder').'/product-my-purchase.html';
        //cargamos el html del template
        $this->templateproductMySale = file_get_contents($this->productMySale);

        $this->administrationOption = $config->get('htmlFolder').'/administration-option.html';
        //cargamos el html del template
        $this->templateAdministrationOption = file_get_contents($this->administrationOption);

        $this->administrationOptionMobile = $config->get('htmlFolder').'/administration-option-mobile.html';
        //cargamos el html del template
        $this->templateAdministrationOptionMobile = file_get_contents($this->administrationOptionMobile);
    }

    public function processView(){
       @session_start();
       if (isset($_SESSION['USER']))
        {            

            $this->template = $this->setFields('{USER}', $this->templateUserMenu, $this->template);

            $this->template = $this->setFields('{MENU_MOBILE}', $this->templateCartElementMobile, $this->template);

            $this->template = $this->setFields('{USER_MOBILE}', $this->templateUserMenu, $this->template);

            $this->template = $this->setFields('{USUARIO_LOGADO}', $_SESSION['USER']['NAMEUSER'], $this->template);          

            $this->template = $this->setFields('{MEJORES_CATEGORIAS_DISPONILBES}', $this->setCategoryFooter(), $this->template);    

            $this->template = $this->setFields('{CARRITO_DE_COMPRAS}', $this->templatecartDetail, $this->template);

            $this->template = $this->setFields('{CANTIDAD_EN_CARITO}', $this->datos['cartProduct']['articulos_total'], $this->template);
            $this->template = $this->setFields('{CANTIDAD_EN_CARITO_MOBILE}', $this->datos['cartProduct']['articulos_total'], $this->template);
            $this->template = $this->setFields('{TOTAL_CARRITO}',$this->datos['cartProduct']['totalPrice'], $this->template);

            $this->template = $this->setFields('{TOTAL_CARRITO_MOBILE}',$this->datos['cartProduct']['totalPrice'], $this->template);
         
            $this->template = $this->setFields('{MIS_VENTAS}', $this->setmyPurchaseProduct(), $this->template);

            $this->template = $this->setFields('{CARRITO}', $this->setCartDetail(), $this->template);

            $this->template = $this->setFields('{CARRITO_MOBILE}', $this->setCartDetail(), $this->template); 

            $this->template = $this->setFields('{OPCION_USUARIO}',$this->templateAdministrationOption,$this->template);

            $this->template = $this->setFields('{OPCION_USUARIO_MOBILE}',$this->templateAdministrationOptionMobile,$this->template);                   

            $this->template = $this->setFields('{IDUSER}', $_SESSION['USER']['IDUSER'], $this->template); //ULTIMO
            $this->template = $this->setFields('{LOGIN_REGISTRO}',"",$this->template);
        }
        echo $this->template;
    }

    private function setCartDetail()
    {   
        $allProductCart = '';

        foreach ($this->datos['cartProduct'] as $key => $CART) 
        {   if (@count($CART)==6)
            {
                $allProductCart .= $this->templateCartElement;
                $allProductCart = $this->setFields('{IMG}', $CART['img'], $allProductCart);
                $allProductCart = $this->setFields('{ID}', $CART['id'],  $allProductCart);
                $allProductCart = $this->setFields('{NOMBRE}', $CART['nombre'],  $allProductCart);
                $allProductCart = $this->setFields('{PRECIO}', $CART['precio'],  $allProductCart);
            }
        }
        return $allProductCart;
    }

    private function setAllProduct()
    {   
        $allProduct = '';

        foreach ($this->datos['allProduct'] as $key => $products) 
        {       $allProduct .= $this->templateallProduct;
                $allProduct = $this->setFields('{IMG_PRODUCTO}', $products['img'], $allProduct);
                $allProduct = $this->setFields('{ID}', $products['id'],  $allProduct);
                $allProduct = $this->setFields('{NOMBRE_PRODUCTO}', $products['name'],  $allProduct);
                $allProduct = $this->setFields('{PRECIO_PRODUCTO}', $products['price'],  $allProduct);
        }
        return $allProduct;
    }

    private function setSelect($field, $value, $template)
    {
        $select = '';
            foreach ($value as $key => $value3) {
                    $select .=  "<option value='".$value3['id']."'>".$value3['name']."</option>";
            }                                              
        $template = str_replace($field, $select, $template);
        return $template;
    }//fin select

    private function setmyPurchaseProduct()
    {   
        $myPurchaseProduct = '';

        if(count($this->datos['myPurchaseProduct'])==0)
        {
               $myPurchaseProduct = "<tr>
                                    <td colspan='6'>No tiene ninguna Compra pendiente!!</td>

                                </tr>";
        }
        else
        {
            foreach ($this->datos['myPurchaseProduct'] as $key => $products) 
            {       
                $myPurchaseProduct .= $this->templateproductMySale;
                $myPurchaseProduct = $this->setFields('{ESTATUS_COMPRA}', $products['status'], $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{IMG_PRODUCTO}', $products['img'], $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{ID_PRODUCTO}', $products['id'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{NOMBRE_PRODUCTO}', $products['product'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{VENCIMIENTO}', $products['date_end'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{CLIENTE}', $products['seller'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{COSTO}', $products['price'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{ID_COMPRA}', $products['id_purchase'],  $myPurchaseProduct);
                switch ($products['status']) {
                    case '-1':
                        $color="135, 135, 135,0.8";
                        $class="table-secondary";
                    break;
                    case '1':
                        $color="255, 202, 51,0.8";
                        $class="table-warning";
                    break;
                    case '2':
                        $color="76, 179, 204,0.8";
                        $class="table-info";
                    break;
                    case '3':
                        $color="172, 255, 51,1";
                        $class="table-success";
                    break;
                    default:
                        # code...
                    break;
                }
                //$myPurchaseProduct = $this->setFields('{color}', $color,  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{class}', $class,  $myPurchaseProduct);
            }
        }

        return $myPurchaseProduct;
    }
 
    private function setCategoryFooter()
    {   
        $categoryFooter = '';

        foreach ($this->datos['besCategory'] as $key => $category) 
        {       $categoryFooter .= $this->templateCategoryFooter;
                $categoryFooter = $this->setFields('{ID_CATEGORIA}', $category['category_id'], $categoryFooter);
                $categoryFooter = $this->setFields('{NOMBRE_CATEGORIA}', $category['name'],  $categoryFooter);
        }
        return $categoryFooter;
    }

    private function setFields($field, $value, $template)
    { 
        return str_replace($field, $value, $template);
    }//fin setFields
}//fin class vista listarusuarios
 ?>