        
        function initMap() {
        var Uruguay = {lat: -34.8730305, lng: -56.261422499999995};
        map = new google.maps.Map(document.getElementById('map'), {
          center: Uruguay,
          zoom: 8
        });
        
        var marcador = new google.maps.Marker({
          position: Uruguay,
          map: map
        });
        
        var informacion = new google.maps.InfoWindow;
        marcador.addListener('click', function(){
          informacion.open(map,marcador);
        })

        autocomplete = new google.maps.places.Autocomplete(document.getElementById('autocomplete'));
        autocomplete.bindTo('bounds',map);

        autocomplete.addListener('place_changed', function(){
          informacion.close;
          marcador.setVisible(false);

          var place = autocomplete.getPlace();

          if(!place.geometry.viewport){
            window.alert("Error al cargar la ubicacion");
            return;
          };
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          }else{
            map.setCenter(place.geometry.location);
            map.setZoom(18);
          }
          marcador.setPosition(place.geometry.location);
          marcador.setVisible(true);
          var address="";
          if (place.address_components) {
            address=[
            (place.address_components[0] && place.address_components[0].short_name || ''),
            (place.address_components[1] && place.address_components[1].short_name || ''),
            (place.address_components[2] && place.address_components[2].short_name || ''),
            ];
          }
          informacion.setContent('<div><strong>'+place.name+'</strong></div>'+address);
          informacion.open(map,marcador);
          //place.name contiene el nombre de la direccion
        });
}

