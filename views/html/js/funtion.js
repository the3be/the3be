		function checUserRegister()
		{
				var name = $('#registerUser :input#name').val();
				var mail = $('#registerUser :input#mail').val();
				var pass = $('#registerUser :input#pass').val();
				var pass2= $('#registerUser :input#repeat-pass').val();

				if (caracteresCorreoValido(mail)) {
					if (pass == pass2) {
						var parameters = {'addUser':'addUser','name':name, 'mail':mail, 'pass':pass};
						$.ajax({
							data: parameters,
							url: "index.php",
							type: 'POST',
							success: function(result){
								$('#RegistrarUsuario').modal('toggle');
								swal(
									  'Bienvenido a The3Be',
									  'Te recomendamos abrir tu email para validad tu cuenta!',
									  'success'
									);
								
								$.ajax({
									data: 'SendEmailRegister',
									url: "index.php?mailUser="+mail,
									type: 'GET',
									success: function(result){

									}
								});
							}
						});					
					}
					else
					{
						$('#RegistrarUsuario').modal('toggle');
						swal({
						  title: 'Las contraseñas NO son iguales!!',
						  animation: false,
						  customClass: 'animated tada'
						}).then(function(){
							$('#RegistrarUsuario').modal();
						});	
					}
				}
		}
		function allow(input){
			input=$('input[name="'+input+'"]');
			if(input.prop('disabled')===false){
				input.prop( "disabled", true );
			}else{
				input.prop( "disabled", false );
			}
		}
		function login()
		{
			$('#Login').modal();
		}
		function register()
		{
			$('#Login').modal('toggle');
			$('#RegistrarUsuario').modal();
		}
		function retornar()
		{
			$('#RegistrarUsuario').modal('toggle');
			$('#Login').modal();
		}
		try{
		$('#registerUser').find('input[type=email]').blur(function(){
			caracteresCorreoValido($(this).val())
		});
		}catch(e){

		}
		function addToCart(idForm)
		{
			var parameters = {
					"idUser" : $('#'+idForm+' :input#idUser').val(),
			        "id" : $('#'+idForm+' :input#id').val(),
			        "name" : $('#'+idForm+' :input#name').val(),
			        "price" : $('#'+idForm+' :input#price').val(),
			        "img" : $('#'+idForm+' :input#img').val()
			};
			if(parameters.idUser == "NOTLOGUED"){
				login();
			}else{
				$.ajax({
					data:  parameters,
					url: "index.php?addProductCart",
					type: 'GET',
					success: function(result){
						swal($('#'+idForm+' :input#name').val(), " fue agregado a tu carrito!", "success").then(function(){
						location.reload();
						});
					}
				});
			}
		}//fin add cart
		function addToFavorite(idForm)
		{

			var parameters = {
			        "idProduct" : $('#'+idForm+' :input#id').val(),
			        "idUser" : $('#'+idForm+' :input#idUser').val(),
			};

			if(parameters.idUser == 'NOTLOGUED'){
				login();
			}else{
				$.ajax({
					data:  parameters,
					url: "index.php?addFavorite",
					type: 'GET',
					success: function(result){
						location.reload();
					}
				});
			}
		}//end add to favorite

		function dellToFavorite(idForm)
		{

			var parameters = {
			        "idProduct" : $('#'+idForm+' :input#id').val(),
			        "idUser" : $('#'+idForm+' :input#idUser').val(),
			};

			if(parameters.idUser == 'NOTLOGUED'){
				login();
			}else{
				$.ajax({
					data:  parameters,
					url: "index.php?dellFavorite",
					type: 'GET',
					success: function(result){
						location.reload();
					}
				});
				
			}
		}//end add to favorite

		function dellProductCart(idProduct,nameProduct)
		{
			var parameters = {"idProduct": idProduct};
			swal({
				  title: '¿Está seguro quiere eliminar '+nameProduct+'?',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, eliminar '
				}).then((result) => 
				{
				  if (result.value) {
				    swal(
				      'Correcto',
				      'Se elimino correctamente el producto de su carrito.',
				      'success'
				    ).then(function(){						
						$.ajax({
							data:  parameters,
							url: "index.php?dellProductCart",
							type: 'GET',
							success: function(result){
								location.reload();
							}
						});
					})
				  }
				});
		}//en function
		// funcion para validar el correo
		function caracteresCorreoValido(email)
		{

		    var caract = new RegExp(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/);

		    if (caract.test(email) == false){		        
	    		$('#RegistrarUsuario').modal('toggle');
				swal({
				  title: 'El mail no es valido!!',
				  animation: false,
				  customClass: 'animated tada'
				}).then(function(){
					$('#RegistrarUsuario').modal();
				});
		    }else{
		    	return true;
		    }
		}		
		function change() 
		{
			valor=$('input[name="customRadio"]:checked').text();
        	$("#resultado").html(valor);
		};		
		function checkDataUser()
		{

			var name = $(':input#name').val();
			var nameOld = $(':input#nameOld').val();
			var pass = $(':input#pass').val();
			var repeat_pass = $(':input#repeat_pass').val();
			var state = $(':input#state').val();
			
			if(name != nameOld)
			{
				name = name;
			}
			else
			{
				name = '-1';
			}
			if (state==null) {
				state = '-2';
			}
			else
			{
				state = state;
			}
			if(pass != 'PASS_ACTUAL')
			{
				if (pass == repeat_pass) 
				{
					pass=pass;
				}				
			}
			else
			{
				pass='-1';	
			}
			if(name == '-1' && state == '-2' && pass == '-1')
			{
			 	return false;
			}

			parameters = {
				'name' : name,
				'pass' : pass,
				'state' : state
			};

			console.log(parameters);

			$.ajax({
			data:  parameters,
			url: "index.php?updateUserData",
			type: 'GET',
				success: function(result){
					location.reload();
				}
			});
		}
		function desactivar()
		{
			parameters = {
				'name' : '-1',
				'pass' : '-1',
				'state' : '2'
			};

			swal({
				  title: '¿Está seguro desactivar su cuenta?',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#d33',
				  cancelButtonColor: '#3085d6',
				  confirmButtonText: 'Si, desactivar'
				}).then((result) => 
				{
				  if (result.value) {
				    swal(
				      'Correcto',
				      'Su cuenta sera desactivada hasta que vuelva a iniciar sesion.',
				      'info'
				    ).then(function(){						
							$.ajax({
							data:  parameters,
							url: "index.php?updateUserData",
							type: 'GET',
								success: function(result){
									location.reload();
								}
							});
					})
				  }
				});	
		}
		function closePurchase(idProduct,idUser,idPurchase,statusPurchase)
		{
			if (statusPurchase==1) {
				swal({
						  title: 'El producto no fue entregado, no se puede calificar la compra aún!',
						  animation: false,
						  customClass: 'animated tada'
						}).then(function(){
							
						});
				return false;	
			}

			swal({
				title: 'Por favor ingrese un comentario acerca del Producto!',
				input: 'text',
				inputPlaceholder: "Reseña",
				html:
				  '<div class="clasificacion" onmouseover="change()">'+
				    '<input type="radio" id="customRadio1" name="customRadio" value="5" class="custom-control-input">'+
							  '<label  for="customRadio1" class="star stars" data-toggle="tooltip" title="Perfecto"><i class="far fa-star fa-2x"></i></label>'+
							
							  '<input type="radio" id="customRadio2" name="customRadio" value="4" class="custom-control-input">'+
							 ' <label  for="customRadio2" class="star stars" data-toggle="tooltip" title="Muy Bueno"><i class="far fa-star fa-2x"></i></label>'+
								
							  '<input type="radio" id="customRadio3" name="customRadio" value="3" class="custom-control-input">'+
							  '<label  for="customRadio3" class="star stars" data-toggle="tooltip" title="Recomendable"><i class="far fa-star fa-2x"></i></label>'+
							
							  '<input type="radio" id="customRadio4" name="customRadio" value="2" class="custom-control-input">'+
							 ' <label  for="customRadio4" class="star stars" data-toggle="tooltip" title="No tan bueno"><i class="far fa-star fa-2x"></i></label>'+
							
							 ' <input type="radio" id="customRadio5" name="customRadio" value="1" class="custom-control-input">'+
							  '<label  for="customRadio5" class="star stars" data-toggle="tooltip" title="Infumable"><i class="far fa-star fa-2x"></i></label>'+
					'</div>'+
					 '<br><span id="resultado"></span>',
				inputAttributes: {
				autocapitalize: 'off'
				},
					showCancelButton: true,
					confirmButtonText: 'Calificar Producto',
					showLoaderOnConfirm: true,
					preConfirm: (coment) => {
						stars=$('input[name="customRadio"]:checked').val();
						$.ajax({
							data: 'addComent',
							url: "index.php?idUser="+idUser+"&idProduct="+idProduct+"&idPurchase="+idPurchase+"&coment="+coment+"&stars="+stars,
							type: 'GET',
							success: function(result){
								location.reload();
							}
						});
				}			
			})
		}
		function puntuacion()
		{
			max=5;
			avg=$('input[name="avg"]').val();
			titles={'0':'Sin Calificar','1':'Infumable','2':'Bueno','3':'Muy Bueno','4':'Recomendable','5':'Excelente'};
			avg=avg.substring(0, 1);
			if (avg == "") {
				avg=0;
			}
			title=titles[avg];
			
			max=max-avg;
			x='';
			y='';
			for (var i = 0; i < avg; i++) {
				x=x+('<i class="far fa-star" title="'+title+'" style="color: orange;"></i>');
			}
			for (var i = 0; i < max; i++) {
				y=y+'<i class="far fa-star" title="'+title+'">';
			}
			$('#stars').html(x+y);
		};		
		function confirmEndPurchase()
		{

			var cantCarrito = $('#cantCarrito').val();

			if (cantCarrito > 0)
			{
				swal({
				  title: '¿Está seguro quiere proceder al pago?',
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Si, proceder al pago.'
				}).then((result) => 
				{
				  if (result.value) {
				    swal(
				      'Correcto',
				      'Le recomendamos ver su casilla de correo.',
				      'success'
				    ).then(function(){
				    	$.ajax({
							data:'endPurchase',
							url: "index.php",
							type: 'GET',
							success: function(result){
								$.ajax({
									data:'SendEmailSales',
									url: "index.php?idUser="+$('#idUser').val(),
									type: 'GET',
									success: function(result){}
								});
								location.reload();
							}
						});
					})
				  }
				});
			}
			else
			{
				 swal({
					  title: 'El carrito esta vacio!',
					  animation: false,
					  customClass: 'animated tada'
					})
			}
		}//en function
		function closeSales(idSale)
		{
			swal({
				title: 'Por favor ingrese un comentario acerca del Comprador!',
				input: 'text',
				inputAttributes: {
				autocapitalize: 'off'
				},
					showCancelButton: true,
					confirmButtonText: 'Confirmar Cierre de Compra',
					showLoaderOnConfirm: true,
					preConfirm: (coment) => {
						$.ajax({
							data: 'closeSales',
							url: "index.php?idSale="+idSale+"&coment="+coment,
							type: 'GET',
							success: function(result){
								swal(
								  'Felicitaciones!!',
								  'Ya se concreto tu venta, exitos, por mas ventas!!!',
								  'success'
								);
								location.reload();
							}
						});
				}			
			})
		}
		function dellPurchase(idPurchase,productName,statusPurchase)
		{
			if (statusPurchase!=1) {
				swal({
						  title: 'El producto ya fue entregado, no se puede cancelar la compra!',
						  animation: false,
						  customClass: 'animated tada'
						}).then(function(){
							
						});
				return false;	
			}


			swal({
				  title: 'Esta seguro?',
				  text: "Esta por cancelar la compra de "+ productName,
				  type: 'warning',
				  showCancelButton: true,
				  confirmButtonColor: '#3085d6',
				  cancelButtonColor: '#d33',
				  confirmButtonText: 'Eliminar Compra!'
				}).then(function(result){

					if (result.value!=undefined) {					
				    	$.ajax({
							data: 'dellPurchase',
							url: "index.php?idPurchase="+idPurchase,
							type: 'GET',
							success: function(result){
								location.reload();						
							}
						});
					}				   
				});
		}
		function sendContactMail()
		{
				document.getElementById('buttonSendMailContact').disabled=true;

				var name 	= $('#formContact :input#name').val();
				var phone 	= $('#formContact :input#phone').val();
				var email 	= $('#formContact :input#email').val();
				var message = $('#formContact :input#message').val();
				if(name=='' || phone=='' || email=='' || message==''){
					swal({
					  title: 'La consulta requiere de todos los campos, verifique!!',
					  animation: false,
					  customClass: 'animated tada'
					}).then(function(){

					});
					document.getElementById('buttonSendMailContact').disabled=false;
					return false;
				}

				var parameters = {'SendEmailContact':'SendEmailContact','name':name, 'phone':phone,'email':email,'comment':message};

				$.ajax({
					data: parameters,
					url: "index.php",
					type: 'GET',
					success: function(result){
					}
				});

				swal.fire(
				  'Ya fue enviado el mail',
				  'Un representante se comunicara en las proximas 24hrs si es necesario, muchas gracias desde el equipo de The3Be!!',
				  'success'
				);
				
				document.getElementById('buttonSendMailContact').disabled=false;

				$('#formContact :input#name').val('');
				$('#formContact :input#phone').val('');
				$('#formContact :input#email').val('');
				$('#formContact :input#message').val('');
		}
		var myVar;
		function myFunction() {
		    myVar = setTimeout(showPage, 2000);
		}

		function showPage() {
			$("#loader-container").fadeOut();
			$("#loader").fadeOut();
			$("#myDiv").fadeOut();
		}
		window.onload=function()
		{
			myFunction();	
			news();
			news_purchase();	 	
			setInterval("news()",5000);
			setInterval("news_purchase()",5000);
			setInterval("general_count()",5000);
		}
		try{
		$(document).ready(function(){
		    	$(window).scroll(function () {
					if ($(this).scrollTop() != 0) {
						$('#toTop').fadeIn();
					} else {
						$('#toTop').fadeOut();
					}
				}); 
		    $('#toTop').click(function(){
		        $("html, body").animate({ scrollTop: 0 }, 600);
		        return false;
		    });
		});
		}catch(e){};
		try{
		$('.far fa-heart').on('click',function(){
			$(this).removeClass('far fa-heart');
			$(this).removeClass('fas fa-heart');
		})
		}catch(e){};
		function archivo(evt)
        {
			var files = evt.target.files; // FileList object

			//Obtenemos la imagen del campo "file". 
			for (var i = 0, f; f = files[i]; i++)
			{         
				//Solo admitimos imágenes.
				if (!f.type.match('image.*'))
				{
					continue;
				}
				var reader = new FileReader();
				reader.onload = (function(theFile)
				{
					return function(e)
					{
						// Creamos la imagen.
						document.getElementById("list").innerHTML = ['<img class="thumb" class="col-sm-6 p-0" style="height: 100%;max-width: 100%;" onclick="$('+"'"+"#picture"+"'"+').click();" src="', e.target.result,'" title="', escape(theFile.name), '"/>'].join('');
					};
				})(f);

				reader.readAsDataURL(f);
			}
		}
        try{   
      	document.getElementById('picture').addEventListener('change', archivo, false);
      	}catch(e){}
      	$(function(){
  		$('table').tablesorter(); 
  		});