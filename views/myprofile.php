<?php 
class myprofile
{
	private $titulo;
	private $idUser;
	private $nicName;
	private $user;
    private $selects;
    private $tables;
    private $path;

	function __construct($datos)
	{
        $this->datos = $datos;
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config_::singleton(); 
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/index.html';
        //cargamos el html del template
        $this->index = file_get_contents($this->path);
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/profile-user.html';
        //cargamos el html del template
        $this->template = file_get_contents($this->path);
        $this->template = $this->setFields('{BODY}',$this->template,$this->index);
        $this->userMenu = $config->get('htmlFolder').'/user-menu.html';
        //cargamos el html del template
        $this->templateUserMenu = file_get_contents($this->userMenu);

        $this->cartDetail = $config->get('htmlFolder').'/cart-detail.html';
        //cargamos el html del template
        $this->templatecartDetail = file_get_contents($this->cartDetail);

        $this->cartElement = $config->get('htmlFolder').'/cart-elemnt.html';
        //cargamos el html del template
        $this->templateCartElement = file_get_contents($this->cartElement);
        //cargamos el html del template
        $this->cartElementMobile = $config->get('htmlFolder').'/cart-elemnt-mobile.html';
        $this->templateCartElementMobile = file_get_contents($this->cartElementMobile);

        $this->categoryFooter = $config->get('htmlFolder').'/category-footer.html';
        //cargamos el html del template
        $this->templateCategoryFooter = file_get_contents($this->categoryFooter);

        $this->administrationOption = $config->get('htmlFolder').'/administration-option.html';
        //cargamos el html del template
        $this->templateAdministrationOption = file_get_contents($this->administrationOption);

        $this->administrationOptionMobile = $config->get('htmlFolder').'/administration-option-mobile.html';
        //cargamos el html del template
        $this->templateAdministrationOptionMobile = file_get_contents($this->administrationOptionMobile);

	}

    public function processView(){
       @session_start();
       if (isset($_SESSION['USER']))
        {

            $this->template = $this->setFields('{USER}', $this->templateUserMenu, $this->template);

            $this->template = $this->setFields('{MENU_MOBILE}', $this->templateCartElementMobile, $this->template);

            $this->template = $this->setFields('{USER_MOBILE}', $this->templateUserMenu, $this->template);

            $this->template = $this->setFields('{USUARIO_LOGADO}', $_SESSION['USER']['NAMEUSER'], $this->template);

            $this->template = $this->setFields('{IDUSER}', $_SESSION['USER']['IDUSER'], $this->template);

            $this->template = $this->setFields('{MEJORES_CATEGORIAS_DISPONILBES}', $this->setCategoryFooter(), $this->template);
            $this->template = $this->setFields('{CARRITO_DE_COMPRAS}', $this->templatecartDetail, $this->template);

            $this->template = $this->setFields('{CANTIDAD_EN_CARITO}', $this->datos['cartProduct']['articulos_total'], $this->template);
            $this->template = $this->setFields('{CANTIDAD_EN_CARITO_MOBILE}', $this->datos['cartProduct']['articulos_total'], $this->template);
            $this->template = $this->setFields('{TOTAL_CARRITO}',$this->datos['cartProduct']['totalPrice'], $this->template);

            $this->template = $this->setFields('{TOTAL_CARRITO_MOBILE}',$this->datos['cartProduct']['totalPrice'], $this->template);
            $this->template = $this->setFields('{CARRITO}', $this->setCartDetail(), $this->template);

            $this->template = $this->setFields('{CARRITO_MOBILE}', $this->setCartDetail(), $this->template);            

            $this->template = $this->setFields('{ID_USUARIO}', $this->datos['myProfile'][0]['id'], $this->template);

            $this->template = $this->setFields('{NOMBRE_USUARIO}', $this->datos['myProfile'][0]['name'], $this->template);

            $this->template = $this->setFields('{MAIL_USUARIO}', $this->datos['myProfile'][0]['mail'], $this->template);

            $this->template = $this->setFields('{SUSCRIPCION_USUARIO}',($this->datos['myProfile'][0]['sub_state'] == '1') ? 'SUSCRIPTO' : 'NO SUSCRIPTO', $this->template);

            $this->template = $this->setFields('{OPCION_USUARIO}',$this->templateAdministrationOption,$this->template);

            $this->template = $this->setFields('{OPCION_USUARIO_MOBILE}',$this->templateAdministrationOptionMobile,$this->template);
            $this->template = $this->setFields('{LOGIN_REGISTRO}',"",$this->template);

        }
        echo $this->template;
    }

    private function setCartDetail()
    {   
        $allProductCart = '';

        foreach ($this->datos['cartProduct'] as $key => $CART) 
        {   if (@count($CART)==6)
            {
                $allProductCart .= $this->templateCartElement;
                $allProductCart = $this->setFields('{IMG}', $CART['img'], $allProductCart);
                $allProductCart = $this->setFields('{ID}', $CART['id'],  $allProductCart);
                $allProductCart = $this->setFields('{NOMBRE}', $CART['nombre'],  $allProductCart);
                $allProductCart = $this->setFields('{PRECIO}', $CART['precio'],  $allProductCart);
            }
        }
        return $allProductCart;
    }

    private function setSelect($field, $value, $template)
    {
        $select = '';
            foreach ($value as $key => $value3) {
                    $select .=  "<option value='".$value3['id']."'>".$value3['name']."</option>";
            }                                              
        $template = str_replace($field, $select, $template);
        return $template;
    }//fin select

    private function setTableUsuarios($field, $value, $template)
    { 
        $table = '';
        foreach ($value as $key2 => $value2)
        {             
            foreach ($value2 as $key => $value3) {
                $table .=  "<tr>";
                    $table .=  "<td>".$value3['id']."</td>";
                    $table .=  "<td>".$value3['nicname']."</td>";
                    $table .=  "<td>".$value3['user']."</td>";
                    $table .=  "<td>".$value3['status']."</td>";
                $table .=  "</tr>";
            }                                              
        }       
        $template = str_replace($field, $table, $template);  
        return $template;
    }//fin table

    private function setProductToEnd()
    {   
        $productToEnd = '';

        foreach ($this->datos['productToEnd'] as $key => $products) 
        {       $productToEnd .= $this->templateproductToEnd;
                $productToEnd = $this->setFields('{IMG_PRODUCTO}', $products['img'], $productToEnd);
                $productToEnd = $this->setFields('{ID}', $products['id'],  $productToEnd);
                $productToEnd = $this->setFields('{NOMBRE_PRODUCTO}', $products['name'],  $productToEnd);
                $productToEnd = $this->setFields('{PRECIO_PRODUCTO}', $products['price'],  $productToEnd);
        }
        return $productToEnd;
    }

    private function setCategoryFooter()
    {   
        $categoryFooter = '';

        foreach ($this->datos['besCategory'] as $key => $category) 
        {       $categoryFooter .= $this->templateCategoryFooter;
                $categoryFooter = $this->setFields('{ID_CATEGORIA}', $category['category_id'], $categoryFooter);
                $categoryFooter = $this->setFields('{NOMBRE_CATEGORIA}', $category['name'],  $categoryFooter);
        }
        return $categoryFooter;
    }

    private function setFields($field, $value, $template)
    { 
        return str_replace($field, $value, $template);
    }//fin setFields

    private function calculateDifHour($dateEnd)
    {
        $date1 = new DateTime($dateEnd);
        $date2 = new DateTime("now");

        $diff = $date1->diff($date2);

        return ( ($diff->days * 24 ) * 60 ) + ( $diff->i * 60 ) + $diff->s . ' seconds';

    }
}//fin class vista listarusuarios
 ?>