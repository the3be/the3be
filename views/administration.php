<?php 
class administration
{
	private $datos;
	private $temp;
	private $canvas;
	private $template;

	public function __construct($datos)
	{
	//carga los templates
        $this->datos = $datos;
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config_::singleton(); 
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/index.html';
        //cargamos el html del template
        $this->index = file_get_contents($this->path);
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/administration.html';
        //cargamos el html del template
        $this->template = file_get_contents($this->path);
        $this->template = str_replace('{BODY}',$this->template,$this->index);
        //cargamos el html del template
        $this->userMenu = $config->get('htmlFolder').'/user-menu.html';
        //cargamos el html del template
        $this->templateUserMenu = file_get_contents($this->userMenu);
        //cargamos el html del template
        $this->administrationOption = $config->get('htmlFolder').'/administration-option.html';
        //cargamos el html del template
        $this->templateAdministrationOption = file_get_contents($this->administrationOption);
        $this->administrationOptionMobile = $config->get('htmlFolder').'/administration-option-mobile.html';
        //cargamos el html del template
        $this->cartDetail = $config->get('htmlFolder').'/cart-detail.html';
        //cargamos el html del template
        $this->table_row = $config->get('htmlFolder').'/table-row.html';
        //cargamos el html del template
        $this->table_row = file_get_contents($this->table_row);
        //cargamos el html del template
        $this->templatecartDetail = file_get_contents($this->cartDetail);
        //cargamos el html del template
        $this->template = $this->setFields('{LOGIN_REGISTRO}',"",$this->template);
        //cargamos el html del template
        $this->categoryFooter = $config->get('htmlFolder').'/category-footer.html';
        //cargamos el html del template
        $this->templateCategoryFooter = file_get_contents($this->categoryFooter);
    
	}

    public function processView()
    {
        @session_start();
	    $this->template = $this->setFields("{GRAFIC1}", $this->datos['grafic1'], $this->template);
	    $this->template = $this->setFields("{GRAFIC2}", $this->datos['grafic2'], $this->template);
	    $this->template = $this->setFields("{GRAFIC3}", $this->datos['grafic3'], $this->template);
	    $this->template = $this->setFields("{GRAFIC4}", $this->datos['grafic4'], $this->template);
        $this->template = $this->setFields("{NUM_SALES}", $this->datos['num_Sales'], $this->template);
        $this->template = $this->setFields("{NUM_PURCHASE}", $this->datos['num_Purchase'], $this->template);
        $this->template = $this->setFields('{OPCION_USUARIO}',$this->templateAdministrationOption,$this->template);
        $this->template = $this->setFields('{CARRITO_DE_COMPRAS}', $this->templatecartDetail, $this->template);
        $this->template = $this->setFields('{CANTIDAD_EN_CARITO}', $this->datos['cartProduct']['articulos_total'], $this->template);
        $this->template = $this->setFields('{CANTIDAD_EN_CARITO_MOBILE}', $this->datos['cartProduct']['articulos_total'], $this->template);
        $this->template = $this->setFields('{TOTAL_CARRITO}',$this->datos['cartProduct']['totalPrice'], $this->template);
        $this->template = $this->setFields('{TOTAL_CARRITO_MOBILE}',$this->datos['cartProduct']['totalPrice'], $this->template);
        $this->template = $this->setFields('{USER}', $this->templateUserMenu, $this->template);
        $this->template = $this->setFields('{USUARIO_LOGADO}', $_SESSION['USER']['NAMEUSER'], $this->template);
        $this->template = $this->setFields('{CARRITO}', $this->setCartDetail(), $this->template);
        $this->template = $this->setFields('{CARRITO_MOBILE}', $this->setCartDetail(), $this->template);
        $this->template = $this->setFields('{TABLA}', $this->tableSales(),  $this->template);
        $this->template = $this->setFields('{MEJORES_CATEGORIAS_DISPONILBES}', $this->setCategoryFooter(), $this->template);   


	    echo  $this->template;
  	} 
    private function setFields($field, $value, $template)
    { 
        return str_replace($field, $value, $template);
    }
    private function setmySales()
    {   
        $myPurchaseProduct = '';

        if(count($this->datos['myPurchaseProduct'])==0)
        {
               $myPurchaseProduct = "<tr>
                                    <td colspan='6'>No tiene ninguna Compra pendiente!!</td>

                                </tr>";
        }
        else
        {
            foreach ($this->datos['myPurchaseProduct'] as $key => $products) 
            {       
                $myPurchaseProduct .= $this->templateproductMySale;
                $myPurchaseProduct = $this->setFields('{ESTATUS_COMPRA}', $products['status'], $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{IMG_PRODUCTO}', $products['img'], $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{ID_PRODUCTO}', $products['id'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{NOMBRE_PRODUCTO}', $products['product'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{VENCIMIENTO}', $products['date_end'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{CLIENTE}', $products['seller'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{COSTO}', $products['price'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{ID_COMPRA}', $products['id_purchase'],  $myPurchaseProduct);
            }
        }

        return $myPurchaseProduct;
    }
    private function setCartDetail()
    {   
        $allProductCart = '';

        foreach ($this->datos['cartProduct'] as $key => $CART) 
        {   
            if (@count($CART)==6)
            {
                $allProductCart .= $this->templateCartElement;
                $allProductCart = $this->setFields('{IMG}', $CART['img'], $allProductCart);
                $allProductCart = $this->setFields('{ID}', $CART['id'],  $allProductCart);
                $allProductCart = $this->setFields('{NOMBRE}', $CART['nombre'],  $allProductCart);
                $allProductCart = $this->setFields('{PRECIO}', $CART['precio'],  $allProductCart);
            }
        }
        return $allProductCart;
    }
    private function setNumSales()
    {   
        $categoryFooter = '';

        foreach ($this->datos['numSales'] as $key => $category) 
        {       $categoryFooter .= $this->templateCategoryFooter;
                $categoryFooter = $this->setFields('{ID_CATEGORIA}', $category['category_id'], $categoryFooter);
                $categoryFooter = $this->setFields('{NOMBRE_CATEGORIA}', $category['name'],  $categoryFooter);
        }
        return $categoryFooter;
    }
    private function setCategoryFooter()
    {   
        $categoryFooter = '';

        foreach ($this->datos['besCategory'] as $key => $category) 
        {       $categoryFooter .= $this->templateCategoryFooter;
                $categoryFooter = $this->setFields('{ID_CATEGORIA}', $category['category_id'], $categoryFooter);
                $categoryFooter = $this->setFields('{NOMBRE_CATEGORIA}', $category['name'],  $categoryFooter);
        }
        return $categoryFooter;
    }
    private function tableSales()
    {   
        $myPurchaseProduct = '';//var_dump($this->datos['SalesTable']);die();
        if(count($this->datos['SalesTable'])==0)
        {
               $myPurchaseProduct = "<tr>
                                    <td colspan='6'>Todavia no has recibido compras</td>

                                </tr>";
        }
        else
        {
            foreach ($this->datos['SalesTable'] as $key => $products) 
            {       
                $myPurchaseProduct .= $this->table_row;
                $myPurchaseProduct = $this->setFields('{ID}', $products['id'], $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{Producto}', $products['product'], $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{Fecha}', $products['date'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{Precio}', $products['price'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{Comprador}', $products['name'],  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{mail}', $products['mail'],  $myPurchaseProduct);
                switch ($products['status']) {
                    case '-1':
                        $color="135, 135, 135,0.8";
                        $class="table-secondary";
                    break;
                    case '1':
                        $color="255, 202, 51,0.8";
                        $class="table-warning";
                    break;
                    case '2':
                        $color="76, 179, 204,0.8";
                        $class="table-primary";
                    break;
                    case '3':
                        $color="172, 255, 51,1";
                        $class="table-success";
                    break;
                    default:
                        # code...
                    break;
                }
                //$myPurchaseProduct = $this->setFields('{color}', $color,  $myPurchaseProduct);
                $myPurchaseProduct = $this->setFields('{class}', $class,  $myPurchaseProduct);
            }
        }

        return $myPurchaseProduct;
    }
}//fin class page503
 ?>