<?php 
class ProductDetail
{
    function __construct($datos)
    {
        $this->datos = $datos;
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config_::singleton(); 
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/index.html';
        //cargamos el html del template
        $this->index = file_get_contents($this->path);
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/product-detail.html';
        //cargamos el html del template
        $this->template = file_get_contents($this->path);
        $this->template = $this->setFields('{BODY}',$this->template,$this->index);
        $this->pathReviewElement = $config->get('htmlFolder').'/review_element.html';
        //cargamos el html del template
        $this->templateReviewElement = file_get_contents($this->pathReviewElement);

        $this->userMenu = $config->get('htmlFolder').'/user-menu.html';
        //cargamos el html del template
        $this->templateUserMenu = file_get_contents($this->userMenu);

        $this->cartDetail = $config->get('htmlFolder').'/cart-detail.html';
        //cargamos el html del template
        $this->templatecartDetail = file_get_contents($this->cartDetail);

        $this->cartElement = $config->get('htmlFolder').'/cart-elemnt.html';
        //cargamos el html del template
        $this->templateCartElement = file_get_contents($this->cartElement);

        $this->cartElementMobile = $config->get('htmlFolder').'/cart-elemnt-mobile.html';
        //cargamos el html del template
        $this->templateCartElementMobile = file_get_contents($this->cartElementMobile);

        $this->useMenuNotLogued = $config->get('htmlFolder').'/user-menu-not-logued.html';
        //cargamos el html del template
        $this->templateUserMenuNotLogued = file_get_contents($this->useMenuNotLogued);

        $this->productDetail = $config->get('htmlFolder').'/product-detail.html';
        //cargamos el html del template
        $this->templateproductDetail = file_get_contents($this->productDetail);

        $this->similarProduct = $config->get('htmlFolder').'/similar-products.html';
        //cargamos el html del template
        $this->templatesimilarProduct = file_get_contents($this->similarProduct);
        
        $this->categoryFooter = $config->get('htmlFolder').'/category-footer.html';
        //cargamos el html del template
        $this->templateCategoryFooter = file_get_contents($this->categoryFooter);

        $this->loginRegister = $config->get('htmlFolder').'/login-register.html';
        //cargamos el html del template
        $this->templateloginRegister = file_get_contents($this->loginRegister);

        $this->administrationOption = $config->get('htmlFolder').'/administration-option.html';
        //cargamos el html del template
        $this->templateAdministrationOption = file_get_contents($this->administrationOption);

        $this->administrationOptionMobile = $config->get('htmlFolder').'/administration-option-mobile.html';
        //cargamos el html del template
        $this->templateAdministrationOptionMobile = file_get_contents($this->administrationOptionMobile);

    }

    public function processView(){
       @session_start();
       if (isset($_SESSION['USER']))
        {

            if (@count($this->datos['productDetail'])!=0) {

                $this->template = $this->setFields('{USER}', $this->templateUserMenu, $this->template);

                $this->template = $this->setFields('{MENU_MOBILE}', $this->templateCartElementMobile, $this->template);

                $this->template = $this->setFields('{USER_MOBILE}', $this->templateUserMenu, $this->template);
                
                $this->template = $this->setFields('{USUARIO_LOGADO}', $_SESSION['USER']['NAMEUSER'], $this->template);

                $this->template = $this->setFields('{MEJORES_CATEGORIAS_DISPONILBES}', $this->setCategoryFooter(), $this->template);
                $this->template = $this->setFields('{CARRITO_DE_COMPRAS}', $this->templatecartDetail, $this->template);

                $this->template = $this->setFields('{CANTIDAD_EN_CARITO}', $this->datos['cartProduct']['articulos_total'], $this->template);
                $this->template = $this->setFields('{CANTIDAD_EN_CARITO_MOBILE}', $this->datos['cartProduct']['articulos_total'], $this->template);
                $this->template = $this->setFields('{TOTAL_CARRITO}',$this->datos['cartProduct']['totalPrice'], $this->template);
                $this->template = $this->setFields('{TOTAL_CARRITO_MOBILE}',$this->datos['cartProduct']['totalPrice'], $this->template);
                $this->template = $this->setFields('{CARRITO}', $this->setCartDetail(), $this->template);

                $this->template = $this->setFields('{CARRITO_MOBILE}', $this->setCartDetail(), $this->template);            

                $this->template = $this->setSelect('{CATEGORIAS}', $this->datos['category'], $this->template);

                $this->template = $this->setFields('{NOMBRE}', $this->datos['productDetail'][0]['name'], $this->template);

                $this->template = $this->setFields('{CATEGORIA}', $this->datos['productDetail'][0]['category'], $this->template);
                $this->template = $this->setFields('{TIPO}', $this->datos['productDetail'][0]['type'], $this->template);

                $this->template = $this->setFields('{TIPO_PRODUCTO}', $this->datos['productDetail'][0]['type'], $this->template);       
                $this->template = $this->setFields('{PRECIO}', $this->datos['productDetail'][0]['price'], $this->template);

                $this->template = $this->setFields('{DESCRIPCION}', $this->datos['productDetail'][0]['info'], $this->template);
                $this->template = $this->setFields('{IMG_PROD}', $this->datos['productDetail'][0]['img'], $this->template);

                $this->template = $this->setFields('{ID}', $this->datos['productDetail'][0]['id'], $this->template);

                $this->template = $this->setFields('{USER_ID}', $this->datos['productDetail'][0]['user_id'], $this->template);
                $this->template = $this->setFields('{NOMBRE_PRODUCTO}', $this->datos['productDetail'][0]['name'], $this->template);
                $this->template = $this->setFields('{PRECIO_PRODUCTO}', $this->datos['productDetail'][0]['price'], $this->template);

                $this->template = $this->setFields('{ESTRELLAS}', $this->datos['productDetail'][0]['stars'], $this->template);
                $this->template = $this->setFields('{IMG_PRODUCTO}', $this->datos['productDetail'][0]['img'], $this->template);
                $this->template = $this->setFields('{PRODUCTOS_SIMILARES}', $this->allSimilarProduct(), $this->template);

                $this->template = $this->setFields('{RESENA}', $this->setReviewProduct(), $this->template);

                $this->template = $this->setFields('{ID_CATEGORY}', $this->datos['productDetail'][0]['category_id'], $this->template);
                $this->template = $this->setFields('{LOGIN_REGISTRO}','', $this->template);

                $this->template = $this->setFields('{OPCION_USUARIO}',$this->templateAdministrationOption,$this->template);

                $this->template = $this->setFields('{OPCION_USUARIO_MOBILE}',$this->templateAdministrationOptionMobile,$this->template);
                $this->template = $this->setFields('{IDUSER}', $_SESSION['USER']['IDUSER'], $this->template);

            }
            else
            {
                header('Location: index.php?error=notValidProduct');
            }

        }
        else
        { 
            if (@count($this->datos['productDetail'])!=0) {

                $this->template = $this->setFields('{MENU_MOBILE}', '', $this->template);

                $this->template = $this->setFields('{USER_MOBILE}', $this->templateUserMenuNotLogued, $this->template);

                $this->template = $this->setFields('{USER}', $this->templateUserMenuNotLogued, $this->template);    
                
                $this->template = $this->setFields('{MEJORES_CATEGORIAS_DISPONILBES}', $this->setCategoryFooter(), $this->template);

                $this->template = $this->setFields('{CARRITO_DE_COMPRAS}', '', $this->template); 

                $this->template = $this->setFields('{NOMBRE}', $this->datos['productDetail'][0]['name'], $this->template);

                $this->template = $this->setFields('{CATEGORIA}', $this->datos['productDetail'][0]['category'], $this->template);

                $this->template = $this->setFields('{TIPO}', $this->datos['productDetail'][0]['type'], $this->template);

                $this->template = $this->setFields('{TIPO_PRODUCTO}', $this->datos['productDetail'][0]['type'], $this->template);       

                $this->template = $this->setFields('{PRECIO}', $this->datos['productDetail'][0]['price'], $this->template);

                $this->template = $this->setFields('{DESCRIPCION}', $this->datos['productDetail'][0]['info'], $this->template);

                $this->template = $this->setFields('{IMG_PROD}', $this->datos['productDetail'][0]['img'], $this->template);            

                $this->template = $this->setFields('{PRODUCTOS_SIMILARES}', $this->allSimilarProduct(), $this->template);

                $this->template = $this->setFields('{ID}', $this->datos['productDetail'][0]['id'], $this->template);

                $this->template = $this->setFields('{NOMBRE_PRODUCTO}', $this->datos['productDetail'][0]['name'], $this->template);
                $this->template = $this->setFields('{USER_ID}', $this->datos['productDetail'][0]['user_id'], $this->template);

                $this->template = $this->setFields('{PRECIO_PRODUCTO}', $this->datos['productDetail'][0]['price'], $this->template);

                $this->template = $this->setFields('{ESTRELLAS}', $this->datos['productDetail'][0]['stars'], $this->template);

                $this->template = $this->setFields('{IMG_PRODUCTO}', $this->datos['productDetail'][0]['img'], $this->template);

                $this->template = $this->setFields('{ID_CATEGORY}', $this->datos['productDetail'][0]['category_id'], $this->template);
                $this->template = $this->setFields('{RESENA}', $this->setReviewProduct(), $this->template);

                $this->template = $this->setFields('{LOGIN_REGISTRO}',$this->templateloginRegister, $this->template);

                $this->template = $this->setFields('{IDUSER}', 'NOTLOGUED', $this->template);

                $this->template = $this->setFields('{OPCION_USUARIO}','',$this->template);

                $this->template = $this->setFields('{OPCION_USUARIO_MOBILE}','',$this->template); 
                $this->template = $this->setFields('{LOGIN_REGISTRO}',"",$this->template);
                
            }
            else
            {
                header('Location: index.php?error=notValidProduct');
            }

        }
        echo $this->template;
    }

    private function setCartDetail()
    {   
        $allProductCart = '';

        foreach ($this->datos['cartProduct'] as $key => $CART) 
        {   if (@count($CART)==6)
            {
                $allProductCart .= $this->templateCartElement;
                $allProductCart = $this->setFields('{IMG}', $CART['img'], $allProductCart);
                $allProductCart = $this->setFields('{ID}', $CART['id'],  $allProductCart);
                $allProductCart = $this->setFields('{NOMBRE}', $CART['nombre'],  $allProductCart);
                $allProductCart = $this->setFields('{PRECIO}', $CART['precio'],  $allProductCart);
            }
        }
        return $allProductCart;
    }


    private function setReviewProduct()
    {   
        $ReviewProduct = '';

        foreach ($this->datos['reviewProduct'] as $key => $products) 
        {       $ReviewProduct .= $this->templateReviewElement;
                $ReviewProduct = $this->setFields('{NOMBRE_RESENA}', $products['name'], $ReviewProduct);
                $ReviewProduct = $this->setFields('{CONTENIDO_RESENA}', $products['review'],  $ReviewProduct);
        }
        return $ReviewProduct;
    }




    private function allSimilarProduct()
    {   
        $allSimilarProduct = '';

        foreach ($this->datos['allSimilarProduct'] as $key => $products) 
        {       $allSimilarProduct .= $this->templatesimilarProduct;
                $allSimilarProduct = $this->setFields('{IMG_PRODUCTO}', $products['img'], $allSimilarProduct);               
                $allSimilarProduct = $this->setFields('{NOMBRE_PRODUCTO}', $products['name'],  $allSimilarProduct);
                $allSimilarProduct = $this->setFields('{PRECIO_PRODUCTO}', $products['price'],  $allSimilarProduct);
                $id_loged=isset($_SESSION['USER']['IDUSER'])?$_SESSION['USER']['IDUSER']:0;
                $id_bd=$products['user_id'];
                if ($id_loged==$id_bd) {
                    $style='style="display:none"';
                   $allSimilarProduct = $this->setFields('{style}', $style,  $allSimilarProduct);
                }else{
                    $style='style="display:block"';
                   $allSimilarProduct = $this->setFields('{style}', $style,  $allSimilarProduct);
                }

                if (in_array($products['id'], $this->datos['allFavorite']))
                {
                    $allSimilarProduct = $this->setFields('{ESTILO_FAVORITO}', 'fas fa-heart fa-2x',  $allSimilarProduct);
                    $allSimilarProduct = $this->setFields('{AGREGAR_SACAR_FAVORITO}', "dellToFavorite('{ID}')",  $allSimilarProduct);
                    $allSimilarProduct = $this->setFields('{TEXTO_BOTON_FAVORITO}', "Eliminar de mis Favoritos",  $allSimilarProduct);
                    
                }
                else
                {
                    $allSimilarProduct = $this->setFields('{ESTILO_FAVORITO}', 'far fa-heart fa-2x',  $allSimilarProduct);                    
                    $allSimilarProduct = $this->setFields('{AGREGAR_SACAR_FAVORITO}', "addToFavorite('{ID}')",  $allSimilarProduct);
                    $allSimilarProduct = $this->setFields('{TEXTO_BOTON_FAVORITO}', "Agregar a mis Favoritos",  $allSimilarProduct);
                }

                $id_loged = isset($_SESSION['USER']['IDUSER'])?$_SESSION['USER']['IDUSER']:0;

                if ($id_loged == $products['user_id'])
                {                   
                   $allSimilarProduct = $this->setFields('{MOSTRAR_BOTON}','disabled',  $allSimilarProduct);
                }
                else
                {                   
                   $allSimilarProduct = $this->setFields('{MOSTRAR_BOTON}', '',  $allSimilarProduct);
                }

                $allSimilarProduct = $this->setFields('{ID}', $products['id'],  $allSimilarProduct);
        }
        return $allSimilarProduct;
    }
    private function setSelect($field, $value, $template)
    {
        $select = '';
            foreach ($value as $key => $value3) {
                    $select .=  "<option value='".$value3['id']."'>".$value3['name']."</option>";
            }                                              
        $template = str_replace($field, $select, $template);
        return $template;
    }//fin select

    private function setTable($field, $value, $template)
    { 
        $table = '';
        foreach ($value as $key2 => $value2)
        {             
            foreach ($value2 as $key => $value3) {
                $table .=  "<tr>";
                    $table .=  "<td>".$value3['id']."</td>";
                    $table .=  "<td>".$value3['nicname']."</td>";
                    $table .=  "<td>".$value3['user']."</td>";
                    $table .=  "<td>".$value3['status']."</td>";
                $table .=  "</tr>";
            }                                              
        }       
        $template = str_replace($field, $table, $template);  
        return $template;
    }//fin table
 
    private function setCategoryFooter()
    {   
        $categoryFooter = '';

        foreach ($this->datos['besCategory'] as $key => $category) 
        {       $categoryFooter .= $this->templateCategoryFooter;
                $categoryFooter = $this->setFields('{ID_CATEGORIA}', $category['category_id'], $categoryFooter);
                $categoryFooter = $this->setFields('{NOMBRE_CATEGORIA}', $category['name'],  $categoryFooter);
        }
        return $categoryFooter;
    }
    private function setFields($field, $value, $template)
    { 
        return str_replace($field, $value, $template);
    }//fin setFields
}//fin class vista listarusuarios
 ?>