<?php 
class index
{
	private $titulo;
	private $idUser;
	private $nicName;
	private $user;
    private $selects;
    private $tables;
    private $path;

	function __construct($datos)
	{
        $this->datos = $datos;
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config_::singleton(); 
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/index.html';
        //cargamos el html del template
        $this->index = file_get_contents($this->path);
        //Armamos la ruta a la plantilla
        $this->path = $config->get('htmlFolder').'/home.html';
        //cargamos el html del template
        $this->template = file_get_contents($this->path);
        $this->template = $this->setFields('{BODY}',$this->template,$this->index);
        $this->userMenu = $config->get('htmlFolder').'/user-menu.html';
        //cargamos el html del template
        $this->templateUserMenu = file_get_contents($this->userMenu);
        //cargamos el html del template
        $this->cartDetail = $config->get('htmlFolder').'/cart-detail.html';
        //cargamos el html del template
        $this->templatecartDetail = file_get_contents($this->cartDetail);

        $this->cartElement = $config->get('htmlFolder').'/cart-elemnt.html';
        //cargamos el html del template
        $this->templateCartElement = file_get_contents($this->cartElement);

        $this->cartElementMobile = $config->get('htmlFolder').'/cart-elemnt-mobile.html';
        //cargamos el html del template
        $this->templateCartElementMobile = file_get_contents($this->cartElementMobile);

        $this->productToEnd = $config->get('htmlFolder').'/product-element-to-end.html';
        //cargamos el html del template
        $this->templateProductToEnd = file_get_contents($this->productToEnd);

        $this->productToWeek = $config->get('htmlFolder').'/best-product-to-week.html';
        //cargamos el html del template
        $this->templateproductToWeek = file_get_contents($this->productToWeek);

        $this->userMenuNotLogued = $config->get('htmlFolder').'/user-menu-not-logued.html';
        //cargamos el html del template
        $this->templateUserMenuNotLogued = file_get_contents($this->userMenuNotLogued);

        $this->categoryFooter = $config->get('htmlFolder').'/category-footer.html';
        //cargamos el html del template
        $this->templateCategoryFooter = file_get_contents($this->categoryFooter);

        $this->loginRegister = $config->get('htmlFolder').'/login-register.html';
        //cargamos el html del template
        $this->templateloginRegister = file_get_contents($this->loginRegister);

        $this->bestSellerProduct = $config->get('htmlFolder').'/best-seller-product.html';
        //cargamos el html del template
        $this->templatebestSellerProduct = file_get_contents($this->bestSellerProduct);

        $this->bestOfCategory = $config->get('htmlFolder').'/best-of-category.html';
        //cargamos el html del template
        $this->templatebestOfCategory = file_get_contents($this->bestOfCategory);

        $this->administrationOption = $config->get('htmlFolder').'/administration-option.html';
        //cargamos el html del template
        $this->templateAdministrationOption = file_get_contents($this->administrationOption);

        $this->administrationOptionMobile = $config->get('htmlFolder').'/administration-option-mobile.html';
        //cargamos el html del template
        $this->templateAdministrationOptionMobile = file_get_contents($this->administrationOptionMobile);
	}

    public function processView(){
       @session_start();
       if (isset($_SESSION['USER']))
        {

            $this->template = $this->setFields('{USER}', $this->templateUserMenu, $this->template);

            $this->template = $this->setFields('{MENU_MOBILE}', $this->templateCartElementMobile, $this->template);

            $this->template = $this->setFields('{USER_MOBILE}', $this->templateUserMenu, $this->template);

            $this->template = $this->setFields('{USUARIO_LOGADO}', $_SESSION['USER']['NAMEUSER'], $this->template);

            $this->template = $this->setFields('{MEJORES_CATEGORIAS_DISPONILBES}', $this->setCategoryFooter(), $this->template);    

            $this->template = $this->setFields('{CARRITO_DE_COMPRAS}', $this->templatecartDetail, $this->template);

            $this->template = $this->setFields('{CANTIDAD_EN_CARITO}', $this->datos['cartProduct']['articulos_total'], $this->template);
            $this->template = $this->setFields('{CANTIDAD_EN_CARITO_MOBILE}', $this->datos['cartProduct']['articulos_total'], $this->template);
            $this->template = $this->setFields('{TOTAL_CARRITO}',$this->datos['cartProduct']['totalPrice'], $this->template);

            $this->template = $this->setFields('{TOTAL_CARRITO_MOBILE}',$this->datos['cartProduct']['totalPrice'], $this->template);
            $this->template = $this->setFields('{CARRITO}', $this->setCartDetail(), $this->template);

            $this->template = $this->setFields('{CARRITO_MOBILE}', $this->setCartDetail(), $this->template);            

            $this->template = $this->setFields('{PRODUCTO_DESTACADO_POR_VENCER}',$this->templateProductToEnd, $this->template);

            $this->template = $this->setFields('{ID_PRODUCTO_POR_VENCER}',$this->datos['productToEnd'][0]['id'], $this->template);

            $this->template = $this->setFields('{IMG_PRODUCTO_POR_VENCER}',$this->datos['productToEnd'][0]['img'], $this->template);

            $this->template = $this->setFields('{NOMBRE_PRODUCTO_POR_VENCER}',$this->datos['productToEnd'][0]['name'], $this->template);
            $this->template = $this->setFields('{PRECIO_PRODUCTO_POR_VENCER}',$this->datos['productToEnd'][0]['price'], $this->template);
            $this->template = $this->setFields('{DIAS_PRODUCTO_POR_VENCER}',$this->calculateDif($this->datos['productToEnd'][0]['date_end'])[0], $this->template);

            $this->template = $this->setFields('{DATE_END}',$this->datos['productToEnd'][0]['date_end'], $this->template);

            $this->template = $this->setFields('{HORAS_PRODUCTO_POR_VENCER}',$this->calculateDif($this->datos['productToEnd'][0]['date_end'])[1], $this->template);

            $this->template = $this->setFields('{MINUTOS_PRODUCTO_POR_VENCER}',$this->calculateDif($this->datos['productToEnd'][0]['date_end'])[2], $this->template);

            $this->template = $this->setFields('{PRODUCTO_DESTACADO_DE_LA_SEMANA}',$this->templateproductToWeek, $this->template);

            @$this->template = $this->setFields('{ID_PRODUCTO}',$this->datos['bestProductToWeek'][0]['id'], $this->template);

            @$this->template = $this->setFields('{IMG_PRODUCTO_SEMANA}',$this->datos['bestProductToWeek'][0]['img'], $this->template);
            @$this->template = $this->setFields('{NOMBRE_PRODUCTO_SEMANA}',$this->datos['bestProductToWeek'][0]['name'], $this->template);
            @$this->template = $this->setFields('{PRECIO_PRODUCTO_SEMANA}',$this->datos['bestProductToWeek'][0]['price'], $this->template);
            $this->template = $this->setFields('{LOGIN_REGISTRO}','', $this->template);

            $this->template = $this->setFields('{PRODUCTOS_DESTACADOS}', $this->setBestSeller(), $this->template);

            $this->template = $this->setFields('{PRODUCTOS_DESTACADOS_POR_CATEGORIA}', $this->setBestOfCategory(), $this->template);

            $this->template = $this->setFields('{PRODUCTOS_20OFF_SIN_LOGAR}','', $this->template);

            $this->template = $this->setFields('{OPCION_USUARIO}',$this->templateAdministrationOption,$this->template);

            $this->template = $this->setFields('{OPCION_USUARIO_MOBILE}',$this->templateAdministrationOptionMobile,$this->template);
        
            $this->template = $this->setFields('{IDUSER}', $_SESSION['USER']['IDUSER'], $this->template);

        }
        else
        {            
            $this->template = $this->setFields('{MENU_MOBILE}', '', $this->template);

            $this->template = $this->setFields('{USER_MOBILE}', $this->templateUserMenuNotLogued, $this->template);

            $this->template = $this->setFields('{USER}', $this->templateUserMenuNotLogued, $this->template);    
            
            $this->template = $this->setFields('{MEJORES_CATEGORIAS_DISPONILBES}', $this->setCategoryFooter(), $this->template);

            $this->template = $this->setFields('{CARRITO_DE_COMPRAS}', '', $this->template);  
      
            $this->template = $this->setFields('{PRODUCTOS_DESTACADOS_POR_TIEMPO}',$this->templateProductToEnd, $this->template);

            $this->template = $this->setFields('{LOGIN_REGISTRO}',$this->templateloginRegister, $this->template);

            $this->template = $this->setFields('{PRODUCTOS_DESTACADOS}', $this->setBestSeller(), $this->template);

            $this->template = $this->setFields('{PRODUCTOS_DESTACADOS_POR_CATEGORIA}', $this->setBestOfCategory(), $this->template);        

            $this->template = $this->setFields('{IDUSER}', 'NOTLOGUED', $this->template); //ULTIMO

            $this->template = $this->setFields('{PRODUCTO_DESTACADO_POR_VENCER}',$this->templateProductToEnd, $this->template);

            @$this->template = $this->setFields('{ID_PRODUCTO_POR_VENCER}',$this->datos['productToEnd'][0]['id'], $this->template);

            @$this->template = $this->setFields('{IMG_PRODUCTO_POR_VENCER}',$this->datos['productToEnd'][0]['img'], $this->template);

            @$this->template = $this->setFields('{NOMBRE_PRODUCTO_POR_VENCER}',$this->datos['productToEnd'][0]['name'], $this->template);
            @$this->template = $this->setFields('{PRECIO_PRODUCTO_POR_VENCER}',$this->datos['productToEnd'][0]['price'], $this->template);
            @$this->template = $this->setFields('{DIAS_PRODUCTO_POR_VENCER}',$this->calculateDif($this->datos['productToEnd'][0]['date_end'])[0], $this->template);

            @$this->template = $this->setFields('{HORAS_PRODUCTO_POR_VENCER}',$this->calculateDif($this->datos['productToEnd'][0]['date_end'])[1], $this->template);

            @$this->template = $this->setFields('{MINUTOS_PRODUCTO_POR_VENCER}',$this->calculateDif($this->datos['productToEnd'][0]['date_end'])[2], $this->template);

            @$this->template = $this->setFields('{DATE_END}',$this->datos['productToEnd'][0]['date_end'], $this->template);

            $this->template = $this->setFields('{PRODUCTO_DESTACADO_DE_LA_SEMANA}',$this->templateproductToWeek, $this->template);
            
            @$this->template = $this->setFields('{ID_PRODUCTO}',$this->datos['bestProductToWeek'][0]['id'], $this->template);

            @$this->template = $this->setFields('{IMG_PRODUCTO_SEMANA}',$this->datos['bestProductToWeek'][0]['img'], $this->template);
            @$this->template = $this->setFields('{NOMBRE_PRODUCTO_SEMANA}',$this->datos['bestProductToWeek'][0]['name'], $this->template);
            @$this->template = $this->setFields('{PRECIO_PRODUCTO_SEMANA}',$this->datos['bestProductToWeek'][0]['price'], $this->template);

            $this->template = $this->setFields('{OPCION_USUARIO}','',$this->template);

            $this->template = $this->setFields('{OPCION_USUARIO_MOBILE}','',$this->template);

        }
        echo $this->template;
    }

    private function setCartDetail()
    {   
        $allProductCart = '';

        foreach ($this->datos['cartProduct'] as $key => $CART) 
        {   if (@count($CART)==6)
            {
                $allProductCart .= $this->templateCartElement;
                $allProductCart = $this->setFields('{IMG}', $CART['img'], $allProductCart);
                $allProductCart = $this->setFields('{ID}', $CART['id'],  $allProductCart);
                $allProductCart = $this->setFields('{NOMBRE}', $CART['nombre'],  $allProductCart);
                $allProductCart = $this->setFields('{PRECIO}', $CART['precio'],  $allProductCart);
            }
        }
        return $allProductCart;
    }

    private function setSelect($field, $value, $template)
    {
        $select = '';
            foreach ($value as $key => $value3) {
                    $select .=  "<option value='".$value3['id']."'>".$value3['name']."</option>";
            }                                              
        $template = str_replace($field, $select, $template);
        return $template;
    }//fin select

    private function setTableUsuarios($field, $value, $template)
    { 
        $table = '';
        foreach ($value as $key2 => $value2)
        {             
            foreach ($value2 as $key => $value3) {
                $table .=  "<tr>";
                    $table .=  "<td>".$value3['id']."</td>";
                    $table .=  "<td>".$value3['nicname']."</td>";
                    $table .=  "<td>".$value3['user']."</td>";
                    $table .=  "<td>".$value3['status']."</td>";
                $table .=  "</tr>";
            }                                              
        }       
        $template = str_replace($field, $table, $template);  
        return $template;
    }//fin table

    private function setProductToEnd()
    {   
        $productToEnd = '';

        foreach ($this->datos['productToEnd'] as $key => $products) 
        {       $productToEnd .= $this->templateproductToEnd;
                $productToEnd = $this->setFields('{IMG_PRODUCTO}', $products['img'], $productToEnd);
                $productToEnd = $this->setFields('{ID}', $products['id'],  $productToEnd);
                $productToEnd = $this->setFields('{NOMBRE_PRODUCTO}', $products['name'],  $productToEnd);
                $productToEnd = $this->setFields('{PRECIO_PRODUCTO}', $products['price'],  $productToEnd);
        }
        return $productToEnd;
    }

    private function setBestSeller()
    {   
        $BestSeller = '';

        foreach ($this->datos['bestSeller'] as $key => $products) 
        {       $BestSeller .= $this->templatebestSellerProduct;
                $BestSeller = $this->setFields('{IMG_PRODUCTO}', $products['img'], $BestSeller);                
                $BestSeller = $this->setFields('{NOMBRE_PRODUCTO}', $products['name'],  $BestSeller);
                $BestSeller = $this->setFields('{PRECIO_PRODUCTO}', $products['price'],  $BestSeller);
                $id_loged=isset($_SESSION['USER']['IDUSER'])?$_SESSION['USER']['IDUSER']:0;
                $id_bd=$products['user_id'];
                if ($id_loged==$id_bd) {
                    $style='style="display:none"';
                   $BestSeller = $this->setFields('{style}', $style,  $BestSeller);
                }else{
                    $style='style="display:block"';
                   $BestSeller = $this->setFields('{style}', $style,  $BestSeller);
                }
                if (in_array($products['id'], $this->datos['allFavorite']))
                {
                    $BestSeller = $this->setFields('{ESTILO_FAVORITO}', 'fas fa-heart fa-2x',  $BestSeller);
                    $BestSeller = $this->setFields('{AGREGAR_SACAR_FAVORITO}', "dellToFavorite('{ID}')",  $BestSeller);
                    $BestSeller = $this->setFields('{TEXTO_BOTON_FAVORITO}', "Eliminar de mis Favoritos",  $BestSeller);
                    
                }
                else
                {
                	$BestSeller = $this->setFields('{ESTILO_FAVORITO}', 'far fa-heart fa-2x',  $BestSeller);                    
                    $BestSeller = $this->setFields('{AGREGAR_SACAR_FAVORITO}', "addToFavorite('{ID}')",  $BestSeller);
                    $BestSeller = $this->setFields('{TEXTO_BOTON_FAVORITO}', "Agregar a mis Favoritos",  $BestSeller);
                }

                $BestSeller = $this->setFields('{ID}', $products['id'],  $BestSeller);
        }
        return $BestSeller;
    }

    private function setBestOfCategory()
    {   
        $BestOfCategory = '';

        foreach ($this->datos['bestOfCategory'] as $key => $products) 
        {       $BestOfCategory .= $this->templatebestOfCategory;
                $BestOfCategory = $this->setFields('{IMG_PRODUCTO}', $products['img'], $BestOfCategory);
                $BestOfCategory = $this->setFields('{NOMBRE_PRODUCTO}', $products['name'],  $BestOfCategory);
                $BestOfCategory = $this->setFields('{CATEGORIA}', $products['category'],  $BestOfCategory);
                $BestOfCategory = $this->setFields('{ID_CATEGORIA}', $products['category_id'],  $BestOfCategory);
        }
        return $BestOfCategory;
    }

    private function setCategoryFooter()
    {   
        $categoryFooter = '';

        foreach ($this->datos['besCategory'] as $key => $category) 
        {       $categoryFooter .= $this->templateCategoryFooter;
                $categoryFooter = $this->setFields('{ID_CATEGORIA}', $category['category_id'], $categoryFooter);
                $categoryFooter = $this->setFields('{NOMBRE_CATEGORIA}', $category['name'],  $categoryFooter);
        }
        return $categoryFooter;
    }

    private function setFields($field, $value, $template)
    { 
        return str_replace($field, $value, $template);
    }//fin setFields

    private function calculateDif($date_end)
    {
        $timestamp = strtotime($date_end.' 23:59:59')-strtotime(date('Y-m-d H:i:s'));

        $return="";
        # Obtenemos el numero de dias
        $days=floor((($timestamp/60)/60)/24);
        if($days>0)
        {
            $timestamp-=$days*24*60*60;
            $return.=$days."-";
        }
        # Obtenemos el numero de horas
        $hours=floor(($timestamp/60)/60);
        if($hours>0)
        {
            $timestamp-=$hours*60*60;
            $return.=str_pad($hours, 2, "0", STR_PAD_LEFT)."-";
        }else
            $return.="00-";
        # Obtenemos el numero de minutos
        $minutes=floor($timestamp/60);
        if($minutes>0)
        {
            $timestamp-=$minutes*60;
            $return.=str_pad($minutes, 2, "0", STR_PAD_LEFT)."-";
        }else
            $return.="00-";
        # Obtenemos el numero de segundos
        $return.=str_pad($timestamp, 2, "0", STR_PAD_LEFT);
        
        $stringDif= explode('-', $return);
        return $stringDif;
    }
}//fin class vista listarusuarios
 ?>