<?php
//Incluimos el FrontController
require 'libs/FrontController.php';
//Lo iniciamos con su método estático main.
FrontController::main();


if (isset($_GET['MSG'])) {
	if ($_GET['MSG']=='userisnotlogued') {
		$message = "Debe estar logueado para utilizar la aplicacion correctamente!!";		
	}
	if ($_GET['MSG']=='UserOrPassERROR') {
		$message = "El usuario o contraseña ingresados, son incorrectos!!";		
	}
	if ($_GET['MSG']=='addListOK') {
		$message = "Se agrego exitosamente su email al mail lister.";		
	}
	if ($_GET['MSG']=='IMGSize') {
		$message = "El tamaño de la imagen supera el limite.";		
	}
	if ($_GET['MSG']=='IMGFormat') {
		$message = "El tipo de images es incorrecto.";		
	}
	if ($_GET['MSG']=='IMGError') {
		$message = "Se produjo un error al intentar cargar la imagen seleccionada.";		
	}
	if ($_GET['MSG']=='UserAllowed') {
		$message = "Tu usuario fue habilitado con exito.";		
	}

	if ($message!='') {
			echo "<script type='text/javascript'>swal({ title: '".$message."',animation: false,customClass: 'animated tada'}).then(function(){});</script>";
	}
}
?>