<?php
class View
{
    public function show($viewName, $datos)
    {       
        //Traemos una instancia de nuestra clase de configuracion.
        $config = Config_::singleton(); 
        //Armamos la ruta a la plantilla
        $path = $config->get('viewsFolder') . $viewName.'.php';

        if (file_exists($path) == false)
        {
            header("Location:index.php?503");
        }
        else
        {   
            //incluimos la vista que corresponde
            include $path; 
            $view = new $viewName($datos);
            $view->processView();
        }      
    }//fin show  
}
?>