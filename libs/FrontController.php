<?php

date_default_timezone_set("America/Montevideo");
set_time_limit(200);
class FrontController
{
    static function main()
    {
        //Incluimos algunas clases:        
        require 'libs/Config_.php'; //de configuracion
        require 'libs/SPDO.php'; //PDO con singleton
        require 'libs/View.php'; //motor de plantillas      
    
        require 'config.php'; //Archivo con configuraciones.
    
        //librerias externas
        require "libs/PHPMailer/PHPMailerAutoload.php";

        //Con el objetivo de no repetir nombre de clases, nuestros controladores
        //terminarán todos en Controller. Por ej, la clase controladora Product, será ProductController
        if(isset($_GET['503']))
        {
            require $config->get('controllersFolder').'PageErrorController.php';
            $PageErrorController = new PageErrorController();
            $PageErrorController->Page503();
        }
        else if (isset($_GET['Productos']))//se visualizan los productos disponibles 
        {
            require $config->get('controllersFolder').'ProductController.php';
            $viewProduct = new ProductController();
            $viewProduct->viewProduct();
        }
        else if (isset($_GET['Administracion']))//usuario nos contacta
        {
            require $config->get('controllersFolder').'GraficController.php';
            $GraficController = new GraficController();
            $GraficController->viewAdministration();
        }
        else if (isset($_GET['Nosotros']))//usuario lee nuestra reseña empresarial
        {
            require $config->get('controllersFolder').'AboutController.php';
            $viewAbout = new AboutController();
            $viewAbout->viewAbout();
        }
        else if (isset($_GET['Contacto']))//usuario nos contacta
        {
            require $config->get('controllersFolder').'ContactController.php';
            $viewContact = new ContactController();
            $viewContact->viewContact();
        }
        else if (isset($_GET['userLogout']))//usuario lee nuestra reseña empresarial
        {
            @session_start();
            @session_destroy();
            header('Location:index.php');
        }
        else if (isset($_GET['Cart']))//usuario revisa el carrito
        {
            require $config->get('controllersFolder').'CartController.php';
            $viewCart = new CartController();
            $viewCart->viewCart();
        }
        else if (isset($_GET['ProductById']))//usuario revisa el carrito
        {
            require $config->get('controllersFolder').'ProductController.php';
            $getProductById = new ProductController();
            $getProductById->getProductById();
        }
        else if (isset($_GET['ProductByCategory']))//usuario revisa el carrito
        {
            require $config->get('controllersFolder').'ProductController.php';
            $getProductById = new ProductController();
            $getProductById->getProductByCategory();
        }
        else if (isset($_GET['ProductByType']))//usuario revisa el carrito
        {
            require $config->get('controllersFolder').'ProductController.php';
            $getProductById = new ProductController();
            $getProductById->getProductByCategory();
        }
        else if (isset($_GET['viewMySale']))//usuario revisa el carrito
        {
            require $config->get('controllersFolder').'SaleController.php';
            $mySale = new SaleController();
            $mySale->viewMySale();
        }
        else if (isset($_GET['viewMyPurchase']))//usuario revisa el carrito
        {
            require $config->get('controllersFolder').'PurchaseController.php';
            $myPurchase = new PurchaseController();
            $myPurchase->viewMyPurchase();
        }
        else if (isset($_GET['viewMyFavorite']))//usuario revisa el carrito
        {
            require $config->get('controllersFolder').'FavoriteController.php';
            $myPurchase = new FavoriteController();
            $myPurchase->getMyFavorite();
        }       
        else if (isset($_GET['SendEmailSales']))//sistema envia un email
        {
            require $config->get('controllersFolder').'EmailController.php';
            $EmailController = new EmailController(); //restan definir
            $EmailController->SendSales($_GET['idUser']);
        }
        else if (isset($_GET['MailListWeekly']))//sistema envia un email
        {
            require $config->get('controllersFolder').'EmailController.php';
            $EmailController = new EmailController(); //restan definir
            $EmailController->SendMailList();
        }
        else if (isset($_GET['SendEmailRegister']))//sistema envia un email
        {
            require $config->get('controllersFolder').'EmailController.php';
            $EmailController = new EmailController(); //restan definir
            $EmailController->SendRegister($_GET['mailUser']);
        }
        else if (isset($_GET['SendEmailContact']))//sistema envia un email
        {
            require $config->get('controllersFolder').'EmailController.php';
            $EmailController = new EmailController(); //restan definir
            $data = array('name'=>$_GET['name'],
                          'phone'=>$_GET['phone'],
                          'email'=>$_GET['email'],
                          'comment'=>$_GET['comment']
                        );
            $EmailController->SendMailContact($data);
        }
        else if (isset($_GET['SendEmailLister']))//sistema envia un email
        {
            require $config->get('controllersFolder').'EmailController.php';
            $EmailController = new EmailController(); //restan definir
            //$EmailController->SendSales();
        }
        else if (isset($_POST['addUser']))//registro de usuario
        {
            require $config->get('controllersFolder').'UsersController.php';
            $addUser = new UsersController();
            $addUser->addUser();
        }
        else if (isset($_GET['addUserMailLister']))//registro de usuario
        {
            require $config->get('controllersFolder').'UsersController.php';
            $setUserMailLister = new UsersController();
            $setUserMailLister->setUserMailLister();
        }
        else if (isset($_GET['updateUserData']))//registro de usuario
        {
            require $config->get('controllersFolder').'UsersController.php';
            $updateUser = new UsersController();
            $updateUser->updateUser();
        }
        else if (isset($_GET['closeSales']))//registro de usuario
        {
            require $config->get('controllersFolder').'SaleController.php';
            $SaleController = new SaleController();
            $SaleController->setCloseSale();
        }
        else if (isset($_GET['dellPurchase']))//registro de usuario
        {
            require $config->get('controllersFolder').'PurchaseController.php';
            $PurchaseController = new PurchaseController();
            $PurchaseController->setClosePurchase();
        }
        else if (isset($_GET['addComent']))//
        {
            require $config->get('controllersFolder').'PurchaseController.php';
            $PurchaseController = new PurchaseController();
            $PurchaseController->setAddComent();
        }
        else if (isset($_POST['loginUser']))//login de usuario
        {
            require $config->get('controllersFolder').'UsersController.php';
            $loginUser = new UsersController();
            $loginUser->loginUser();             
        }
        else if (isset($_GET['viewAddProduct']))//usuario agrega nuevo producto
        {
            require $config->get('controllersFolder').'ProductController.php';
            $viewAddProduct = new ProductController();       
            $viewAddProduct->viewAddProduct();
        }
        else if (isset($_POST['addProduct']))//usuario agrega nuevo producto
        {
            require $config->get('controllersFolder').'ProductController.php';
            $addProduct = new ProductController();
            $addProduct->addProduct();            
        }
        else if (isset($_GET['allowUser']))//usuario agrega nuevo producto
        {
            require $config->get('controllersFolder').'UsersController.php';
            $allowUser = new UsersController();
            $allowUser->allowUser($_GET['allowUser']);
            header("Location:index.php?MSG=UserAllowed");           
        }
        else if (isset($_GET['addProductCart']))//usuario agrega nuevo producto al carrito
        {
            require $config->get('controllersFolder').'CartController.php';
            $addCart = new CartController();
            $articulo = array(
                "idUser"=>$_GET['idUser'],
                "id" => $_GET['id'],
                "nombre" => $_GET['name'],
                "precio" => $_GET['price'],
                "img"=>$_GET['img'] 
            );

            return $addCart->addProduct($articulo);
        }
        else if (isset($_POST['addLocal']))//usuario agrega nuevo local
        {
            require $config->get('controllersFolder').'LocalController.php';
            $LocalController = new LocalController();
            $local = array(
                "idUser"=>$_POST['idUser'],
                "localName" => $_POST['localName'],
                "localAddress" => $_POST['localAddress'],
                "localSchedule"=>$_POST['localSchedule'],
                "localPhone"=>$_POST['localPhone']
            );
            $LocalController->setLocal($local);
            header("Location:index.php?newLocalOk");
        }
        else if (isset($_GET['dellProductCart']))//usuario elimina producto del carrito
        {
            require $config->get('controllersFolder').'CartController.php';
            $addCart = new CartController();
            $articulo = array(
                "idProduct"=>$_GET['idProduct']
            );
            $addCart->removeProduct($articulo);
            return true;
        }
        else if (isset($_GET['endPurchase']))//usuario agrega nuevo producto
        {
            require $config->get('controllersFolder').'PurchaseController.php';
            $purchase = new PurchaseController();

            $purchase->endPurchase();
        }
        else if (isset($_GET['viewProfile']))//usuario agrega nuevo producto
        {
            require $config->get('controllersFolder').'UsersController.php';
            $user = new UsersController();
            $user->myProfile();
        }  
        else if (isset($_GET['addFavorite']))//usuario agrega nuevo producto
        {
            require $config->get('controllersFolder').'FavoriteController.php';
            $addFavorite = new FavoriteController();
            $product = array(
                "idUser" => $_GET['idUser'],
                "idProduct" => $_GET['idProduct'],
                "status"=>1
            );
            $addFavorite->setFavorite($product);
            return true;
        }
        else if (isset($_GET['dellFavorite']))//usuario agrega nuevo producto
        {
            require $config->get('controllersFolder').'FavoriteController.php';
            $addFavorite = new FavoriteController();
            $product = array(
                "idUser" => $_GET['idUser'],
                "idProduct" => $_GET['idProduct'],
                "status"=>2
            );
            $addFavorite->setFavorite($product);
            return true;
        }
        else if (isset($_GET['getNumSales']))//usuario agrega nuevo producto
        {
            @session_start();
            if(isset($_SESSION['USER']['IDUSER']))
            {
                require $config->get('controllersFolder').'GraficController.php';
                $GraficController = new GraficController();  

                $NumSales = $GraficController->getNumSales($_SESSION['USER']['IDUSER']);         
                echo trim($NumSales[0][0]);              
            }
            else
            {
                echo 0;
            }                        
        }
        else if (isset($_GET['getNumPurchase']))//usuario agrega nuevo producto
        {
            @session_start();
            if(isset($_SESSION['USER']['IDUSER']))
            {
                require $config->get('controllersFolder').'GraficController.php';
                $GraficController = new GraficController(); 

                $NumSales = $GraficController->getNumPurchase($_SESSION['USER']['IDUSER']);         
                echo trim($NumSales[0][0]);              
            }
            else
            {
                echo 0;
            }                           
        }
        else
        {
            require $config->get('controllersFolder').'IndexController.php';
            $index = new IndexController();
            $index->viewIndex();
        }
    }
}
?>



