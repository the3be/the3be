<?php 
class FavoriteModel
{
    protected $db;
 
    public function __construct()
    {
        //Traemos la única instancia de PDO
        $this->db = SPDO::singleton();
    }

	public function setFavorite($newFavorite)
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_add_favorite(:idUser,:idProduct,:status);");
        $consulta->bindParam(':idUser',$newFavorite['idUser']);
        $consulta->bindParam(':idProduct',$newFavorite['idProduct']);
        $consulta->bindParam(':status',$newFavorite['status']);

        return $consulta->execute();
	}

    public function getFavoriteFromId($idUser)
    {
        $consulta = $this->db->prepare("call sp_get_favorites(:idUser);");
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_COLUMN, 0);        
    }

    public function getMyFavorite($idUser)
    {
        $consulta = $this->db->prepare("call sp_get_favorites(:idUser);");
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }
}
?>