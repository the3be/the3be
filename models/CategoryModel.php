<?php 
class CategoryModel
{
    protected $db;
 
    public function __construct()
    {
        //Traemos la única instancia de PDO
        $this->db = SPDO::singleton();
    }
	public function allCategory()
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_category();");
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
	}

    public function bestCategory()
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_best4_category();");
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }
}
?>