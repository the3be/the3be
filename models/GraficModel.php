<?php
class GraficModel
{
    protected $db;
 
    public function __construct()
    {
        //Traemos la única instancia de PDO
        $this->db = SPDO::singleton();
    }
	public function getQuantityByCategorySale()
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_category_quantity();");
        $consulta->execute();
        if($consulta->rowCount()>0){
        	$return= $consulta->fetchall(PDO::FETCH_NUM);
        	
        }else{
		 	$return=array(
			    0=> "0",
			    1=> "0"
			  );
		 	$return=array($return);

		}
		return $return;
	}
	public function getNumSales($idUser)
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_num_sales(:idUser);");
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        
        if($consulta->rowCount()>0){
        	$return= $consulta->fetchall(PDO::FETCH_NUM);

        }else{
			$return=array(
			    0=> "0",
			    1=> "0"
			  );
			$return=array($return);
		}
		$return=$return[0];
		return $return;
	}
    public function getSalesTable($idUser)
    {
        $consulta = $this->db->prepare("call sp_get_sales_table(:idUser)");
        
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }
	public function getNumPurchase($idUser)
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_num_purchases(:idUser);");
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        
        if($consulta->rowCount()>0){
        	$return= $consulta->fetchall(PDO::FETCH_NUM);

        }else{
			$return=array(
			    0=> "0",
			    1=> "0"
			  );
			$return=array($return);
		}
		$return=$return[0];
		return $return;
	}
	public function getSaleByProduct($idUser)
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_chart_by_id(:idUser);");
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        
        if($consulta->rowCount()>0){
        	$return= $consulta->fetchall(PDO::FETCH_NUM);

        }else{
			$return=array(
			    0=> "0",
			    1=> "0"
			  );
			$return=array($return);
		}
		return $return;
	}

	public function getEarnings($idUser)
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_earnings(:idUser);");
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        
        if($consulta->rowCount()>0){
        	$return= $consulta->fetchall(PDO::FETCH_NUM);

        }else{
			$return=array(
			    0=> "0",
			    1=> "0"
			  );
			$return=array($return);
		}
		return $return;
	}

	public function getProductSells($idProduct,$idUser)
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_product_sells(:idProduct,:idUser);");
        $consulta->bindParam(':idUser',$idUser);
        $consulta->bindParam(':idProduct',$idProduct);
        $consulta->execute();
        
        if($consulta->rowCount()>0){
        	$return= $consulta->fetchall(PDO::FETCH_NUM);

        }else{
		    $return=array(
		    	0=> "0",
		    	1=> "0"
		  	  );
		    $return=array($return);
        }
		return $return;
	}
}
 ?>