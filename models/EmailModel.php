<?php 
class EmailModel
{
	
	private $host       = "smtp.gmail.com";
    private $from       = "ventas@the3be.com";
    private $nameFrom   = "The3Be";
    private $emailFrom  = "threebeses@gmail.com";
    private $pass       = "Lima1747";    
    private $SMTPOptions;

    public $SubjectMail;
    public $BodyMail;
    public $Destination;
    public $emailCopy;//"threebeses@gmail.com";
    public $AddEmbeddedImage = array();


	function __construct()
	{
		$this->SMTPOptions = array('ssl' => array(
						                'verify_peer' => false,
						                'verify_peer_name' => false,
						                'allow_self_signed' => true
						            )
						        );
		$this->mail= new PHPmailer(); 
	}

	public function send()
    {
        $this->mail->isSMTP();
        $this->mail->SMTPOptions = $this->SMTPOptions;
        $this->mail->Host = $this->host;
        $this->mail->SMTPAuth = true;
        $this->mail->setFrom($this->from,$this->nameFrom);
        $this->mail->Username = $this->emailFrom; 
        $this->mail->Password = $this->pass;
        $this->mail->SMTPSecure = "tls";
        $this->mail->Port = 587;        
        foreach ($this->Destination as $key => $mail) {
            $this->mail->addAddress($mail);
        }
        /*foreach ($this->emailCopy as $key => $mail) {
            $this->mail->addBCC($mail);
        }*/
        $this->mail->isHTML(true); 
        $this->mail->Subject = $this->SubjectMail;          
        $this->mail->Body = $this->BodyMail;

        foreach ($this->AddEmbeddedImage as $key => $embedded) {
        	$this->mail->AddEmbeddedImage($embedded['address'],$embedded['cid'],$embedded['name']);        
        }		
		$this->sendStatus = $this->mail->send();
        if($this->sendStatus) { 
           $this->mail->smtpClose();        
        }
        return $this->sendStatus;
    }//end sendmail
}
 ?>