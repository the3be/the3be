<?php
class PurchaseModel
{
    protected $db;
 
    public function __construct()
    {
        //Traemos la única instancia de PDO
        $this->db = SPDO::singleton();
    }
    
    public function setPurchase($idUser,$idProduct)
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("CALL sp_add_purchase(:idUser,:idProduct,'')"); 

        $consulta->bindParam(':idUser', $idUser);
        $consulta->bindParam(':idProduct', $idProduct);
        
        return $consulta->execute();       
    }//fin set user

    public function getMyPurchase($idUser)
    {
        $consulta = $this->db->prepare("call sp_get_user_purchases(:idUser)");
        
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function setAddComent($idUser,$idProduct,$idPurchase,$coment,$stars)
    {
        $consulta = $this->db->prepare("call sp_add_product_review(:idUser,:idProduct,:idPurchase,:coment,:stars)");

        $consulta->bindParam(':idUser',$idUser);
        $consulta->bindParam(':idProduct',$idProduct);
        $consulta->bindParam(':idPurchase',$idPurchase);
        $consulta->bindParam(':coment',$coment);
        $consulta->bindParam(':stars',$stars);

        return $consulta->execute();        
    }

    public function setClosePurchase($idPurchase)
    {
        $consulta = $this->db->prepare("call sp_update_purchase_state(:idPurchase,:status,:coment);");

        $consulta->bindParam(':idPurchase',$idPurchase);
        $status = -1;
        $consulta->bindParam(':status',$status);
        $coment = ''; 
        $consulta->bindParam(':coment',$coment);
        
        return $consulta->execute();
    }
}
?>