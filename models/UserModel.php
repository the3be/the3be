<?php
class UserModel
{
    protected $db;
 
    public function __construct()
    {
        //Traemos la única instancia de PDO
        $this->db = SPDO::singleton();
    }
    
    public function setUser($data)
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("CALL sp_add_user(:name,:mail,:pass,3,1)"); 

        $consulta->bindParam(':name', $data['name']);
        $consulta->bindParam(':mail', $data['mail']);
        $pass = md5($data['pass']);
        $consulta->bindParam(':pass', $pass);
        
        return $consulta->execute();        
    }//fin setUser

    public function setUserMailLister($data)
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("CALL sp_add_user('',:mail,'','',1)");
        $consulta->bindParam(':mail', $data['mail']);
        
        return $consulta->execute();        
    }//fin setUserMailLister

    public function UpdateUser($data)
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("CALL sp_update_user_info(:idUser,:name,:pass,-1,:state,-2)"); 

        $consulta->bindParam(':idUser', $data['idUser']);
        $consulta->bindParam(':name', $data['name']);
        $pass = $data['pass']!='-1' ? md5($data['pass']) : $data['pass'];
        $consulta->bindParam(':pass', $pass);
        $consulta->bindParam(':state', $data['state']);

        if($consulta->execute()){
            @session_start();
            $_SESSION['USER']['NAMEUSER'] = ($data['name'] != '-1') ? $data['name'] : $_SESSION['USER']['NAMEUSER']; 
        }
    }//fin UpdateUser

    public function getUserData($idUser)
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("CALL sp_get_user_data(:idUser)"); 
        $consulta->bindParam(':idUser', $idUser);
        $consulta->execute();

        return $consulta->fetchall(PDO::FETCH_ASSOC);       
    }//fin getUserData

    public function getAllMailList()
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("CALL sp_get_subscriber()");
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_COLUMN, 0);

    }//fin getAllMailList

    public function loginUser($data)
    {
        $consulta = $this->db->prepare("CALL sp_get_user(:mail,:pass)");
        
        $consulta->bindParam(':mail', $data['mail']);
        $pass = md5($data['pass']);
        $consulta->bindParam(':pass', $pass);
        $consulta->execute();

        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }//fin loginUser

    public function allowUser($mailUser)
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("CALL sp_update_user_state_mail(:mailUser)"); 
        $consulta->bindParam(':mailUser', $mailUser);
        $consulta->execute();
    }//fin allowUser
}
?>