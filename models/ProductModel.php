<?php
class ProductModel
{
    protected $db;
 
    public function __construct()
    {
        //Traemos la única instancia de PDO
        $this->db = SPDO::singleton();
    }

    public function addProduct($data)
    {
         //realizamos la consulta
        $consulta = $this->db->prepare("call sp_add_product (:idUser,:name,:type,:date_ini,:date_end,:price,:quantity,:info,:img,:status,:local,:category_id)"); 

        $consulta->bindParam(':idUser',     $data['idUser']);
        $consulta->bindParam(':name',       $data['name']);
        $consulta->bindParam(':type',       $data['type']);
        $consulta->bindParam(':date_ini',   $data['date_ini']);
        $consulta->bindParam(':date_end',   $data['date_end']);
        $consulta->bindParam(':price',      $data['price']);
        $consulta->bindParam(':quantity',   $data['quantity']);
        $consulta->bindParam(':info',       $data['info']);
        $consulta->bindParam(':img',        $data['img']);
        $consulta->bindParam(':status',     $data['status']);
        $consulta->bindParam(':local',      $data['local']);
        $consulta->bindParam(':category_id',$data['category_id']);
        
        return $consulta->execute();              
    }

    public function getProductById($idProduct)
    {
        $consulta = $this->db->prepare("call sp_get_product_from_id(:idProduct)");
        $consulta->bindParam(':idProduct',$idProduct);
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function getProductBestSeller()
    {
        $consulta = $this->db->prepare("call sp_get_bestseller_product()");
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function getProductBestOfCategory()
    {
        $consulta = $this->db->prepare("call sp_get_best_of_category()");
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function getSimilarProductByIdProduct($IdProduct)
    {

        $product = $this->getProductById($IdProduct);
        $IdCategory = @$product[0]['category_id'];    
            
        $dataFilter['filterName']     = '-1';
        $dataFilter['filterpriceMin'] = '-1';
        $dataFilter['filterpriceMax'] = '-1';
        $dataFilter['filterCategory'] = $IdCategory;
        $dataFilter['filterType']     = '-1';
        return $this->getProductFilter($dataFilter);
    }


    public function getProductFilter($data)
    {
        $consulta = $this->db->prepare("call sp_get_filtered_product(:name,:price_min,:price_max,:category,:type)");

        $consulta->bindParam(':name',       $data['filterName']);
        $consulta->bindParam(':price_min',  $data['filterpriceMin']);
        $consulta->bindParam(':price_max',  $data['filterpriceMax']);
        $consulta->bindParam(':category',   $data['filterCategory']);
        $consulta->bindParam(':type',       $data['filterType']);

        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function getProductProxToEnd()
    {
        $consulta = $this->db->prepare("call sp_get_prox_to_end()");
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function getBestProductToWeek()
    {
        $consulta = $this->db->prepare("call sp_get_best_of_week()");
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function getReviewProductById($idProduct)
    {
        $consulta = $this->db->prepare("call sp_get_product_review(:idProduct)");
        $consulta->bindParam(':idProduct',$idProduct);
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function getNewProducts()
    {
        $consulta = $this->db->prepare("call sp_get_new_products()");
        $consulta->execute();
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

}
?>