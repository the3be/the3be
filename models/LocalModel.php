<?php 
class LocalModel
{
    protected $db;
 
    public function __construct()
    {
        //Traemos la única instancia de PDO
        $this->db = SPDO::singleton();
    }

	public function allLocal($id)
	{
		//realizamos la consulta
        $consulta = $this->db->prepare("call sp_get_local(:idUser)");
        $consulta->bindParam(':idUser',$id);
        $consulta->execute();

        return $consulta->fetchall(PDO::FETCH_ASSOC);
	}

    public function setLocal($data)
    {
        //realizamos la consulta
        $consulta = $this->db->prepare("call sp_add_local(:idUser,:name,:address,:schedule,:phone)"); 

        $consulta->bindParam(':idUser', $data['idUser']);
        $consulta->bindParam(':name', $data['localName']);
        $consulta->bindParam(':address', $data['localAddress']);
        $consulta->bindParam(':schedule', $data['localSchedule']);
        $consulta->bindParam(':phone', $data['localPhone']);
        
        return $consulta->execute();
    }
}
?>