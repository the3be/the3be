<?php
class SaleModel
{
    protected $db;
 
    public function __construct()
    {
        //Traemos la única instancia de PDO
        $this->db = SPDO::singleton();
    }

    public function getMySale($idUser)
    {
        $consulta = $this->db->prepare("call sp_get_user_sales(:idUser)");
        
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function setMySale($data)
    {
        $consulta = $this->db->prepare("call sp_get_Sale_from_id(:idSale,:newStatus,:commentary)");

        $consulta->bindParam(':idUser',$data['idUser']);
        $consulta->bindParam(':newStatus',$data['newStatus']);
        $consulta->bindParam(':commentary',$data['commentary']);
        $consulta->execute();
        
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function getAllSales($idUser)
    {
        $consulta = $this->db->prepare("call sp_get_all_sales_by_idUser(:idUser);");
        $consulta->bindParam(':idUser',$idUser);
        $consulta->execute();
        
        return $consulta->fetchall(PDO::FETCH_ASSOC);
    }

    public function setCloseSales($idSale,$coment)
    {
        $consulta = $this->db->prepare("call sp_update_purchase_state(:idSale,:status,:coment);");

        $consulta->bindParam(':idSale',$idSale);
        $status = 2;
        $consulta->bindParam(':status',$status); 
        $consulta->bindParam(':coment',$coment);
        
        return $consulta->execute();
    }
}
?>